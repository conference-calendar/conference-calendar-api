SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- CREATE SCHEMA IF NOT EXISTS conference_calendar DEFAULT CHARACTER SET latin1 ;
USE `conference_calendar`;

CREATE TABLE IF NOT EXISTS SC_SCHOOL (
  SC_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  SC_NAME VARCHAR(50) NOT NULL COMMENT '',
  SC_STREET VARCHAR(100) NOT NULL COMMENT '',
  SC_CITY VARCHAR(50) NOT NULL COMMENT '',
  SC_STATE VARCHAR(50) NOT NULL COMMENT '',
  SC_POSTAL_CD VARCHAR(50) NOT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (SC_ID)  COMMENT '',
  UNIQUE INDEX SC_SCHOOL_AK (SC_NAME, SC_POSTAL_CD ASC)  COMMENT 'School names must be unique within a given zip code.')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS CS_CONFERENCE_SESSION (
  CS_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  CS_NAME VARCHAR(50) NOT NULL COMMENT '',
  CS_START_DATE DATE NOT NULL COMMENT '',
  CS_END_DATE DATE NOT NULL COMMENT '',
  CS_AVG_MEETING_MIN INT(11) NOT NULL COMMENT '',
  CS_AVG_INTERVAL_MIN INT(11) NOT NULL COMMENT '',
  CS_SCHOOL_ID INT(11) NOT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (CS_ID)  COMMENT '',
  UNIQUE INDEX CS_CONFERENCE_SESSION_AK (CS_NAME ASC, CS_SCHOOL_ID ASC)  COMMENT '',
  INDEX CS_CONFERENCE_SESSION_FK_IDX (CS_SCHOOL_ID ASC)  COMMENT '',
  CONSTRAINT CS_CONFERENCE_SESSION_FK
    FOREIGN KEY (CS_SCHOOL_ID)
    REFERENCES SC_SCHOOL (SC_ID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS CD_CONFERENCE_DAY (
  CD_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  CD_DATE DATE NOT NULL COMMENT '',
  CD_START_TIME TIME NOT NULL COMMENT '',
  CD_END_TIME TIME NOT NULL COMMENT '',
  CD_CONFERENCE_SESSION_ID INT(11) NOT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (CD_ID)  COMMENT '',
  UNIQUE INDEX CD_CONFERENCE_DAY_AK (CD_DATE ASC, CD_CONFERENCE_SESSION_ID ASC)  COMMENT '',
  INDEX CD_CONFERENCE_DAY_FK_IDX (CD_CONFERENCE_SESSION_ID ASC)  COMMENT '',
  CONSTRAINT CD_CONFERENCE_DAY_FK
    FOREIGN KEY (CD_CONFERENCE_SESSION_ID)
    REFERENCES CS_CONFERENCE_SESSION (CS_ID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS CR_CLASSROOM (
  CR_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  CR_NAME VARCHAR(50) NOT NULL COMMENT '',
  CR_DESCRIPTION VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  CR_SCHOOL_ID INT(11) NOT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (CR_ID)  COMMENT '',
  UNIQUE INDEX CR_CLASSROOM_AK (CR_NAME ASC, CR_SCHOOL_ID ASC)  COMMENT '',
  INDEX CR_CLASSROOM_FK_IDX (CR_SCHOOL_ID ASC)  COMMENT '',
  CONSTRAINT CR_CLASSROOM_FK
  FOREIGN KEY (CR_SCHOOL_ID)
    REFERENCES SC_SCHOOL (SC_ID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS PE_PERSON (
  PE_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  PE_FNAME VARCHAR(50) NOT NULL COMMENT '',
  PE_LNAME VARCHAR(50) NOT NULL COMMENT '',
  PE_EMAIL VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  PE_SCHOOL_ID INT(11) NOT NULL COMMENT '',
  PE_USER_FLAG BIT(1) NULL DEFAULT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (PE_ID)  COMMENT '',
  UNIQUE INDEX PE_PERSON_AK (PE_EMAIL ASC, PE_SCHOOL_ID ASC)  COMMENT '',
  INDEX PE_PERSON_FK_IDX (PE_SCHOOL_ID ASC)  COMMENT '',
  CONSTRAINT PE_PERSON_FK
    FOREIGN KEY (PE_SCHOOL_ID)
    REFERENCES SC_SCHOOL (SC_ID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS ST_STUDENT (
  ST_PERSON_ID INT(11) NOT NULL COMMENT '',
  ST_STUDENT_ID VARCHAR(50) NULL DEFAULT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (ST_PERSON_ID)  COMMENT '',
  INDEX ST_STUDENT_FK_IDX (ST_PERSON_ID ASC)  COMMENT '',
  CONSTRAINT ST_STUDENT_FK
    FOREIGN KEY (ST_PERSON_ID)
    REFERENCES PE_PERSON (PE_ID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS TE_TEACHER (
  TE_PERSON_ID INT(11) NOT NULL COMMENT '',
  TE_EMPLOYEE_ID VARCHAR(50) NULL DEFAULT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (TE_PERSON_ID)  COMMENT '',
  INDEX TE_TEACHER_FK_IDX (TE_PERSON_ID ASC)  COMMENT '',
  CONSTRAINT TE_TEACHER_FK
    FOREIGN KEY (TE_PERSON_ID)
    REFERENCES PE_PERSON (PE_ID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS CO_CONFERENCE (
  CO_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  CO_CONFERENCE_DAY_ID INT(11) NOT NULL COMMENT '',
  CO_START_TIME TIME NOT NULL COMMENT '',
  CO_END_TIME TIME NOT NULL COMMENT '',
  CO_STUDENT_ID INT(11) NOT NULL COMMENT '',
  CO_TEACHER_ID INT(11) NOT NULL COMMENT '',
  CO_DESCRIPTION VARCHAR(255) NULL DEFAULT NULL COMMENT '',
  CO_CLASSROOM_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (CO_ID)  COMMENT '',
  UNIQUE INDEX CO_CONFERENCE_TEACHER_AK (CO_CONFERENCE_DAY_ID ASC, CO_START_TIME ASC, CO_TEACHER_ID ASC)  COMMENT '',
  UNIQUE INDEX CO_CONFERENCE_STUDENT_AK (CO_CONFERENCE_DAY_ID ASC, CO_START_TIME ASC, CO_STUDENT_ID ASC)  COMMENT '',
  INDEX CO_CONFERENCE_DAY_FK_IDX (CO_CONFERENCE_DAY_ID ASC)  COMMENT '',
  INDEX CO_CONFERENCE_STUDENT_FK_IDX (CO_STUDENT_ID ASC)  COMMENT '',
  INDEX CO_CONFEENCE_TEACHER_FK_IDX (CO_TEACHER_ID ASC)  COMMENT '',
  INDEX CO_CONFEENCE_ROOM_FK_IDX (CO_CLASSROOM_ID ASC)  COMMENT '',
  CONSTRAINT CO_CONFERENCE_DAY_FK
    FOREIGN KEY (CO_CONFERENCE_DAY_ID)
    REFERENCES CD_CONFERENCE_DAY (CD_ID),
  CONSTRAINT CO_CONFEENCE_ROOM_FK
    FOREIGN KEY (CO_CLASSROOM_ID)
    REFERENCES CR_CLASSROOM (CR_ID),
  CONSTRAINT CO_CONFERENCE_STUDENT_FK
    FOREIGN KEY (CO_STUDENT_ID)
    REFERENCES ST_STUDENT (ST_PERSON_ID),
  CONSTRAINT CO_CONFEENCE_TEACHER_FK
    FOREIGN KEY (CO_TEACHER_ID)
    REFERENCES TE_TEACHER (TE_PERSON_ID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS PR_PERSON_ROLE (
  PR_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  PR_NAME VARCHAR(50) NULL DEFAULT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (PR_ID)  COMMENT '',
  UNIQUE INDEX PR_PERSON_ROLE_AK (PR_NAME ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS CA_CONFERENCE_ATTENDEE (
  CA_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  CA_CONFERENCE_ID INT(11) NOT NULL COMMENT '',
  CA_PERSON_ID INT(11) NOT NULL COMMENT '',
  CA_PERSON_ROLE_ID INT(11) NOT NULL COMMENT '',
  CREATE_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID INT(11) NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE DATE NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (CA_ID)  COMMENT '',
  UNIQUE INDEX CA_CONFERENCE_ATTENDEE_AK (CA_CONFERENCE_ID ASC, CA_PERSON_ID ASC)  COMMENT '',
  INDEX CA_CONFERENCE_ATTENDEE_FK_IDX (CA_CONFERENCE_ID ASC)  COMMENT '',
  INDEX CA_CONFERENCE_ATTENDEE_PERSON_FK_IDX (CA_PERSON_ID ASC)  COMMENT '',
  INDEX CA_CONFERENCE_ATTENDEE_PERSON_ROLE_FK_IDX (CA_PERSON_ROLE_ID ASC)  COMMENT '',
  CONSTRAINT CA_CONFERENCE_ATTENDEE_FK_IDX
    FOREIGN KEY (CA_CONFERENCE_ID)
    REFERENCES CO_CONFERENCE (CO_ID),
  CONSTRAINT CA_CONFERENCE_ATTENDEE_PERSON_FK_IDX
    FOREIGN KEY (CA_PERSON_ID)
    REFERENCES PE_PERSON (PE_ID),
  CONSTRAINT CA_CONFERENCE_ATTENDEE_PERSON_ROLE_FK_IDX
    FOREIGN KEY (CA_PERSON_ROLE_ID)
    REFERENCES PR_PERSON_ROLE (PR_ID))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS SE_SEARCH (
  SE_ID INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  SE_RESULT_SOURCE_ALIAS    VARCHAR(50) NOT NULL COMMENT 'e.g. School',
  SE_RESULT                 VARCHAR(255) NOT NULL COMMENT 'e.g. Warren G. Harding School',
  SE_KEYWORD_SOURCE_ALIAS   VARCHAR(50) NOT NULL COMMENT 'e.g. School',
  SE_KEYWORD_SOURCE_TABLE   VARCHAR(50) NOT NULL COMMENT 'e.g. SC_SCHOOL',
  SE_KEYWORD_SOURCE_ID      INT(11) NOT NULL COMMENT 'This is the primary key for the search value within it\'s source table.',
  SE_KEYWORD_SOURCE_FIELD   VARCHAR(50) NOT NULL COMMENT 'e.g. SC_STREET',
  SE_KEYWORD                VARCHAR(255) NOT NULL COMMENT 'e.g. 3211 165th St, Hammond, IN 46323',
  SE_SCHOOL_ID              INT(11) NOT NULL COMMENT '',
  CREATE_USER_ID            INT(11) NULL DEFAULT NULL COMMENT '',
  CREATE_DATE               DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_DATE               DATE NULL DEFAULT NULL COMMENT '',
  MODIFY_USER_ID            INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (SE_ID)  COMMENT '',
  UNIQUE INDEX SE_SEARCH_AK (SE_KEYWORD_SOURCE_ID ASC, SE_KEYWORD_SOURCE_FIELD ASC, SE_KEYWORD_SOURCE_TABLE ASC)  COMMENT '',
  INDEX SE_SEARCH_FK_IDX (SE_SCHOOL_ID ASC)  COMMENT 'Cannot have matching foreign key constraint because this would prevent us from adding schools since they point to themselves.')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

DELIMITER $$

CREATE TRIGGER SC_TRIGGER_AFTER_DELETE AFTER DELETE ON SC_SCHOOL
FOR EACH ROW
BEGIN
  DELETE FROM SE_SEARCH
  WHERE SE_KEYWORD_SOURCE_TABLE = 'SC_SCHOOL'
  AND SE_KEYWORD_SOURCE_ID = OLD.SC_ID;
END$$

CREATE TRIGGER SC_TRIGGER_AFTER_INSERT AFTER INSERT ON SC_SCHOOL
FOR EACH ROW
BEGIN
  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  VALUES ('School', NEW.SC_NAME, 'School', 'SC_SCHOOL', NEW.SC_ID, 'SC_NAME', NEW.SC_NAME, NEW.SC_ID);

  IF NOT NEW.SC_STREET IS NULL THEN
    INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
    VALUES ('School', NEW.SC_NAME, 'School', 'SC_SCHOOL', NEW.SC_ID, 'SC_STREET', NEW.SC_STREET, NEW.SC_ID);
  END IF;
END$$

CREATE TRIGGER SC_TRIGGER_AFTER_UPDATE AFTER UPDATE ON SC_SCHOOL
FOR EACH ROW
BEGIN
  UPDATE SE_SEARCH
  SET SE_RESULT = NEW.SC_NAME,
      SE_KEYWORD = NEW.SC_NAME
  WHERE SE_KEYWORD_SOURCE_ID = NEW.SC_ID
  AND SE_KEYWORD_SOURCE_TABLE = 'SC_SCHOOL'
  AND SE_KEYWORD_SOURCE_FIELD = 'SC_NAME';

  IF NOT NEW.SC_STREET IS NULL AND NOT OLD.SC_STREET IS NULL THEN
    UPDATE SE_SEARCH
    SET SE_RESULT = NEW.SC_NAME,
        SE_KEYWORD = NEW.SC_STREET
    WHERE SE_KEYWORD_SOURCE_ID = NEW.SC_ID
    AND SE_KEYWORD_SOURCE_TABLE = 'SC_SCHOOL'
    AND SE_KEYWORD_SOURCE_FIELD = 'SC_STREET';
  ELSEIF NOT NEW.SC_STREET IS NULL AND OLD.SC_STREET IS NULL THEN
    INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
    VALUES ('School', NEW.SC_NAME, 'School', 'SC_SCHOOL', NEW.SC_ID, 'SC_STREET', NEW.SC_STREET, NEW.SC_ID);
  ELSEIF NEW.SC_STREET IS NULL AND NOT OLD.SC_STREET IS NULL THEN
    DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_TABLE = 'SC_SCHOOL'
    AND SE_KEYWORD_SOURCE_ID = OLD.SC_ID
    AND SE_KEYWORD_SOURCE_FIELD = 'SC_STREET';
  END IF;
END$$

CREATE TRIGGER SC_TRIGGER_BEFORE_DELETE BEFORE DELETE ON SC_SCHOOL
FOR EACH ROW
BEGIN
  DELETE FROM SE_SEARCH
  WHERE SE_SCHOOL_ID = OLD.SC_ID;

  DELETE FROM CR_CLASSROOM
  WHERE CR_SCHOOL_ID = OLD.SC_ID;

  DELETE FROM CS_CONFERENCE_SESSION
  WHERE CS_SCHOOL_ID = OLD.SC_ID;

  DELETE FROM PE_PERSON
  WHERE PE_SCHOOL_ID = OLD.SC_ID;
END$$

CREATE TRIGGER SC_TRIGGER_INSERT BEFORE INSERT ON SC_SCHOOL
FOR EACH ROW
BEGIN
 SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER SC_TRIGGER_UPDATE BEFORE UPDATE ON SC_SCHOOL
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER CS_TRIGGER_AFTER_DELETE AFTER DELETE ON CS_CONFERENCE_SESSION
FOR EACH ROW
BEGIN
	DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_TABLE = 'CS_CONFERENCE_SESSION'
    AND SE_KEYWORD_SOURCE_FIELD = 'CS_NAME'
    AND SE_KEYWORD_SOURCE_ID = OLD.CS_ID;
END$$

CREATE TRIGGER CS_TRIGGER_AFTER_INSERT AFTER INSERT ON CS_CONFERENCE_SESSION
FOR EACH ROW
BEGIN
  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  VALUES ('Conference Session', NEW.CS_NAME, 'Conference Session', 'CS_CONFERENCE_SESSION', NEW.CS_ID, 'CS_NAME', NEW.CS_NAME, NEW.CS_SCHOOL_ID);
END$$

CREATE TRIGGER CS_TRIGGER_AFTER_UPDATE AFTER UPDATE ON CS_CONFERENCE_SESSION
FOR EACH ROW
BEGIN
    UPDATE SE_SEARCH
    SET SE_RESULT = NEW.CS_NAME,
        SE_KEYWORD = NEW.CS_NAME
    WHERE SE_KEYWORD_SOURCE_ID = NEW.CS_ID
          AND SE_KEYWORD_SOURCE_TABLE = 'CS_CONFERENCE_SESSION'
          AND SE_KEYWORD_SOURCE_FIELD = 'CS_NAME';
END$$

CREATE TRIGGER CS_TRIGGER_BEFORE_DELETE BEFORE DELETE ON CS_CONFERENCE_SESSION
FOR EACH ROW
BEGIN
  DELETE FROM CD_CONFERENCE_DAY
  WHERE CD_CONFERENCE_SESSION_ID = OLD.CS_ID;
END$$

CREATE TRIGGER CS_TRIGGER_INSERT BEFORE INSERT ON CS_CONFERENCE_SESSION
FOR EACH ROW
BEGIN
  IF NEW.CS_START_DATE < NEW.CS_END_DATE THEN
    SET NEW.CREATE_DATE = curdate();
  ELSE
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference end date must be greater than the conference start date.';
  END IF;
END$$

CREATE TRIGGER CS_TRIGGER_UPDATE BEFORE UPDATE ON CS_CONFERENCE_SESSION
FOR EACH ROW
BEGIN
  IF NEW.CS_START_DATE < NEW.CS_END_DATE THEN
    SET NEW.CREATE_DATE = OLD.CREATE_DATE;
    SET NEW.MODIFY_DATE = curdate();
  ELSE
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference end date must be greater than the conference start date.';
  END IF;
END$$

CREATE TRIGGER CR_TRIGGER_AFTER_DELETE AFTER DELETE ON CR_CLASSROOM
FOR EACH ROW
BEGIN
	DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_TABLE = 'CR_CLASSROOM'
    AND SE_KEYWORD_SOURCE_FIELD = 'CR_NAME'
    AND SE_KEYWORD_SOURCE_ID = OLD.CR_ID;
END$$

CREATE TRIGGER CR_TRIGGER_AFTER_INSERT AFTER INSERT ON CR_CLASSROOM
FOR EACH ROW
BEGIN
  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  VALUES ('Classroom', NEW.CR_NAME, 'Classroom', 'CR_CLASSROOM', NEW.CR_ID, 'CR_NAME', NEW.CR_NAME, NEW.CR_SCHOOL_ID);
END$$

CREATE TRIGGER CR_TRIGGER_AFTER_UPDATE AFTER UPDATE ON CR_CLASSROOM
FOR EACH ROW
BEGIN
  UPDATE SE_SEARCH
  SET SE_RESULT = NEW.CR_NAME,
      SE_KEYWORD = NEW.CR_NAME
  WHERE SE_KEYWORD_SOURCE_ID = NEW.CR_ID
  AND SE_KEYWORD_SOURCE_TABLE = 'CR_CLASSROOM'
  AND SE_KEYWORD_SOURCE_FIELD = 'CR_NAME';
END$$

CREATE TRIGGER CR_TRIGGER_INSERT BEFORE INSERT ON CR_CLASSROOM
FOR EACH ROW
BEGIN
    SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER CR_TRIGGER_UPDATE BEFORE UPDATE ON CR_CLASSROOM
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER PE_TRIGGER_AFTER_DELETE AFTER DELETE ON PE_PERSON
FOR EACH ROW
BEGIN
  DELETE FROM SE_SEARCH
  WHERE SE_KEYWORD_SOURCE_TABLE = 'PE_PERSON'
  AND SE_KEYWORD_SOURCE_FIELD IN ('DERIVED', 'PE_EMAIL')
  AND SE_KEYWORD_SOURCE_ID = OLD.PE_ID;
END$$

CREATE TRIGGER PE_TRIGGER_AFTER_INSERT AFTER INSERT ON PE_PERSON
FOR EACH ROW
BEGIN
  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  VALUES ('Person', CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME), 'Person', 'PE_PERSON', NEW.PE_ID, 'DERIVED', CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME), NEW.PE_SCHOOL_ID);

  IF NOT NEW.PE_EMAIL IS NULL THEN
    INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
    VALUES ('Person', CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME), 'Person', 'PE_PERSON', NEW.PE_ID, 'PE_EMAIL', NEW.PE_EMAIL, NEW.PE_SCHOOL_ID);
  END IF;
END$$

CREATE TRIGGER PE_TRIGGER_AFTER_UPDATE AFTER UPDATE ON PE_PERSON
FOR EACH ROW
BEGIN
    UPDATE SE_SEARCH
    SET SE_RESULT = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME),
        SE_KEYWORD = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME)
    WHERE SE_KEYWORD_SOURCE_ID = NEW.PE_ID
    AND SE_KEYWORD_SOURCE_TABLE = 'PE_PERSON'
    AND SE_KEYWORD_SOURCE_FIELD = 'DERIVED';

    IF NOT NEW.PE_EMAIL IS NULL AND NOT OLD.PE_EMAIL IS NULL THEN
      UPDATE SE_SEARCH
      SET SE_RESULT = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME),
          SE_KEYWORD = NEW.PE_EMAIL
      WHERE SE_KEYWORD_SOURCE_ID = NEW.PE_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'PE_PERSON'
      AND SE_KEYWORD_SOURCE_FIELD = 'PE_EMAIL';
    ELSEIF NOT NEW.PE_EMAIL IS NULL AND OLD.PE_EMAIL IS NULL THEN
      INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
      VALUES ('Person', CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME), 'Person', 'PE_PERSON', NEW.PE_ID, 'PE_EMAIL', NEW.PE_EMAIL, NEW.PE_SCHOOL_ID);
    ELSEIF NEW.PE_EMAIL IS NULL AND NOT OLD.PE_EMAIL IS NULL THEN
      DELETE FROM SE_SEARCH
      WHERE SE_KEYWORD_SOURCE_TABLE = 'PE_PERSON'
      AND SE_KEYWORD_SOURCE_FIELD = 'PE_EMAIL'
      AND SE_KEYWORD_SOURCE_ID = OLD.PE_ID;
    END IF;

    -- ********************************************************************************
    -- I am banking on the person and conference tables and triggers being so intimately
    -- connected is a one-off.  If this turns out not to be the case then we will
    -- need to look for an alternate rule/search update strategy.
    -- ********************************************************************************
    UPDATE SE_SEARCH INNER JOIN CO_CONFERENCE ON SE_KEYWORD_SOURCE_ID = CO_ID AND
                                                 SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE' AND
                                                 SE_KEYWORD_SOURCE_FIELD = 'CO_STUDENT_ID'
    SET SE_KEYWORD = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME)
    WHERE CO_STUDENT_ID = NEW.PE_ID;

    UPDATE SE_SEARCH INNER JOIN CO_CONFERENCE ON SE_KEYWORD_SOURCE_ID = CO_ID AND
                                                 SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE' AND
                                                 SE_KEYWORD_SOURCE_FIELD = 'CO_TEACHER_ID'
    SET SE_KEYWORD = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME)
    WHERE CO_TEACHER_ID = NEW.PE_ID;

    UPDATE SE_SEARCH
    SET SE_RESULT = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME)
    WHERE SE_KEYWORD_SOURCE_ID = NEW.PE_ID
    AND SE_KEYWORD_SOURCE_TABLE = 'ST_STUDENT'
    AND SE_KEYWORD_SOURCE_FIELD = 'ST_STUDENT_ID';

    UPDATE SE_SEARCH
    SET SE_RESULT = CONCAT(CONCAT(NEW.PE_FNAME, ' '), NEW.PE_LNAME)
    WHERE SE_KEYWORD_SOURCE_ID = NEW.PE_ID
    AND SE_KEYWORD_SOURCE_TABLE = 'TE_TEACHER'
    AND SE_KEYWORD_SOURCE_FIELD = 'TE_TEACHER_ID';
  END$$

CREATE TRIGGER PE_TRIGGER_BEFORE_DELETE BEFORE DELETE ON PE_PERSON
FOR EACH ROW
BEGIN
  DELETE FROM ST_STUDENT
  WHERE ST_PERSON_ID = OLD.PE_ID;

  DELETE FROM TE_TEACHER
  WHERE TE_PERSON_ID = OLD.PE_ID;

  DELETE FROM CA_CONFERENCE_ATTENDEE
  WHERE CA_PERSON_ID = OLD.PE_ID;
END$$

CREATE TRIGGER PE_TRIGGER_INSERT BEFORE INSERT ON PE_PERSON
FOR EACH ROW
BEGIN
 SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER PE_TRIGGER_UPDATE BEFORE UPDATE ON PE_PERSON
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER ST_TRIGGER_AFTER_DELETE AFTER DELETE ON ST_STUDENT
FOR EACH ROW
BEGIN
  DELETE FROM SE_SEARCH
  WHERE SE_KEYWORD_SOURCE_TABLE = 'ST_STUDENT'
  AND SE_KEYWORD_SOURCE_ID = OLD.ST_PERSON_ID;
END$$

CREATE TRIGGER ST_TRIGGER_AFTER_INSERT AFTER INSERT ON ST_STUDENT
FOR EACH ROW
BEGIN
  UPDATE SE_SEARCH
  SET SE_RESULT_SOURCE_ALIAS = 'Student',
      SE_KEYWORD_SOURCE_ALIAS = 'Student'
  WHERE SE_KEYWORD_SOURCE_TABLE = 'PE_PERSON'
  AND SE_KEYWORD_SOURCE_ID = NEW.ST_PERSON_ID;

  IF NOT NEW.ST_STUDENT_ID IS NULL THEN
    INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
    SELECT 'Person', CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME), 'Student', 'ST_STUDENT', NEW.ST_PERSON_ID, 'ST_STUDENT_ID', NEW.ST_STUDENT_ID, PE_SCHOOL_ID
    FROM PE_PERSON
    WHERE PE_ID = NEW.ST_PERSON_ID;
  END IF;
END$$

CREATE TRIGGER ST_TRIGGER_AFTER_UPDATE AFTER UPDATE ON ST_STUDENT
FOR EACH ROW
BEGIN
  IF NOT NEW.ST_STUDENT_ID IS NULL AND NOT OLD.ST_STUDENT_ID IS NULL THEN
    UPDATE SE_SEARCH
    SET SE_KEYWORD = NEW.ST_STUDENT_ID
    WHERE SE_KEYWORD_SOURCE_ID = NEW.ST_PERSON_ID
          AND SE_KEYWORD_SOURCE_TABLE = 'ST_STUDENT'
          AND SE_KEYWORD_SOURCE_FIELD = 'ST_STUDENT_ID';
  ELSEIF NOT NEW.ST_STUDENT_ID IS NULL AND OLD.ST_STUDENT_ID IS NULL THEN
    INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
    SELECT 'Student', CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME), 'Student', 'ST_STUDENT', NEW.ST_PERSON_ID, 'ST_STUDENT_ID', NEW.ST_STUDENT_ID, PE_SCHOOL_ID
    FROM PE_PERSON
    WHERE PE_ID = NEW.ST_PERSON_ID;
  ELSEIF NEW.ST_STUDENT_ID IS NULL AND NOT OLD.ST_STUDENT_ID IS NULL THEN
    DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_ID = NEW.ST_PERSON_ID
    AND SE_KEYWORD_SOURCE_TABLE = 'ST_STUDENT'
    AND SE_KEYWORD_SOURCE_FIELD = 'ST_STUDENT_ID';
  END IF;
END$$

CREATE TRIGGER ST_TRIGGER_INSERT BEFORE INSERT ON ST_STUDENT
FOR EACH ROW
BEGIN
  IF (SELECT COUNT(*) FROM ST_STUDENT INNER JOIN PE_PERSON ON ST_PERSON_ID = PE_ID AND ST_STUDENT_ID = NEW.ST_STUDENT_ID) = 1 THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The specified student ID already exists.';
  ELSE
    SET NEW.CREATE_DATE = curdate();
  END IF;
END$$

CREATE TRIGGER ST_TRIGGER_UPDATE BEFORE UPDATE ON ST_STUDENT
FOR EACH ROW
BEGIN
  IF (SELECT COUNT(*) FROM ST_STUDENT INNER JOIN PE_PERSON ON ST_PERSON_ID = PE_ID AND ST_STUDENT_ID = NEW.ST_STUDENT_ID AND ST_PERSON_ID != NEW.ST_PERSON_ID) = 1 THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The specified student ID already exists.';
  ELSE
    SET NEW.CREATE_DATE = OLD.CREATE_DATE;
    SET NEW.MODIFY_DATE = curdate();
  END IF;
END$$

CREATE TRIGGER TE_TRIGGER_AFTER_DELETE AFTER DELETE ON TE_TEACHER
FOR EACH ROW
BEGIN
    DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_TABLE = 'TE_TEACHER'
    AND SE_KEYWORD_SOURCE_ID = OLD.TE_PERSON_ID;
END$$

CREATE TRIGGER TE_TRIGGER_AFTER_INSERT AFTER INSERT ON TE_TEACHER
FOR EACH ROW
BEGIN
    UPDATE SE_SEARCH
    SET SE_RESULT_SOURCE_ALIAS = 'Teacher',
        SE_KEYWORD_SOURCE_ALIAS = 'Teacher'
    WHERE SE_KEYWORD_SOURCE_TABLE = 'PE_PERSON'
    AND SE_KEYWORD_SOURCE_ID = NEW.TE_PERSON_ID;

    IF NOT NEW.TE_EMPLOYEE_ID IS NULL THEN
      INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
      SELECT 'Teacher', CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME), 'Teacher', 'TE_TEACHER', NEW.TE_PERSON_ID, 'TE_EMPLOYEE_ID', NEW.TE_EMPLOYEE_ID, PE_SCHOOL_ID
      FROM PE_PERSON
      WHERE PE_ID = NEW.TE_PERSON_ID;
    END IF;
END$$

CREATE TRIGGER TE_TRIGGER_AFTER_UPDATE AFTER UPDATE ON TE_TEACHER
FOR EACH ROW
BEGIN
    IF NOT NEW.TE_EMPLOYEE_ID IS NULL AND NOT OLD.TE_EMPLOYEE_ID IS NULL THEN
      UPDATE SE_SEARCH
      SET SE_KEYWORD = NEW.TE_EMPLOYEE_ID
      WHERE SE_KEYWORD_SOURCE_ID = NEW.TE_PERSON_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'TE_TEACHER'
      AND SE_KEYWORD_SOURCE_FIELD = 'TE_EMPLOYEE_ID';
    ELSEIF NOT NEW.TE_EMPLOYEE_ID IS NULL AND OLD.TE_EMPLOYEE_ID IS NULL THEN
      INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
      SELECT 'Person', CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME), 'Teacher', 'TE_TEACHER', NEW.TE_PERSON_ID, 'TE_EMPLOYEE_ID', NEW.TE_EMPLOYEE_ID, PE_SCHOOL_ID
      FROM PE_PERSON
      WHERE PE_ID = NEW.TE_PERSON_ID;
    ELSEIF NEW.TE_EMPLOYEE_ID IS NULL AND NOT OLD.TE_EMPLOYEE_ID IS NULL THEN
      DELETE FROM SE_SEARCH
      WHERE SE_KEYWORD_SOURCE_ID = NEW.TE_PERSON_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'TE_TEACHER'
      AND SE_KEYWORD_SOURCE_FIELD = 'TE_EMPLOYEE_ID';
    END IF;
END$$

CREATE TRIGGER TE_TRIGGER_INSERT BEFORE INSERT ON TE_TEACHER
FOR EACH ROW
BEGIN
    SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER TE_TRIGGER_UPDATE BEFORE UPDATE ON TE_TEACHER
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER CO_TRIGGER_AFTER_DELETE AFTER DELETE ON CO_CONFERENCE
FOR EACH ROW
BEGIN
    DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE'
    AND SE_KEYWORD_SOURCE_ID = OLD.CO_ID;
END$$

CREATE TRIGGER CO_TRIGGER_AFTER_INSERT AFTER INSERT ON CO_CONFERENCE
FOR EACH ROW
BEGIN
  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  SELECT 'Conference Scheduled', DATE_FORMAT(CONCAT(CD_DATE, ' ', NEW.CO_START_TIME), '%W %M %d, %Y %h:%i %p'), 'Student', 'CO_CONFERENCE', NEW.CO_ID, 'CO_STUDENT_ID', NEW.CO_STUDENT_ID, PE_SCHOOL_ID
  FROM PE_PERSON, CD_CONFERENCE_DAY
  WHERE PE_ID = NEW.CO_STUDENT_ID
  AND CD_ID = NEW.CO_CONFERENCE_DAY_ID;

  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  SELECT 'Conference Scheduled', DATE_FORMAT(CONCAT(CD_DATE, ' ', NEW.CO_START_TIME), '%W %M %d, %Y %h:%i %p'), 'Teacher', 'CO_CONFERENCE', NEW.CO_ID, 'CO_TEACHER_ID', NEW.CO_TEACHER_ID, PE_SCHOOL_ID
  FROM PE_PERSON, CD_CONFERENCE_DAY
  WHERE PE_ID = NEW.CO_TEACHER_ID
  AND CD_ID = NEW.CO_CONFERENCE_DAY_ID;

  IF NOT NEW.CO_DESCRIPTION IS NULL THEN
    INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
    SELECT 'Conference Scheduled', DATE_FORMAT(CONCAT(CD_DATE, ' ', NEW.CO_START_TIME), '%W %M %d, %Y %h:%i %p'), 'Conference Scheduled', 'CO_CONFERENCE', NEW.CO_ID, 'CO_DESCRIPTION', NEW.CO_DESCRIPTION, CS_SCHOOL_ID
    FROM CS_CONFERENCE_SESSION INNER JOIN CD_CONFERENCE_DAY ON CS_ID = CD_CONFERENCE_SESSION_ID
    WHERE CD_ID = NEW.CO_CONFERENCE_DAY_ID;
  END IF;
END$$

CREATE TRIGGER CO_TRIGGER_AFTER_UPDATE AFTER UPDATE ON CO_CONFERENCE
FOR EACH ROW
BEGIN
    IF NEW.CO_STUDENT_ID != OLD.CO_STUDENT_ID THEN
      UPDATE SE_SEARCH INNER JOIN PE_PERSON ON PE_ID = NEW.CO_STUDENT_ID
      SET SE_KEYWORD = CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME)
      WHERE SE_KEYWORD_SOURCE_ID = NEW.CO_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE'
      AND SE_KEYWORD_SOURCE_FIELD = 'CO_STUDENT_ID';
    END IF;

    IF NEW.CO_TEACHER_ID != OLD.CO_TEACHER_ID THEN
      UPDATE SE_SEARCH INNER JOIN PE_PERSON ON PE_ID = NEW.CO_TEACHER_ID
      SET SE_KEYWORD = CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME)
      WHERE SE_KEYWORD_SOURCE_ID = NEW.CO_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE'
      AND SE_KEYWORD_SOURCE_FIELD = 'CO_TEACHER_ID';
    END IF;

    IF NOT NEW.CO_DESCRIPTION IS NULL AND NOT OLD.CO_DESCRIPTION IS NULL THEN
      UPDATE SE_SEARCH
      SET SE_KEYWORD = NEW.CO_DESCRIPTION
      WHERE SE_KEYWORD_SOURCE_ID = NEW.CO_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE'
      AND SE_KEYWORD_SOURCE_FIELD = 'CO_DESCRIPTION';
    ELSEIF NOT NEW.CO_DESCRIPTION IS NULL AND OLD.CO_DESCRIPTION IS NULL THEN
      INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
      SELECT 'Conference Scheduled', DATE_FORMAT(CONCAT(CD_DATE, ' ', NEW.CO_START_TIME), '%W %M %d, %Y %h:%i %p'), 'Conference Scheduled', 'CO_CONFERENCE', NEW.CO_ID, 'CO_DESCRIPTION', NEW.CO_DESCRIPTION, CS_SCHOOL_ID
      FROM CS_CONFERENCE_SESSION INNER JOIN CD_CONFERENCE_DAY ON CS_ID = CD_CONFERENCE_SESSION_ID
      WHERE CD_ID = NEW.CO_CONFERENCE_DAY_ID;
    ELSEIF NEW.CO_DESCRIPTION IS NULL AND NOT OLD.CO_DESCRIPTION IS NULL THEN
      DELETE FROM SE_SEARCH
      WHERE SE_KEYWORD_SOURCE_ID = NEW.CO_ID
      AND SE_KEYWORD_SOURCE_TABLE = 'CO_CONFERENCE'
      AND SE_KEYWORD_SOURCE_FIELD = 'CO_DESCRIPTION';
    END IF;
END$$

CREATE TRIGGER CO_TRIGGER_INSERT BEFORE INSERT ON CO_CONFERENCE
FOR EACH ROW
BEGIN
  IF (NEW.CO_START_TIME >= NEW.CO_END_TIME) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference start time must be less than the conference end time.';
  ELSEIF (SELECT COUNT(*) FROM CD_CONFERENCE_DAY WHERE CD_ID = NEW.CO_CONFERENCE_DAY_ID AND NEW.CO_START_TIME >= CD_START_TIME AND NEW.CO_END_TIME <= CD_END_TIME) = 1 THEN
    SET NEW.CREATE_DATE = curdate();
  ELSE
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference start and end times must fall between the conference day''s begin and end times.';
  END IF;
END$$

CREATE TRIGGER CO_TRIGGER_UPDATE BEFORE UPDATE ON CO_CONFERENCE
FOR EACH ROW
BEGIN
  IF (NEW.CO_START_TIME >= NEW.CO_END_TIME) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference start time must be less than the conference end time.';
  ELSEIF (SELECT COUNT(*) FROM CD_CONFERENCE_DAY WHERE CD_ID = NEW.CO_CONFERENCE_DAY_ID AND NEW.CO_START_TIME >= CD_START_TIME AND NEW.CO_END_TIME <= CD_END_TIME) = 1 THEN
    SET NEW.CREATE_DATE = OLD.CREATE_DATE;
    SET NEW.MODIFY_DATE = curdate();
  ELSE
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference start and end times must fall between the conference day''s begin and end times.';
  END IF;
END$$

CREATE TRIGGER PR_TRIGGER_INSERT BEFORE INSERT ON PR_PERSON_ROLE
FOR EACH ROW
BEGIN
    SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER PR_TRIGGER_UPDATE BEFORE UPDATE ON PR_PERSON_ROLE
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER CA_TRIGGER_AFTER_DELETE AFTER DELETE ON CA_CONFERENCE_ATTENDEE
FOR EACH ROW
BEGIN
    DELETE FROM SE_SEARCH
    WHERE SE_KEYWORD_SOURCE_TABLE = 'CA_CONFERENCE_ATTENDEE'
    AND SE_KEYWORD_SOURCE_ID = OLD.CA_ID;
END$$

CREATE TRIGGER CA_TRIGGER_AFTER_INSERT AFTER INSERT ON CA_CONFERENCE_ATTENDEE
FOR EACH ROW
BEGIN
  INSERT INTO SE_SEARCH(SE_RESULT_SOURCE_ALIAS, SE_RESULT, SE_KEYWORD_SOURCE_ALIAS, SE_KEYWORD_SOURCE_TABLE, SE_KEYWORD_SOURCE_ID, SE_KEYWORD_SOURCE_FIELD, SE_KEYWORD, SE_SCHOOL_ID)
  SELECT 'Conference Scheduled', DATE_FORMAT(CONCAT(CD_DATE, ' ', CO_START_TIME), '%W %M %d, %Y %h:%i %p'), PR_NAME, 'CA_CONFERENCE_ATTENDEE', NEW.CA_ID, 'CA_PERSON_ID', CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME), PE_SCHOOL_ID
  FROM PE_PERSON, PR_PERSON_ROLE, CD_CONFERENCE_DAY INNER JOIN CO_CONFERENCE ON CD_ID = CO_CONFERENCE_DAY_ID
  WHERE PE_ID = NEW.CA_PERSON_ID
  AND PR_ID = NEW.CA_PERSON_ROLE_ID
  AND CO_ID = NEW.CA_CONFERENCE_ID;
END$$

CREATE TRIGGER CA_TRIGGER_AFTER_UPDATE AFTER UPDATE ON CA_CONFERENCE_ATTENDEE
FOR EACH ROW
BEGIN
  IF NEW.CA_PERSON_ID != OLD.CA_PERSON_ID THEN
    UPDATE SE_SEARCH JOIN PE_PERSON
    SET SE_KEYWORD = CONCAT(CONCAT(PE_FNAME, ' '), PE_LNAME)
    WHERE PE_ID = NEW.CA_PERSON_ID
    AND SE_KEYWORD_SOURCE_TABLE = 'CA_CONFERENCE_ATTENDEE'
    AND SE_KEYWORD_SOURCE_FIELD = 'CA_PERSON_ID'
    AND SE_KEYWORD_SOURCE_ID = NEW.CA_ID;
  END IF;
END$$

CREATE TRIGGER CA_TRIGGER_INSERT BEFORE INSERT ON CA_CONFERENCE_ATTENDEE
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER CA_TRIGGER_UPDATE BEFORE UPDATE ON CA_CONFERENCE_ATTENDEE
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER SE_TRIGGER_INSERT BEFORE INSERT ON SE_SEARCH
FOR EACH ROW
BEGIN
    SET NEW.CREATE_DATE = curdate();
END$$

CREATE TRIGGER SE_TRIGGER_UPDATE BEFORE UPDATE ON SE_SEARCH
FOR EACH ROW
BEGIN
  SET NEW.CREATE_DATE = OLD.CREATE_DATE;
  SET NEW.MODIFY_DATE = curdate();
END$$

CREATE TRIGGER CD_TRIGGER_INSERT BEFORE INSERT ON CD_CONFERENCE_DAY
FOR EACH ROW
BEGIN
  IF (SELECT COUNT(*) FROM CS_CONFERENCE_SESSION WHERE CS_ID = NEW.CD_CONFERENCE_SESSION_ID AND NEW.CD_DATE >= CS_START_DATE AND NEW.CD_DATE <= CS_END_DATE) = 1 THEN
    SET NEW.CREATE_DATE = curdate();
  ELSE
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference day must fall between the conference session''s begin and end dates.';
  END IF;
END$$

CREATE TRIGGER CD_TRIGGER_UPDATE BEFORE UPDATE ON CD_CONFERENCE_DAY
FOR EACH ROW
BEGIN
  IF (SELECT COUNT(*) FROM CS_CONFERENCE_SESSION WHERE CS_ID = NEW.CD_CONFERENCE_SESSION_ID AND NEW.CD_DATE >= CS_START_DATE AND NEW.CD_DATE <= CS_END_DATE) = 1 THEN
    SET NEW.CREATE_DATE = OLD.CREATE_DATE;
    SET NEW.MODIFY_DATE = curdate();
  ELSE
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The conference day must fall between the conference session''s begin and end dates.';
  END IF;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- GRANT ALL privileges ON conference_calendar.* to 'ccadmin'@'%';

-- flush privileges;
