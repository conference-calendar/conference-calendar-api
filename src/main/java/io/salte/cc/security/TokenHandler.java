package io.salte.cc.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.salte.cc.configuration.ConfigurationSettings;

import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;

public class TokenHandler {
    private PublicKey publicKey;
    private UserService userService;

    public TokenHandler(Certificate certificate, UserService userService) throws CertificateEncodingException {
        publicKey = certificate.getPublicKey();
        this.userService = userService;
    }

    public User parseUserFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
        User user = (User) userService.loadUserByUsername(claims.get(ConfigurationSettings.getInstance().getEmailClaimIndex()).toString());

        String roles[] = claims.get(ConfigurationSettings.getInstance().getRoleClaimIndex()).toString().split(",");
        for (int i = 0; i < roles.length; i++) user.addAuthority(roles[i].trim());

        return user;
    }
}
