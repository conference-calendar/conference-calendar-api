package io.salte.cc.security;

import org.springframework.security.core.GrantedAuthority;

public class UserRole implements GrantedAuthority {
    private String role;

    // Must be prefixed with "ROLE_" even though this prefix isn't required in the "hasRole" annotation.
    public UserRole(String role) {
        this.role = ((role.startsWith("ROLE_") ? role : "ROLE_".concat(role.toLowerCase())));
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
