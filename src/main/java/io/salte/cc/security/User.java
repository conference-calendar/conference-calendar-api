package io.salte.cc.security;

import io.salte.cc.models.SecurityRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class User implements UserDetails {
    private String username;
    private Collection<UserRole> grantedAuthorities;
    private Integer schoolId;

    public User(String username, Collection<UserRole> grantedAuthorities, Integer schoolId) {
        this.username = username;
        this.grantedAuthorities = grantedAuthorities;
        this.schoolId = schoolId;
    }

    public User(String username) {
        this.username = username;
        grantedAuthorities = new ArrayList<>();
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void addAuthority(String role) {
        this.grantedAuthorities.add(new UserRole(role));
    }

    public void addAuthority(SecurityRole role) {
        this.grantedAuthorities.add(new UserRole(role.getRoleCode()));
    }
}