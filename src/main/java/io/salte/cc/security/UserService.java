package io.salte.cc.security;

import io.salte.cc.models.Person;
import io.salte.cc.services.IPeopleService;
import io.salte.cc.services.ISecurityRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

public class UserService implements UserDetailsService {
    @Autowired
    IPeopleService peopleService;

    @Autowired
    ISecurityRoleService securityRoleService;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String emailAddress) throws UsernameNotFoundException {
        User user = new User(emailAddress);

        Person person = peopleService.getPerson(emailAddress);
        if (person != null) {
            user.setSchoolId(person.getSchoolId());
            if (person.getSecurityRoleId() != null)
                user.addAuthority(securityRoleService.getSecurityRole(person.getSecurityRoleId()));
        }

        return user;
    }
}
