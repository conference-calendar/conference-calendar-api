package io.salte.cc.security;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

public class TokenAuthenticationService {
    private static final String AUTH_HEADER_NAME = "X-JWT-ASSERTION";

    TokenHandler tokenHandler;

    public TokenAuthenticationService(String certificatePathAndFilename, UserService userService) throws IOException, CertificateException {
        FileInputStream fileInputStream = new FileInputStream(certificatePathAndFilename);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        if (bufferedInputStream.available() > 0) {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            Certificate certificate = certificateFactory.generateCertificate(bufferedInputStream);
            tokenHandler = new TokenHandler(certificate, userService);
        }
        bufferedInputStream.close();
    }

    public Authentication getAuthentication(HttpServletRequest httpServletRequest) {
        final String token = httpServletRequest.getHeader(AUTH_HEADER_NAME);
         User user = tokenHandler.parseUserFromToken(token);
        return new TokenAuthentication(user);
    }
}
