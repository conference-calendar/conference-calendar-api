package io.salte.cc.repositories;

import io.salte.cc.models.SecurityRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("SecurityRoleRepository")
public interface SecurityRoleRepository extends JpaRepository<SecurityRole, Integer> {
}
