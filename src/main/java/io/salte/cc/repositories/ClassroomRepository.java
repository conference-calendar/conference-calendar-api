package io.salte.cc.repositories;


import io.salte.cc.models.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ClassroomRepository")
public interface ClassroomRepository extends JpaRepository<Classroom, Integer> {
    List<Classroom> findAllBySchoolId(Integer schoolId);
}