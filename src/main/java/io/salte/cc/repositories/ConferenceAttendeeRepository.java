package io.salte.cc.repositories;

import io.salte.cc.models.ConferenceAttendee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ConferenceAttendeeRepository")
public interface ConferenceAttendeeRepository extends JpaRepository<ConferenceAttendee, Integer> {
    List<ConferenceAttendee> findAllByPersonId(Integer personId);
    List<ConferenceAttendee> findAllByPersonRoleId(Integer roleId);
    List<ConferenceAttendee> findAllByConferenceId(Integer conferenceId);
}