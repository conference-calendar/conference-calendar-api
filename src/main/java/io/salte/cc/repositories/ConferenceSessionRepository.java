package io.salte.cc.repositories;

import io.salte.cc.models.ConferenceSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ConferenceSessionRepository")
public interface ConferenceSessionRepository extends JpaRepository<ConferenceSession, Integer> {
    List<ConferenceSession> findAllBySchoolId(Integer schoolId);
}
