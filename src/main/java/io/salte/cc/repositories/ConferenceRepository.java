package io.salte.cc.repositories;

import io.salte.cc.models.Conference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ConferenceRepository")
public interface ConferenceRepository extends JpaRepository<Conference, Integer> {
    List<Conference> findAllByTeacherId(Integer personId);
    List<Conference> findAllByStudentId(Integer personId);
    List<Conference> findAllByConferenceDayId(Integer conferenceDayId);
    List<Conference> findAllByClassroomId(Integer classroomId);
    List<Conference> findAllByConferenceDayIdAndTeacherId(Integer conferenceDayId, Integer personId);
}
