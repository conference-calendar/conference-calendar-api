package io.salte.cc.repositories;

import io.salte.cc.models.ConferenceDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ConferenceDayRepository")
public interface ConferenceDayRepository extends JpaRepository<ConferenceDay, Integer> {
    List<ConferenceDay> findAllByConferenceSessionIdOrderByDateAsc(Integer conferenceSessionId);
}
