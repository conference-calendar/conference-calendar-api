package io.salte.cc.repositories;

import io.salte.cc.models.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("SchoolRepository")
public interface SchoolRepository extends JpaRepository<School, Integer> {

}