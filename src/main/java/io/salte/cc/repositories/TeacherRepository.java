package io.salte.cc.repositories;

import io.salte.cc.models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("TeacherRepository")
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
    List<Teacher> findAllBySchoolId(Integer schoolId);
}
