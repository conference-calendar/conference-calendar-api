package io.salte.cc.repositories;

import io.salte.cc.models.SearchResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Repository("SearchRepository")
@Transactional(readOnly = true)
public class SearchRepository {
    private static final Logger logger = LogManager.getLogger(SearchRepository.class);

    @PersistenceContext(unitName = "appDb")
    private EntityManager _EntityManager;

    private FullTextEntityManager _FullTextEntityManager;

    public List<SearchResult> search(String alias, String keyword) {
        List<SearchResult> results = null;

        _FullTextEntityManager = Search.getFullTextEntityManager(_EntityManager);
        try {
            _FullTextEntityManager.createIndexer().startAndWait();
        } catch (Throwable throwable) {
            logger.error(throwable.toString());
            _FullTextEntityManager = null;
        }

        if (_FullTextEntityManager != null) {
            QueryBuilder queryBuilder = _FullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(SearchResult.class).get();

            Query friendlyNameQry = null;
            if (alias != null && alias.length() > 0 && (alias.contains("?") || alias.contains("*")))
                friendlyNameQry = queryBuilder.keyword().wildcard().onField("keywordSourceAlias").matching(alias).createQuery();
            else if (alias != null && alias.length() > 0)
                friendlyNameQry = queryBuilder.keyword().fuzzy().onField("keywordSourceAlias").matching(alias).createQuery();

            Query searchValueQry = null;
            try {
                if (keyword != null && keyword.length() > 0 && (keyword.contains("?") || keyword.contains("*")))
                    searchValueQry = queryBuilder.keyword().wildcard().onField("keyword").matching(keyword).createQuery();
                else if (keyword != null && keyword.length() > 0)
                    searchValueQry = queryBuilder.keyword().fuzzy().onField("keyword").matching(keyword).createQuery();
            } catch (Throwable t) {
                logger.error(t.getMessage());
            }

            Query finalQry;
            if (friendlyNameQry != null && searchValueQry != null)
                finalQry = queryBuilder.bool().must(friendlyNameQry).must(searchValueQry).createQuery();
            else if (friendlyNameQry != null)
                finalQry = friendlyNameQry;
            else if (searchValueQry != null)
                finalQry = searchValueQry;
            else
                finalQry = queryBuilder.all().createQuery();

            FullTextQuery fullTextQuery = _FullTextEntityManager.createFullTextQuery(finalQry, SearchResult.class);
            results = fullTextQuery.getResultList();
        }

        return results;
    }
}
