package io.salte.cc.repositories;

import io.salte.cc.models.PersonRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("PersonRoleRepository")
public interface PersonRoleRepository extends JpaRepository<PersonRole, Integer> {
}
