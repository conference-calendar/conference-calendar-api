package io.salte.cc.repositories;

import io.salte.cc.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("PeopleRepository")
public interface PeopleRepository extends JpaRepository<Person, Integer> {
    List<Person> findAllBySchoolId(Integer schoolId);
    Person findOneByEmail(String email);
}
