package io.salte.cc.exceptions;

public class BadClientRequest extends RuntimeException {
    public BadClientRequest() {
        super();
    }

    public BadClientRequest(String message) {
        super(message);
    }
}
