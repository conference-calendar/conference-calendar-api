package io.salte.cc.exceptions;

public class UnauthorizedApiAccessException extends RuntimeException {
    public UnauthorizedApiAccessException() {
        super();
    }

    public UnauthorizedApiAccessException(String message) {
        super(message);
    }
}
