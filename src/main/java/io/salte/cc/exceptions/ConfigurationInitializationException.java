package io.salte.cc.exceptions;

public class ConfigurationInitializationException extends RuntimeException {
    public ConfigurationInitializationException() {
        super();
    }

    public ConfigurationInitializationException(String message) {
        super(message);
    }
}
