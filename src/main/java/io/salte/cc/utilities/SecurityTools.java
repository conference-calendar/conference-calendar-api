package io.salte.cc.utilities;

import io.salte.cc.configuration.ConfigurationSettings;

import javax.json.JsonObject;
import java.security.SecureRandom;
import java.util.Random;

public class SecurityTools {
    private static final Random RANDOM = new SecureRandom();
    public static final int PASSWORD_LENGTH = 8;

    public static boolean isAdminUser(JsonObject userProfile) {
        return (userProfile.getString(String.format("%s/%s", ConfigurationSettings.getInstance().getClaimsNamespace(), ConfigurationSettings.getInstance().getSetting("gateway_jwt_role"))).contains(ConfigurationSettings.getInstance().getSetting("gateway_jwt_role_admin")));
    }

    public static String generateRandomPassword()
    {
        // Pick from some letters that won't be easily mistaken for each
        // other. So, for example, omit o O and 0, 1 l and L.
        String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";

        String pw = "";
        for (int i=0; i<PASSWORD_LENGTH; i++)
        {
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
        return pw;
    }
}
