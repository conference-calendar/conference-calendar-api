package io.salte.cc.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTools {
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern email_pattern;

    public static boolean isValidEmail(String email) {
        if (email != null) {
            Pattern email_pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher email_matcher = email_pattern.matcher(email);
            return email_matcher.matches();
        } else
            return false;
    }
}
