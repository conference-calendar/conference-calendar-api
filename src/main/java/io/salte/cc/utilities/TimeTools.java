package io.salte.cc.utilities;

import java.sql.Time;

public class TimeTools {
    public static Time addMinutes(Time value, int minutes) {
        Integer thisHours = Integer.parseInt(value.toString().substring(0, 2));
        Integer thisMinutes = Integer.parseInt(value.toString().substring(3, 5));
        thisMinutes = thisMinutes + minutes;
        thisHours = thisHours + (thisMinutes / 60);
        thisMinutes = thisMinutes % 60;
        return Time.valueOf(String.format("%s:%s:00", thisHours, thisMinutes));
    }
}
