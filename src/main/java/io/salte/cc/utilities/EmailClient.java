package io.salte.cc.utilities;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailClient {

    public static void sendEmail(String email, String subject, String message) throws MessagingException {

        final String username = "username@gmail.com";
        final String password = "password";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.live.com");
        props.put("mail.smtp.port", "25");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("davidwoodwrd@hotmail.com", "Zummer#69");
                    }
                });

        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("admin@salte.io"));
        msg.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(email));
        msg.setSubject(subject);
        msg.setText(message);
        Transport.send(msg);
    }
}