package io.salte.cc.services;

import io.salte.cc.models.ConferenceAttendee;
import io.salte.cc.repositories.ConferenceAttendeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ConferenceAttendeeService")
public class ConferenceAttendeeService implements IConferenceAttendeeService {
    @Autowired
    private ConferenceAttendeeRepository _ConferenceAttendeeRepository;

    @Override
    public ConferenceAttendee getConferenceAttendee(Integer conferenceAttendeeId) {
        return _ConferenceAttendeeRepository.findOne(conferenceAttendeeId);
    }

    @Override
    public List<ConferenceAttendee> getConferenceAttendees() {
        return _ConferenceAttendeeRepository.findAll();
    }

    @Override
    public ConferenceAttendee saveConferenceAttendee(ConferenceAttendee conferenceAttendee) {
        return _ConferenceAttendeeRepository.save(conferenceAttendee);
    }

    @Override
    public void deleteConferenceAttendee(Integer conferenceAttendeeId) {
        _ConferenceAttendeeRepository.delete(conferenceAttendeeId);
    }

    @Override
    public void deleteConferenceAttendees(List<ConferenceAttendee> conferenceAttendees) {
        _ConferenceAttendeeRepository.delete(conferenceAttendees);
    }

    @Override
    public List<ConferenceAttendee> getConferenceAttendeesWithRole(Integer personRoleId) {
        return _ConferenceAttendeeRepository.findAllByPersonRoleId(personRoleId);
    }

    @Override
    public List<ConferenceAttendee> getConferencesAttendedByPerson(Integer personId) {
        return _ConferenceAttendeeRepository.findAllByPersonId(personId);
    }

    @Override
    public List<ConferenceAttendee> getConferenceAttendees(Integer conferenceId) {
        return _ConferenceAttendeeRepository.findAllByConferenceId(conferenceId);
    }
}
