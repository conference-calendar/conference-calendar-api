package io.salte.cc.services;

import io.salte.cc.models.PersonRole;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface IPersonRoleService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    PersonRole getPersonRole(Integer personRoleId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    List<PersonRole> getPersonRoleList();

    @PreAuthorize("hasRole('admin')")
    PersonRole savePersonRole(PersonRole personRole);

    @PreAuthorize("hasRole('admin')")
    void deletePersonRole(Integer personRoleId);

    @PreAuthorize("hasRole('admin')")
    void deletePersonRole(List<PersonRole> personRoles);
}
