package io.salte.cc.services;

import io.salte.cc.models.Person;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IPeopleService {
    //Need to retrieve before the user is authenticated.
    Person getPerson(String email);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    Person getPerson(Integer personId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Person> getPersonList();

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    Person savePerson(Person person);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deletePerson(Integer personId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    void deletePerson(List<Person> people);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Person> getPersonList(Integer schoolId);
}
