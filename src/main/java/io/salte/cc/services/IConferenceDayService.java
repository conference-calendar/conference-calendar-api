package io.salte.cc.services;

import io.salte.cc.models.ConferenceDay;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IConferenceDayService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    ConferenceDay getConferenceDay(Integer conferenceDayId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceDay> getConferenceDayList();

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceDay> getConferenceDayList(Integer conferenceSessionId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    ConferenceDay saveConferenceDay(ConferenceDay conferenceDay);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deleteConferenceDay(Integer sessionDayId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    void deleteConferenceDays(List<ConferenceDay> conferenceDays);
}
