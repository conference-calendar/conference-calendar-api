package io.salte.cc.services;

import io.salte.cc.exceptions.ConfigurationInitializationException;
import io.salte.cc.external.wso2.exceptions.BadUserAdminServiceRequest;
import io.salte.cc.models.Person;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface IUserAdminService {
    // Only Admin can call through controller.  Otherwise, this is called via IPeopleService and locked down there.
    void addUser(String username, String password);

    // Only Admin can call through controller.  Otherwise, this is called via IPeopleService and locked down there.
    String getUser(String username) throws BadUserAdminServiceRequest, ConfigurationInitializationException;

    // Only Admin can call through controller.  Otherwise, this is called via IPeopleService and locked down there.
    List<String> getUserList(Integer maxUsers) throws BadUserAdminServiceRequest;

    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void resetPassword(String username, String newPassword);

    // If you know someone's username and password then why shouldn't we let you change it.
    void changePassword(String username, String currentPassword, String newPassword);

    // Only Admin can call through controller.  Otherwise, this is called via IPeopleService and locked down there.
    void updateUser(String username, String newUsername);

    // Only Admin can call through controller.  Otherwise, this is called via IPeopleService and locked down there.
    void deleteUser(String username);
}
