package io.salte.cc.services;

import io.salte.cc.models.SecurityRole;
import io.salte.cc.repositories.SecurityRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("SecurityRoleService")
public class SecurityRoleService implements ISecurityRoleService {
    @Autowired
    SecurityRoleRepository _SecurityRoleRepository;

    @Override
    public SecurityRole getSecurityRole(Integer securityRoleId) {
        return _SecurityRoleRepository.findOne(securityRoleId);
    }

    @Override
    public List<SecurityRole> getSecurityRoleList() {
        return _SecurityRoleRepository.findAll();
    }

    @Override
    public SecurityRole saveSecurityRole(SecurityRole securityRole) {
        return _SecurityRoleRepository.saveAndFlush(securityRole);
    }

    @Override
    public void deleteSecurityRole(Integer securityRoleId) {
        _SecurityRoleRepository.delete(securityRoleId);
    }

    @Override
    public void deleteSecurityRole(List<SecurityRole> securityRoleList) {
        _SecurityRoleRepository.delete(securityRoleList);
    }
}
