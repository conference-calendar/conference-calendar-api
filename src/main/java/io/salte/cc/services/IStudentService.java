package io.salte.cc.services;

import io.salte.cc.models.Student;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IStudentService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    Student getStudent(Integer personId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Student> getStudentList();

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    Student saveStudent(Student student);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deleteStudent(Integer personId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    void deleteStudent(List<Student> students);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Student> getStudentList(Integer schoolId);
}
