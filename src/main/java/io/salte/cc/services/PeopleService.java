package io.salte.cc.services;


import io.salte.cc.models.Person;
import io.salte.cc.repositories.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("PeopleService")
public class PeopleService implements IPeopleService {
    @Autowired
    private PeopleRepository _PeopleRepository;

    @Override
    public Person getPerson(String email) {
        return _PeopleRepository.findOneByEmail(email);
    }

    @Override
    public Person getPerson(Integer personId) {
        return _PeopleRepository.findOne(personId);
    }

    @Override
    public List<Person> getPersonList() {
        return _PeopleRepository.findAll();
    }

    @Override
    @Transactional
    public Person savePerson(Person person) {
        return _PeopleRepository.saveAndFlush(person);
    }

    @Override
    @Transactional
    public void deletePerson(Integer personId) {
        _PeopleRepository.delete(personId);
    }

    @Override
    @Transactional
    public void deletePerson(List<Person> people) {
        _PeopleRepository.delete(people);
    }

    @Override
    public List<Person> getPersonList(Integer schoolId) {
        return _PeopleRepository.findAllBySchoolId(schoolId);
    }
}
