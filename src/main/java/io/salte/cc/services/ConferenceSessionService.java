package io.salte.cc.services;

import io.salte.cc.models.ConferenceSession;
import io.salte.cc.repositories.ConferenceSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ConferenceSessionService")
public class ConferenceSessionService implements IConferenceSessionService {
    @Autowired
    private ConferenceSessionRepository _ConferenceSessionRepository;

    @Override
    public ConferenceSession getConferenceSession(Integer conferenceSessionId) {
        return _ConferenceSessionRepository.findOne(conferenceSessionId);
    }

    @Override
    public List<ConferenceSession> getConferenceSessionList() {
        return _ConferenceSessionRepository.findAll();
    }

    @Override
    public List<ConferenceSession> getConferenceSessionList(Integer schoolId) {
        return _ConferenceSessionRepository.findAllBySchoolId(schoolId);
    }

    @Transactional
    @Override
    public ConferenceSession saveConferenceSession(ConferenceSession conferenceSession) {
        return _ConferenceSessionRepository.saveAndFlush(conferenceSession);
    }

    @Transactional
    @Override
    public void deleteConferenceSession(Integer conferenceSessionId) {
        _ConferenceSessionRepository.delete(conferenceSessionId);
    }

    @Transactional
    @Override
    public void deleteConferenceSession(List<ConferenceSession> conferenceSessions) {
        _ConferenceSessionRepository.delete(conferenceSessions);
    }
}
