package io.salte.cc.services;

import io.salte.cc.configuration.ConfigurationSettings;
import io.salte.cc.external.wso2.exceptions.BadUserAdminServiceRequest;
import io.salte.cc.external.wso2.exceptions.ForbiddenUserAdminServiceRequest;
import io.salte.cc.external.wso2.exceptions.InvalidUserAdminServiceRequest;
import io.salte.cc.external.wso2.exceptions.UserAdminServiceError;
import io.salte.cc.external.wso2.models.User;
import io.salte.cc.external.wso2.models.UserAttribute;
import io.salte.cc.external.wso2.repositories.UserAttributeRepository;
import io.salte.cc.external.wso2.repositories.UserRepository;
import io.salte.cc.external.wso2.services.ClaimValue;
import io.salte.cc.external.wso2.services.UserAdminSoap11BindingStub;
import io.salte.cc.external.wso2.utilities.Constants;
import io.salte.cc.utilities.StringTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service("UserAdminService")
public class UserAdminService implements IUserAdminService {
    @Autowired
    private UserRepository _UserRepository;

    @Autowired
    private UserAttributeRepository _UserAttributeRepository;

    private static final Logger logger = LogManager.getLogger(UserAdminService.class);
    private static final String PWD_PATTERN = "^.*(?=.{8,})(?=.*\\d)(?=.*[A-Za-z]).*$";
    private Pattern password_pattern;

    public UserAdminService() {
        password_pattern = Pattern.compile(PWD_PATTERN);
    }

    @Override
    public void addUser(String username, String password) {
        if (!StringTools.isValidEmail(username))
            throw new BadUserAdminServiceRequest(String.format("Unable to add the username '%s' because it isn't a valid email address.", username));

        try {
            String existingUser = getUser(username);
            throw new BadUserAdminServiceRequest(String.format("Unable to add the username '%s' because it already exists.", username));
        } catch (InvalidUserAdminServiceRequest ex) {
            // This is a good thing since we want to use the username we just searched for.
        }

        if (!password_pattern.matcher(password).matches())
            throw new BadUserAdminServiceRequest(String.format(String.format("Unable to add the username '%s' because the password doesn't meet strength requirements. It must be at least 8 characters long and contain at least one digit.", username)));

        try {
            UserAdminSoap11BindingStub service = new UserAdminSoap11BindingStub();
            service._setProperty("javax.xml.rpc.security.auth.username", ConfigurationSettings.getInstance().getSetting("gateway_username"));
            service._setProperty("javax.xml.rpc.security.auth.password", ConfigurationSettings.getInstance().getSetting("gateway_password"));
            service._setProperty("javax.xml.rpc.service.endpoint.address", ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url"));

            service.addUser(username, password, null, new ClaimValue[] { new ClaimValue(ConfigurationSettings.getInstance().getEmailClaimIndex(), username) }, null);
        } catch (RemoteException ex) {
            UserAdminServiceError userAdminServiceError = new UserAdminServiceError(String.format("Unable to add user '%s' to user store.", username));
            userAdminServiceError.initCause(ex);
            logger.error(userAdminServiceError.toString());
            throw userAdminServiceError;
        }
    }

    @Override
    public String getUser(String username) {
        try {
            UserAdminSoap11BindingStub service = new UserAdminSoap11BindingStub();
            service._setProperty("javax.xml.rpc.security.auth.username", ConfigurationSettings.getInstance().getSetting("gateway_username"));
            service._setProperty("javax.xml.rpc.security.auth.password", ConfigurationSettings.getInstance().getSetting("gateway_password"));
            service._setProperty("javax.xml.rpc.service.endpoint.address", ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url"));
            String[] usernames = service.listUsers(username, Constants.getDefaultMaxUsers());
            if (usernames != null && usernames.length > 1)
                throw new UserAdminServiceError(String.format("The username provided is not unique! %d usernames were found that match the provided value.", usernames.length));
            else if (usernames != null && usernames.length == 1)
                return usernames[0];
            else
                throw new InvalidUserAdminServiceRequest(String.format("The username '%s' does not exist.", username));
        } catch (RemoteException ex) {
            UserAdminServiceError userAdminServiceError = new UserAdminServiceError(String.format("Unable to retrieve user %s from user store.", username));
            userAdminServiceError.initCause(ex);
            logger.error(userAdminServiceError.toString());
            logger.error(ex.toString());
            throw userAdminServiceError;
        }
    }

    @Override
    public List<String> getUserList(Integer maxUsers) {
        try {
            UserAdminSoap11BindingStub service = new UserAdminSoap11BindingStub();
            service._setProperty("javax.xml.rpc.security.auth.username", ConfigurationSettings.getInstance().getSetting("gateway_username"));
            service._setProperty("javax.xml.rpc.security.auth.password", ConfigurationSettings.getInstance().getSetting("gateway_password"));
            service._setProperty("javax.xml.rpc.service.endpoint.address", ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url"));
            String[] names = service.listUsers("", maxUsers);
            ArrayList<String> usernames = new ArrayList<String>();
            for (String name : names) {
                if (!name.equals(ConfigurationSettings.getInstance().getSetting("gateway_username")))
                    usernames.add(name);
            }
            return usernames;
        } catch (RemoteException ex) {
            UserAdminServiceError userAdminServiceError = new UserAdminServiceError("Unable to retrieve users from user store.");
            userAdminServiceError.initCause(ex);
            logger.error(userAdminServiceError.toString());
            logger.error(ex.toString());
            throw userAdminServiceError;
        }
    }

    @Override
    public void resetPassword(String username, String newPassword) {
        // Verify User Exists
        username = getUser(username);

        if (!password_pattern.matcher(newPassword).matches())
            throw new BadUserAdminServiceRequest(String.format(String.format("Unable to reset the password for '%s' because the new password doesn't meet strength requirements. It must be at least 8 characters long and contain at least one digit.", username)));

         try {
            UserAdminSoap11BindingStub service = new UserAdminSoap11BindingStub();
            service._setProperty("javax.xml.rpc.security.auth.username", ConfigurationSettings.getInstance().getSetting("gateway_username"));
            service._setProperty("javax.xml.rpc.security.auth.password", ConfigurationSettings.getInstance().getSetting("gateway_password"));
            service._setProperty("javax.xml.rpc.service.endpoint.address", ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url"));
            service.changePassword(username, newPassword);
        } catch (RemoteException ex) {
            UserAdminServiceError userAdminServiceError = new UserAdminServiceError(String.format("Unable to reset password for \"%s.\"", username));
            userAdminServiceError.initCause(ex);
            logger.error(userAdminServiceError.toString());
            throw userAdminServiceError;
        }
    }

    @Override
    public void changePassword(String username, String currentPassword, String newPassword) {
        // Verify User Exists
        username = getUser(username);

        if (!password_pattern.matcher(newPassword).matches())
            throw new BadUserAdminServiceRequest(String.format("Unable to change the password for '%s' because the new password doesn't meet strength requirements. It must be at least 8 characters long and contain at least one digit.", username));

        try {
            UserAdminSoap11BindingStub service = new UserAdminSoap11BindingStub();
            service._setProperty("javax.xml.rpc.security.auth.username", username);
            service._setProperty("javax.xml.rpc.security.auth.password", currentPassword);
            service._setProperty("javax.xml.rpc.service.endpoint.address", ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url"));
            service.changePasswordByUser(currentPassword, newPassword);
        } catch (RemoteException ex) {
            UserAdminServiceError userAdminServiceError = new UserAdminServiceError(String.format("Unable to change password for \"%s.\"", username));
            userAdminServiceError.initCause(ex);
            logger.error(userAdminServiceError.toString());
            throw userAdminServiceError;
        }
    }

    @Override
    public void updateUser(String username, String newUsername) {
        // Verify User Exists
        username = getUser(username);

        if (!StringTools.isValidEmail(newUsername))
            throw new BadUserAdminServiceRequest(String.format("Unable to update to the specified username '%s' because it isn't a valid email address.", newUsername));

        try {
            String existingUser = getUser(newUsername);
            throw new BadUserAdminServiceRequest(String.format("Unable to update to the specified username '%s' because it already exists.", newUsername));
        } catch (InvalidUserAdminServiceRequest ex) {
            // This is a good thing since we want to use the username we just searched for.
        }

        User user =  _UserRepository.findOneByUsername(username);
        user.setUsername(newUsername);
        _UserRepository.saveAndFlush(user);

        UserAttribute userAttribute = _UserAttributeRepository.findOneByUserIdAndAttributeName(user.getUserId(), "mail");
        userAttribute.setAttributeValue(newUsername);
        _UserAttributeRepository.saveAndFlush(userAttribute);
    }

    @Override
    public void deleteUser(String username) {
        try {
            if (username.equals(ConfigurationSettings.getInstance().getSetting("gateway_username")))
                throw new ForbiddenUserAdminServiceRequest(String.format("The specified user '%s' cannot be deleted.", username));

            // Verify User Exists...
            username = getUser(username);

            UserAdminSoap11BindingStub service = new UserAdminSoap11BindingStub();
            service._setProperty("javax.xml.rpc.security.auth.username", ConfigurationSettings.getInstance().getSetting("gateway_username"));
            service._setProperty("javax.xml.rpc.security.auth.password", ConfigurationSettings.getInstance().getSetting("gateway_password"));
            service._setProperty("javax.xml.rpc.service.endpoint.address", ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url"));
            service.deleteUser(username);
        } catch (RemoteException ex) {
            UserAdminServiceError userAdminServiceError = new UserAdminServiceError(String.format("Unable to delete user %s from user store.", username));
            userAdminServiceError.initCause(ex);
            logger.error(userAdminServiceError.toString());
            throw userAdminServiceError;
        }
    }
}
