package io.salte.cc.services;

import io.salte.cc.models.Student;
import io.salte.cc.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("StudentService")
public class StudentService implements IStudentService {
    @Autowired
    private StudentRepository _StudentRepository;
    
    @Override
    public Student getStudent(Integer classroomId) {
        return _StudentRepository.findOne(classroomId);
    }
    
    @Override
    public List<Student> getStudentList() {

        return _StudentRepository.findAll();
    }
    
    @Override
    @Transactional
    public Student saveStudent(Student student) {
        return _StudentRepository.saveAndFlush(student);
    }
    
    @Override
    @Transactional
    public void deleteStudent(Integer classroomId) {

        _StudentRepository.delete(classroomId);
    }
    
    @Override
    @Transactional
    public void deleteStudent(List<Student> Students) {

        _StudentRepository.delete(Students);
    }
    
    @Override
    public List<Student> getStudentList(Integer studentId) {
        return _StudentRepository.findAllBySchoolId(studentId);
    }
}
