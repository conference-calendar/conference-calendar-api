package io.salte.cc.services;

import io.salte.cc.models.School;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ISchoolService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    School getSchool(Integer schoolId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<School> getSchoolList();

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    School saveSchool(School school);

    @Transactional
    @PreAuthorize("hasRole('admin')")
    void deleteSchool(Integer schoolId);

    @Transactional
    @PreAuthorize("hasRole('admin')")
    void deleteSchool(List<School> schools);
}
