package io.salte.cc.services;

import io.salte.cc.models.Classroom;
import io.salte.cc.repositories.ClassroomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ClassroomService")
public class ClassroomService implements IClassroomService {
    @Autowired
    private ClassroomRepository _ClassroomRepository;

    @Override
    public Classroom getClassroom(Integer classroomId) {
        return _ClassroomRepository.findOne(classroomId);
    }

    @Override
    public List<Classroom> getClassroomList() {

        return _ClassroomRepository.findAll();
    }

    @Transactional
    @Override
    public Classroom saveClassroom(Classroom classroom) {

        return _ClassroomRepository.saveAndFlush(classroom);
    }

    @Transactional
    @Override
    public void deleteClassroom(Integer classroomId) {

        _ClassroomRepository.delete(classroomId);
    }

    @Transactional
    @Override
    public void deleteClassrooms(List<Classroom> classrooms) {

        _ClassroomRepository.delete(classrooms);
    }

    @Override
    public List<Classroom> getClassroomList(Integer schoolId) {
        return _ClassroomRepository.findAllBySchoolId(schoolId);
    }
}
