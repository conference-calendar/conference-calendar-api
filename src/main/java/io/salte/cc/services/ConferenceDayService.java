package io.salte.cc.services;

import io.salte.cc.models.ConferenceDay;
import io.salte.cc.repositories.ConferenceDayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ConferenceDayService")
public class ConferenceDayService implements IConferenceDayService {
    @Autowired
    private ConferenceDayRepository _ConferenceDayRepository;

    @Override
    public ConferenceDay getConferenceDay(Integer conferenceDayId) {
        return _ConferenceDayRepository.findOne(conferenceDayId);
    }

    @Override
    public List<ConferenceDay> getConferenceDayList() {
        return _ConferenceDayRepository.findAll();
    }

    @Override
    public List<ConferenceDay> getConferenceDayList(Integer conferenceSessionId) {
        return _ConferenceDayRepository.findAllByConferenceSessionIdOrderByDateAsc(conferenceSessionId);
    }

    @Transactional
    @Override
    public ConferenceDay saveConferenceDay(ConferenceDay conferenceDay) {
        return _ConferenceDayRepository.saveAndFlush(conferenceDay);
    }

    @Transactional
    @Override
    public void deleteConferenceDay(Integer sessionDayId) {
        _ConferenceDayRepository.delete(sessionDayId);
    }

    @Transactional
    @Override
    public void deleteConferenceDays(List<ConferenceDay> conferenceDays) {
        _ConferenceDayRepository.delete(conferenceDays);
    }
}
