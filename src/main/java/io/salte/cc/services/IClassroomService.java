package io.salte.cc.services;

import io.salte.cc.models.Classroom;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IClassroomService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    Classroom getClassroom(Integer classroomId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    List<Classroom> getClassroomList();

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    Classroom saveClassroom(Classroom classroom);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deleteClassroom(Integer classroomId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    void deleteClassrooms(List<Classroom> classrooms);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    List<Classroom> getClassroomList(Integer schoolId);
}
