package io.salte.cc.services;

import io.salte.cc.models.ConferenceAttendee;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IConferenceAttendeeService {
    ConferenceAttendee getConferenceAttendee(Integer personId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceAttendee> getConferenceAttendees();

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    ConferenceAttendee saveConferenceAttendee(ConferenceAttendee conferenceAttendee);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deleteConferenceAttendee(Integer attendeeId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    void deleteConferenceAttendees(List<ConferenceAttendee> conferenceAttendees);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceAttendee> getConferenceAttendeesWithRole(Integer personRoleId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceAttendee> getConferencesAttendedByPerson(Integer personId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.schoolId == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceAttendee> getConferenceAttendees(Integer conferenceId);
}
