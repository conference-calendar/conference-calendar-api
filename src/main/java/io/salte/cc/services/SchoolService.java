package io.salte.cc.services;

import io.salte.cc.models.School;
import io.salte.cc.repositories.SchoolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service("SchoolService")
public class SchoolService implements ISchoolService {
    @Autowired
    private SchoolRepository _SchoolRepository;

    @Override
    public School getSchool(Integer schoolId) {
        return _SchoolRepository.findOne(schoolId);
    }

    @Override
    public List<School> getSchoolList() {

        return _SchoolRepository.findAll();
    }

    @Transactional
    @Override
    public School saveSchool(School school) {
        return _SchoolRepository.saveAndFlush(school);
    }

    @Transactional
    @Override
    public void deleteSchool(Integer schoolId) {
        _SchoolRepository.delete(schoolId);
    }

    @Transactional
    @Override
    public void deleteSchool(List<School> schools) {
        _SchoolRepository.delete(schools);
    }
}
