package io.salte.cc.services;

import io.salte.cc.models.Conference;
import io.salte.cc.repositories.ConferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ConferenceService")
public class ConferenceService implements IConferenceService {
    @Autowired
    ConferenceRepository _ConferenceRepository;

    @Override
    public Conference getConference(Integer conferenceId) {
        return _ConferenceRepository.findOne(conferenceId);
    }

    @Override
    public List<Conference> getConferenceList() {
        return _ConferenceRepository.findAll();
    }

    @Override
    public Conference saveConference(Conference conference) {
        return _ConferenceRepository.save(conference);
    }

    @Override
    public void deleteConference(Integer conferenceId) {
        _ConferenceRepository.delete(conferenceId);
    }

    @Override
    public void deleteConference(List<Conference> conferences) {
        _ConferenceRepository.delete(conferences);
    }

    @Override
    public List<Conference> getConferencesByDay(Integer conferenceDayId) {
        return _ConferenceRepository.findAllByConferenceDayId(conferenceDayId);
    }

    @Override
    public List<Conference> getConferencesByTeacher(Integer personId) {
        return _ConferenceRepository.findAllByTeacherId(personId);
    }

    @Override
    public List<Conference> getConferencesByStudent(Integer personId) {
        return _ConferenceRepository.findAllByStudentId(personId);
    }

    @Override
    public List<Conference> getConferencesByClassroom(Integer classroomId) {
        return _ConferenceRepository.findAllByClassroomId(classroomId);
    }

    @Override
    public List<Conference> getConferencesByConferenceDayIdAndTeacherId(Integer conferenceDayId, Integer personId) {
        return _ConferenceRepository.findAllByConferenceDayIdAndTeacherId(conferenceDayId, personId);
    }
}
