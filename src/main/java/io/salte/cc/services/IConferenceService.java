package io.salte.cc.services;

import io.salte.cc.models.Conference;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IConferenceService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    Conference getConference(Integer conferenceId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Conference> getConferenceList();

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    Conference saveConference(Conference conference);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deleteConference(Integer conferenceId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    void deleteConference(List<Conference> conferences);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Conference> getConferencesByDay(Integer conferenceDayId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Conference> getConferencesByTeacher(Integer personId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Conference> getConferencesByStudent(Integer personId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Conference> getConferencesByClassroom(Integer classroomId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<Conference> getConferencesByConferenceDayIdAndTeacherId(Integer conferenceDayId, Integer personId);
}
