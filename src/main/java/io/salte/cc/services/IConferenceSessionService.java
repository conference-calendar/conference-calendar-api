package io.salte.cc.services;

import io.salte.cc.models.ConferenceSession;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IConferenceSessionService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    ConferenceSession getConferenceSession(Integer sessionId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceSession> getConferenceSessionList();

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<ConferenceSession> getConferenceSessionList(Integer schoolId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    ConferenceSession saveConferenceSession(ConferenceSession conferenceSession);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    void deleteConferenceSession(Integer conferenceSessionId);

    @Transactional
    @PreAuthorize("hasRole('admin') || hasRole('school_admin')")
    @PreFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    void deleteConferenceSession(List<ConferenceSession> conferenceSessions);
}
