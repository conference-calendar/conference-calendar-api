package io.salte.cc.services;

import io.salte.cc.models.SearchResult;
import io.salte.cc.repositories.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("SearchService")
public class SearchService implements ISearchService {
    @Autowired
    private SearchRepository _SearchRepository;

    @Override
    public List<SearchResult> getSearchResultList(String friendlyName, String searchValue) {
        return _SearchRepository.search(friendlyName, searchValue);
    }
}
