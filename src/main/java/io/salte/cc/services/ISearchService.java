package io.salte.cc.services;

import io.salte.cc.models.SearchResult;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface ISearchService {
    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    @PostFilter("filterObject.identity == authentication.schoolId || authentication.schoolId == null")
    List<SearchResult> getSearchResultList(String friendlyName, String searchValue);
}

