package io.salte.cc.services;

import io.salte.cc.models.PersonRole;
import io.salte.cc.repositories.PersonRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("PersonRoleService")
public class PersonRoleService implements IPersonRoleService {
    @Autowired
    private PersonRoleRepository _PersonRoleRepository;

    @Override
    public PersonRole getPersonRole(Integer personRoleId) {
        return _PersonRoleRepository.findOne(personRoleId);
    }

    @Override
    public List<PersonRole> getPersonRoleList() {
        return _PersonRoleRepository.findAll();
    }

    @Override
    public PersonRole savePersonRole(PersonRole personRole) {
        return _PersonRoleRepository.save(personRole);
    }

    @Override
    public void deletePersonRole(Integer personRoleId) {
        _PersonRoleRepository.delete(personRoleId);
    }

    @Override
    public void deletePersonRole(List<PersonRole> personRoles) {
        _PersonRoleRepository.delete(personRoles);
    }
}
