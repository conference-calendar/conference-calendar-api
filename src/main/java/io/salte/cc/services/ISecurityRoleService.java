package io.salte.cc.services;

import io.salte.cc.models.SecurityRole;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ISecurityRoleService {
    //Need to retrieve before the user is authenticated.
    SecurityRole getSecurityRole(Integer securityRoleId);

    @PreAuthorize("hasRole('admin') || hasRole('school_admin') || hasRole('teacher') || hasRole('parent') || hasRole('student')")
    List<SecurityRole> getSecurityRoleList();

    @Transactional
    @PreAuthorize("hasRole('admin')")
    SecurityRole saveSecurityRole(SecurityRole securityRole);

    @Transactional
    @PreAuthorize("hasRole('admin')")
    void deleteSecurityRole(Integer securityRoleId);

    @Transactional
    @PreAuthorize("hasRole('admin')")
    void deleteSecurityRole(List<SecurityRole> securityRoleList);
}
