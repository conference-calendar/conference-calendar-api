package io.salte.cc.services;

import io.salte.cc.models.Teacher;
import io.salte.cc.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("TeacherService")
public class TeacherService implements ITeacherService {
    @Autowired
    private TeacherRepository _TeacherRepository;

    @Override
    public Teacher getTeacher(Integer teacherId) {
        return _TeacherRepository.findOne(teacherId);
    }

    @Override
    public List<Teacher> getTeacherList() {

        return _TeacherRepository.findAll();
    }

    @Override
    @Transactional
    public Teacher saveTeacher(Teacher teacher) {
        return _TeacherRepository.saveAndFlush(teacher);
    }

    @Override
    @Transactional
    public void deleteTeacher(Integer personId) {

        _TeacherRepository.delete(personId);
    }

    @Override
    @Transactional
    public void deleteTeacher(List<Teacher> teachers) {

        _TeacherRepository.delete(teachers);
    }

    @Override
    public List<Teacher> getTeacherList(Integer schoolId) {
        return _TeacherRepository.findAllBySchoolId(schoolId);
    }
}
