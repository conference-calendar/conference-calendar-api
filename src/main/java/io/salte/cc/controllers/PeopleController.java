package io.salte.cc.controllers;

import io.salte.cc.configuration.ConfigurationSettings;
import io.salte.cc.exceptions.BadClientRequest;
import io.salte.cc.exceptions.InternalServerError;
import io.salte.cc.external.wso2.exceptions.UserAdminServiceError;
import io.salte.cc.external.wso2.utilities.Constants;
import io.salte.cc.models.Person;
import io.salte.cc.models.School;
import io.salte.cc.models.SecurityRole;
import io.salte.cc.services.IPeopleService;
import io.salte.cc.services.UserAdminService;
import io.salte.cc.utilities.EmailClient;
import io.salte.cc.utilities.SecurityTools;
import io.salte.cc.utilities.StringTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonObject;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Person.class)
public class PeopleController {
    @Autowired
    private IPeopleService _PeopleService;

    @Autowired
    private UserAdminService _UserAdminService;

    @RequestMapping(value = "/people/{personId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getPerson(HttpSession session, @PathVariable Integer personId) {
        if (personId == 1) {
            Person person = extractAdminUser((JsonObject) session.getAttribute("userProfile"));
            addLink(person);
            return new ResponseEntity<>(person, HttpStatus.OK);
        } else {
            Person person = _PeopleService.getPerson(personId);
            if (person != null) {
                addLink(person);
                return new ResponseEntity<>(person, HttpStatus.OK);
            } else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/people/current", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getPerson(HttpSession session) {
        String email = String.format("%s/%s", ConfigurationSettings.getInstance().getClaimsNamespace(), ConfigurationSettings.getInstance().getSetting("gateway_jwt_email"));

        JsonObject userProfile = (JsonObject) session.getAttribute("userProfile");
        Person person = _PeopleService.getPerson(userProfile.getString(email));
        if (person != null) {
            addLink(person);
            return new ResponseEntity<>(person, HttpStatus.OK);
        } else if (SecurityTools.isAdminUser(userProfile)) {
            person = extractAdminUser(userProfile);
            addLink(person);
            return new ResponseEntity<>(person, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    private Person extractAdminUser(JsonObject userProfile) {
        String claimsNamespace = ConfigurationSettings.getInstance().getSetting("gateway_jwt_namespace");
        String email = String.format("%s/%s", claimsNamespace, ConfigurationSettings.getInstance().getSetting("gateway_jwt_email"));

        Person person = new Person();
        person.setIdentity(-1);
        person.setUserFlag(true);
        person.setEmail(userProfile.getString(email));
        person.setFirstName(userProfile.getString(String.format("%s/%s", claimsNamespace, ConfigurationSettings.getInstance().getSetting("gateway_jwt_first"))));
        person.setLastName(userProfile.getString(String.format("%s/%s", claimsNamespace, ConfigurationSettings.getInstance().getSetting("gateway_jwt_last"))));
        person.setSecurityRoleId(-1);

        return person;
    }

    @RequestMapping(value = "/people", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Person> getPersonList() {
        List<Person> people = _PeopleService.getPersonList();
        addLinks(people);
        return new Resources<>(people, linkTo(methodOn(PeopleController.class).getPersonList()).withSelfRel());
    }

    @RequestMapping(value = "/schools/{schoolId}/people", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Person> getPersonList(@PathVariable Integer schoolId) {
        List<Person> people = _PeopleService.getPersonList(schoolId);
        addLinks(people);
        return new Resources<>(people, linkTo(methodOn(PeopleController.class).getPersonList(schoolId)).withSelfRel());
    }

    @RequestMapping(value = "/people", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createPerson(@RequestBody @Valid Person person, @RequestParam(name = "appHomePage", required = false) String appHomePage) {
        if (person.getIdentity() == null ) {
            if (person.getUserFlag() && !StringTools.isValidEmail(person.getEmail()))
                throw new BadClientRequest("An email address must be provided when the user flag is selected.");

            Person savedPerson = _PeopleService.savePerson(person);

            if (savedPerson.getUserFlag())
                addUserAfterSave(savedPerson, appHomePage);

            addLink(savedPerson);
            return new ResponseEntity<>(savedPerson, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/people/{personId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> savePerson(@RequestBody @Valid Person person, @PathVariable Integer personId, @RequestParam(name = "appHomePage", required = false) String appHomePage) {
        if (person.getIdentity() != null && personId != null && person.getIdentity().equals(personId)) {
            if (person.getUserFlag() && !StringTools.isValidEmail(person.getEmail()))
                throw new BadClientRequest("An email address must be provided when the user flag is selected.");

            Person originalPerson = _PeopleService.getPerson(personId);
            Person updatedPerson = _PeopleService.savePerson(person);

            if (!originalPerson.getUserFlag() && updatedPerson.getUserFlag()) {
                addUserAfterSave(updatedPerson, appHomePage);
            } else if (originalPerson.getUserFlag() && !updatedPerson.getUserFlag()) {
                try {
                    _UserAdminService.deleteUser(originalPerson.getEmail());
                } catch (Exception ex) {
                    InternalServerError internalServerError = new InternalServerError("The person was successfully saved but their user ID could not be deleted.");
                    internalServerError.initCause(ex);
                    throw internalServerError;
                }
            } else if (originalPerson.getUserFlag() && updatedPerson.getUserFlag() && !originalPerson.getEmail().equals(updatedPerson.getEmail())) {
                try {
                    _UserAdminService.updateUser(originalPerson.getEmail(), updatedPerson.getEmail());
                } catch (Exception ex) {
                    InternalServerError internalServerError = new InternalServerError("The person was successfully saved but their user ID could not be updated.");
                    internalServerError.initCause(ex);
                    throw internalServerError;
                }
            }

            addLink(updatedPerson);
            return new ResponseEntity<>(updatedPerson, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    private void addUserAfterSave(Person person, String appHomePage) {
        String randomPassword = SecurityTools.generateRandomPassword();
        try {
            _UserAdminService.addUser(person.getEmail(), randomPassword);
            String url = ((appHomePage != null) ? appHomePage : ConfigurationSettings.getInstance().getSetting("ui_url"));
            EmailClient.sendEmail(person.getEmail(), "Parent/Teacher Conference Calendar", String.format("Welcome %s,\n\nYou have been granted access to the Parent/Teacher Conference Calendar.  You may access the application by navigating to %s and logging in using your email address and the password listed below.  Once logged in, you will be able to change your password to something that will be easier to remember.\n\nPassword: %s\n\nThank You,\nSalte Technologies LLC", person.getFirstName(), url, randomPassword));
        } catch (UserAdminServiceError ex) {
            person.setUserFlag(false);
            _PeopleService.savePerson(person);
            InternalServerError internalServerError = new InternalServerError("The person was saved successfully but their user ID could not be created.");
            internalServerError.initCause(ex);
            throw internalServerError;
        } catch (MessagingException ex) {
            InternalServerError internalServerError = new InternalServerError("The person was saved and a user ID was successfully created.  However, we were unable to send a new user notification to their email address.  Try resetting their password.  If successful, an email notification will be sent to them containing their new password.");
            internalServerError.initCause(ex);
            throw internalServerError;
        }
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/people/{personId}", method = RequestMethod.DELETE)
    public void deletePerson(@PathVariable Integer personId) {
        Person original = _PeopleService.getPerson(personId);
        _PeopleService.deletePerson(personId);

        if (original.getUserFlag()) {
            try {
                _UserAdminService.deleteUser(original.getEmail());
            } catch (Exception ex) {
                InternalServerError internalServerError = new InternalServerError("The person was successfully removed but their user ID could not be deleted.");
                internalServerError.initCause(ex);
                throw internalServerError;
            }
        }
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = { "/people", "/schools/{schoolId}/people" }, method = RequestMethod.DELETE)
    public void deletePerson(@RequestBody List<Person> persons) {
        _PeopleService.deletePerson(persons);

        StringBuilder failedDeletes = new StringBuilder();
        persons.forEach(person -> {
            try {
                if (person.getUserFlag())
                    _UserAdminService.deleteUser(person.getEmail());
            } catch (Exception ex) {
                if (failedDeletes.length() == 0)
                    failedDeletes.append(person.getEmail());
                else
                    failedDeletes.append(String.format(". %s", person.getEmail()));
            }
        });

        if (failedDeletes.length() > 0) {
            InternalServerError internalServerError = new InternalServerError(String.format("The following people were successfully removed but their user IDs could not be deleted: %s", failedDeletes.toString()));
            throw internalServerError;
        }

    }

    private void addLinks(List<Person> rooms) {
        Iterator<Person> iterator = rooms.iterator();
        Person current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(Person current) {
        current.add(linkTo(methodOn(PeopleController.class).getPerson(null, current.getIdentity())).withSelfRel());

        if (current.getSchoolId() != null)
            current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getSchoolId())).withRel(new School().singularRelationshipName()));

        if (current.getSecurityRoleId() != null)
            current.add(linkTo(methodOn(SecurityRoleController.class).getSecurityRole(current.getSecurityRoleId())).withRel(new SecurityRole().singularRelationshipName()));
    }
}
