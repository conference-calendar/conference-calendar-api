package io.salte.cc.controllers;

import io.salte.cc.models.PersonRole;
import io.salte.cc.services.IPersonRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(PersonRole.class)
public class PersonRoleController {
    @Autowired
    private IPersonRoleService _PersonRoleService;

    @RequestMapping(value = "/person-roles/{personRoleId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getPersonRole(@PathVariable  Integer personRoleId) {
        PersonRole personRole = _PersonRoleService.getPersonRole(personRoleId);
        if (personRole != null) {
            addLink(personRole);
            return new ResponseEntity<>(personRole, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/person-roles", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<PersonRole> getPersonRoleList() {
        List<PersonRole> personRoles = _PersonRoleService.getPersonRoleList();
        addLinks(personRoles);
        return new Resources<>(personRoles, linkTo(methodOn(PersonRoleController.class).getPersonRoleList()).withSelfRel());
    }

    @RequestMapping(value = "/person-roles", method = RequestMethod.POST)
    public ResponseEntity<?> createPersonRole(@RequestBody @Valid PersonRole personRole) {
        if (personRole.getIdentity() == null ) {
            PersonRole savedPersonRole = _PersonRoleService.savePersonRole(personRole);
            addLink(personRole);
            return new ResponseEntity<>(savedPersonRole, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/person-roles/{personRoleId}", method = RequestMethod.POST)
    public ResponseEntity<?> savePersonRole(@RequestBody @Valid PersonRole personRole, @PathVariable Integer personRoleId) {
        if (personRole.getIdentity() != null && personRoleId != null && personRole.getIdentity().equals(personRoleId)) {
            PersonRole savedPersonRole = _PersonRoleService.savePersonRole(personRole);
            addLink(personRole);
            return new ResponseEntity<>(savedPersonRole, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/person-roles/{personRoleId}", method = RequestMethod.DELETE)
    public void deletePersonRole(@PathVariable Integer personRoleId) {
        _PersonRoleService.deletePersonRole(personRoleId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/person-roles", method = RequestMethod.DELETE)
    public void deletePersonRoles(@RequestBody List<PersonRole> personRoles) {
        _PersonRoleService.deletePersonRole(personRoles);
    }

    private void addLinks(List<PersonRole> personRoles) {
        Iterator<PersonRole> iterator = personRoles.iterator();
        PersonRole current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(PersonRole current) {
        current.add(linkTo(methodOn(PersonRoleController.class).getPersonRole(current.getIdentity())).withSelfRel());
    }
}
