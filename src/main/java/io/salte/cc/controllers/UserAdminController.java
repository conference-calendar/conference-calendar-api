package io.salte.cc.controllers;

import io.salte.cc.external.wso2.exceptions.BadUserAdminServiceRequest;
import io.salte.cc.models.ErrorResponse;
import io.salte.cc.services.IUserAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserAdminController {
    @Autowired
    private IUserAdminService userAdminService;

    @RequestMapping(value = "/administration/users/{username:.+}/password", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<?> processPasswordRequest(@PathVariable String username, @RequestParam(name = "action", required = true) String action, @RequestParam(name = "newPassword", required = true) String newPassword, @RequestParam(name = "currentPassword", required = false) String currentPassword) {
        if ("reset".equalsIgnoreCase(action)) {
            userAdminService.resetPassword(username, newPassword);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } else if ("change".equalsIgnoreCase(action) && currentPassword != null) {
            userAdminService.changePassword(username, currentPassword, newPassword);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(new ErrorResponse("The action parameter must be set to either \"reset\" or \"change\" and the current password must be provided to complete the \"change\" action."), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/administration/users", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<?> createUser(@RequestParam(name = "username", required = true) String username, @RequestParam(name = "password", required = true) String password) {
        userAdminService.addUser(username, password);
        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/administration/users", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<?> getUsernameList(@RequestParam(name = "maxUsers", required = true) Integer maxUsers) throws BadUserAdminServiceRequest {
        List<String> userList = userAdminService.getUserList(maxUsers);
        return new Resources<>(userList, linkTo(methodOn(UserAdminController.class).getUsernameList(maxUsers)).withSelfRel());
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/administration/users/{username:.+}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getUser(@PathVariable String username) {
        String existingUsername = userAdminService.getUser(username);
        if (existingUsername != null) {
            return new ResponseEntity<>(existingUsername, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/administration/users/{username:.+}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<?> updateUser(@PathVariable String username, @RequestParam(name = "newUsername", required = true) String newUsername) {
        userAdminService.updateUser(username, newUsername);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/administration/users/{username:.+}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable String username) {
        userAdminService.deleteUser(username);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }
}
