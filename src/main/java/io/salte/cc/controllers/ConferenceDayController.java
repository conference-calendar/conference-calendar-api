package io.salte.cc.controllers;

import io.salte.cc.models.Conference;
import io.salte.cc.models.ConferenceDay;
import io.salte.cc.models.ConferenceSession;
import io.salte.cc.services.IConferenceDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController("ConferenceDayController")
@ExposesResourceFor(ConferenceDay.class)
public class ConferenceDayController {
    @Autowired
    private IConferenceDayService _ConferenceDayService;

    @RequestMapping(value = "/conference-days/{conferenceDayId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getConferenceDay(@PathVariable Integer conferenceDayId) {
        ConferenceDay conferenceDay = _ConferenceDayService.getConferenceDay(conferenceDayId);
        if (conferenceDay != null) {
            addLink(conferenceDay);
            return new ResponseEntity<>(conferenceDay, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/conference-days", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<ConferenceDay> getConferenceDayList() {
        List<ConferenceDay> conferenceDays = _ConferenceDayService.getConferenceDayList();
        addLinks(conferenceDays);
        return new Resources<>(conferenceDays, linkTo(methodOn(ConferenceDayController.class).getConferenceDayList()).withSelfRel());
    }

    @RequestMapping(value = "/conference-sessions/{conferenceSessionId}/conference-days", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<ConferenceDay> getConferenceDayList(@PathVariable int conferenceSessionId) {
        List<ConferenceDay> conferenceDays = _ConferenceDayService.getConferenceDayList(conferenceSessionId);
        addLinks(conferenceDays);
        return new Resources<>(conferenceDays, linkTo(methodOn(ConferenceDayController.class).getConferenceDayList(conferenceSessionId)).withSelfRel());
    }

    @RequestMapping(value = "/conference-days", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createConferenceDay(@RequestBody @Valid ConferenceDay conferenceDay) {
        if (conferenceDay.getIdentity() == null) {
            ConferenceDay conferenceDayUpdated = _ConferenceDayService.saveConferenceDay(conferenceDay);
            addLink(conferenceDayUpdated);
            return new ResponseEntity<>(conferenceDayUpdated, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/conference-sessions/{conferenceSessionId}/conference-days", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createConferenceDay(@RequestBody @Valid ConferenceDay conferenceDay, @PathVariable Integer conferenceSessionId) {
         if (conferenceDay.getIdentity() == null && conferenceSessionId.equals(conferenceDay.getConferenceSessionId())) {
            ConferenceDay conferenceDayUpdated = _ConferenceDayService.saveConferenceDay(conferenceDay);
            addLink(conferenceDayUpdated);
            return new ResponseEntity<>(conferenceDayUpdated, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/conference-days/{conferenceDayId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveConferenceDay(@RequestBody @Valid ConferenceDay conferenceDay, @PathVariable Integer conferenceDayId) {
        if (conferenceDay.getIdentity() != null && conferenceDayId != null && conferenceDay.getIdentity().equals(conferenceDayId)) {
            ConferenceDay conferenceDayUpdated = _ConferenceDayService.saveConferenceDay(conferenceDay);
            addLink(conferenceDayUpdated);
            return new ResponseEntity<>(conferenceDayUpdated, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/conference-sessions/{conferenceSessionId}/conference-days/{conferenceDayId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveConferenceDay(@RequestBody @Valid ConferenceDay conferenceDay, @PathVariable Integer conferenceSessionId, @PathVariable Integer conferenceDayId) {
        if (conferenceDay.getIdentity() != null && conferenceDayId != null && conferenceDay.getIdentity().equals(conferenceDayId) && conferenceDay.getConferenceSessionId() != null && conferenceSessionId != null && conferenceDay.getConferenceSessionId().equals(conferenceSessionId)) {
            ConferenceDay conferenceDayUpdated = _ConferenceDayService.saveConferenceDay(conferenceDay);
            addLink(conferenceDayUpdated);
            return new ResponseEntity<>(conferenceDayUpdated, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/conference-days/{conferenceDayId}", method = RequestMethod.DELETE)
    public void deleteSession(@PathVariable Integer conferenceDayId) {

        _ConferenceDayService.deleteConferenceDay(conferenceDayId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = {"/conference-days", "/conference-sessions/{conferenceSessionId}/conference-days" }, method = RequestMethod.DELETE)
    public void deleteSession(@RequestBody List<ConferenceDay> conferenceDays) {
        _ConferenceDayService.deleteConferenceDays(conferenceDays);
    }

    private void addLinks(List<ConferenceDay> conferenceDays) {
        Iterator<ConferenceDay> iterator = conferenceDays.iterator();
        ConferenceDay current;
          while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(ConferenceDay current) {
        current.add(linkTo(methodOn(ConferenceDayController.class).getConferenceDay(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(ConferenceSessionController.class).getConferenceSession(current.getConferenceSessionId())).withRel(new ConferenceSession().singularRelationshipName()));
        current.add(linkTo(methodOn(ConferenceController.class).getConferenceList(current.getIdentity(), null, null, null)).withRel(new Conference().pluralRelationshipName()));
    }
}
