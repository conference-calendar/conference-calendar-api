package io.salte.cc.controllers;

import io.salte.cc.models.Conference;
import io.salte.cc.models.ConferenceAttendee;
import io.salte.cc.models.Person;
import io.salte.cc.models.PersonRole;
import io.salte.cc.services.IConferenceAttendeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(ConferenceAttendee.class)
public class ConferenceAttendeeController {
    @Autowired
    private IConferenceAttendeeService _ConferenceAttendeeService;

    @RequestMapping(value = "/conference-attendees/{conferenceAttendeeId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getConferenceAttendee(@PathVariable Integer conferenceAttendeeId) {
        ConferenceAttendee conferenceAttendee = _ConferenceAttendeeService.getConferenceAttendee(conferenceAttendeeId);
        if (conferenceAttendee != null) {
            addLink(conferenceAttendee);
            return new ResponseEntity<>(conferenceAttendee, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/conference-attendees", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<ConferenceAttendee> getConferenceAttendees() {
        List<ConferenceAttendee> conferenceAttendees = _ConferenceAttendeeService.getConferenceAttendees();
        addLinks(conferenceAttendees);
        return new Resources<>(conferenceAttendees, linkTo(methodOn(ConferenceAttendeeController.class).getConferenceAttendees()).withSelfRel());
    }

    @RequestMapping(value = "/conference-attendees", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createConferenceAttendee(@RequestBody @Valid ConferenceAttendee conferenceAttendee) {
        if (conferenceAttendee.getIdentity() == null ) {
            ConferenceAttendee savedAttendee = _ConferenceAttendeeService.saveConferenceAttendee(conferenceAttendee);
            addLink(savedAttendee);
            return new ResponseEntity<>(savedAttendee, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/conference-attendees/{conferenceAttendeeId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveConferenceAttendee(@RequestBody @Valid ConferenceAttendee conferenceAttendee, @PathVariable Integer conferenceAttendeeId) {
        if (conferenceAttendee.getIdentity() != null && conferenceAttendeeId != null && conferenceAttendee.getIdentity().equals(conferenceAttendeeId)) {
            ConferenceAttendee savedAttendee = _ConferenceAttendeeService.saveConferenceAttendee(conferenceAttendee);
            addLink(savedAttendee);
            return new ResponseEntity<>(savedAttendee, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/conference-attendees/{conferenceAttendeeId}", method = RequestMethod.DELETE)
    public void deleteConferenceAttendee(@PathVariable Integer conferenceAttendeeId) {
        _ConferenceAttendeeService.deleteConferenceAttendee(conferenceAttendeeId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/conference-attendees", method = RequestMethod.DELETE)
    public void deleteConferenceAttendees(@RequestBody List<ConferenceAttendee> conferenceAttendees) {
        _ConferenceAttendeeService.deleteConferenceAttendees(conferenceAttendees);
    }

    private void addLinks(List<ConferenceAttendee> conferenceAttendees) {
        Iterator<ConferenceAttendee> iterator = conferenceAttendees.iterator();
        ConferenceAttendee current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(ConferenceAttendee current) {
        current.add(linkTo(methodOn(ConferenceAttendeeController.class).getConferenceAttendee(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(PeopleController.class).getPerson(null, current.getPersonId())).withRel(new Person().singularRelationshipName()));
        current.add(linkTo(methodOn(PersonRoleController.class).getPersonRole(current.getPersonRoleId())).withRel(new PersonRole().singularRelationshipName()));
        current.add(linkTo(methodOn(ConferenceController.class).getConference(current.getConferenceId())).withRel(new Conference().singularRelationshipName()));
    }
}
