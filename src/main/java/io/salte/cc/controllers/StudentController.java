package io.salte.cc.controllers;

import io.salte.cc.models.Conference;
import io.salte.cc.models.Student;
import io.salte.cc.services.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Student.class)
public class StudentController {
    @Autowired
    private IStudentService _StudentService;

    @RequestMapping(value = "/students/{personId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getStudent(@PathVariable Integer personId) {
        Student student = _StudentService.getStudent(personId);
        if (student != null) {
            addLink(student);
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/students", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Student> getStudentList() {
        List<Student> students = _StudentService.getStudentList();
        addLinks(students);
        return new Resources<>(students, linkTo(methodOn(StudentController.class).getStudentList()).withSelfRel());
    }

    @RequestMapping(value = "/schools/{schoolId}/students", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Student> getStudentList(@PathVariable Integer schoolId) {
        List<Student> students = _StudentService.getStudentList(schoolId);
        addLinks(students);
        return new Resources<>(students, linkTo(methodOn(StudentController.class).getStudentList(schoolId)).withSelfRel());
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createStudent(@RequestBody @Valid Student student) {
        if (student.getIdentity() == null ) {
            Student savedStudent = _StudentService.saveStudent(student);
            addLink(savedStudent);
            return new ResponseEntity<>(savedStudent, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/students/{personId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveStudent(@RequestBody @Valid Student student, @PathVariable Integer personId) {
        if (student.getIdentity() != null && personId != null && student.getIdentity().equals(personId)) {
            Student savedStudent = _StudentService.saveStudent(student);
            addLink(savedStudent);
            return new ResponseEntity<>(savedStudent, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/students/{personId}", method = RequestMethod.DELETE)
    public void deleteStudent(@PathVariable Integer personId) {
        _StudentService.deleteStudent(personId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = { "/students", "/schools/{schoolId}/students" }, method = RequestMethod.DELETE)
    public void deleteStudent(@RequestBody List<Student> students) {
        _StudentService.deleteStudent(students);
    }

    private void addLinks(List<Student> students) {
        Iterator<Student> iterator = students.iterator();
        Student current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(Student current) {
        current.add(linkTo(methodOn(StudentController.class).getStudent(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getSchoolId())).withRel("school"));
        current.add(linkTo(methodOn(ConferenceController.class).getConferenceList(null, null, current.getIdentity(), null)).withRel(new Conference().pluralRelationshipName()));
    }
}
