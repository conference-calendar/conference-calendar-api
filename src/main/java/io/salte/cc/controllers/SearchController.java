package io.salte.cc.controllers;

import io.salte.cc.models.School;
import io.salte.cc.models.SearchResult;
import io.salte.cc.services.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(SearchResult.class)
public class SearchController {
    @Autowired
    private ISearchService _SearchService;

    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<SearchResult> getSearchResultList(@RequestParam(name = "alias", required = false) String alias,
                                                       @RequestParam(name = "keyword", required = false) String keyword) {

        List<SearchResult> searchResultList = _SearchService.getSearchResultList(alias, keyword);

        Iterator<SearchResult> iterator = searchResultList.iterator();
        SearchResult current;
        while (iterator.hasNext()) {
            current = iterator.next();
            switch (current.getKeywordSourceTable()) {
                case "SC_SCHOOL": current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getKeywordSourceId())).withSelfRel());
                    break;
                case "CR_CLASSROOM": current.add(linkTo(methodOn(ClassroomController.class).getClassroom(current.getKeywordSourceId())).withSelfRel());
                    break;
                case "CO_CONFERENCE": current.add(linkTo(methodOn(ConferenceSessionController.class).getConferenceSession(current.getKeywordSourceId())).withSelfRel());
                    break;
            }
            current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getSchoolId())).withRel(new School().singularRelationshipName()));
        }

        return new Resources<>(searchResultList, linkTo(methodOn(SearchController.class).getSearchResultList(alias, keyword)).withSelfRel());
    }
}