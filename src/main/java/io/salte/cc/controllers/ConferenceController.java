package io.salte.cc.controllers;

import io.salte.cc.models.*;
import io.salte.cc.services.ConferenceDayService;
import io.salte.cc.services.ConferenceSessionService;
import io.salte.cc.services.IConferenceService;
import io.salte.cc.utilities.TimeTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Conference.class)
public class ConferenceController {
    @Autowired
    private IConferenceService _ConferenceService;

    @Autowired
    private ConferenceSessionService _ConferenceSessionService;

    @Autowired
    private ConferenceDayService _ConferenceDayService;

    @RequestMapping(value = "/conferences/{conferenceId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getConference(@PathVariable  Integer conferenceId) {
        Conference current = _ConferenceService.getConference(conferenceId);
        if (current != null) {
            addLink(current);
            return new ResponseEntity<>(current, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/conferences", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Conference> getConferenceList(@RequestParam(name = "conferenceDayId", required = false) Integer conferenceDayId,
                                                   @RequestParam(name = "teacherId", required = false) Integer teacherId,
                                                   @RequestParam(name = "studentId", required = false) Integer studentId,
                                                   @RequestParam(name = "classroomId", required = false) Integer classroomId) {
        Resources<Conference> response = null;

        if (conferenceDayId != null) {
            List<Conference> conferences;
            if (teacherId != null)
                conferences =_ConferenceService.getConferencesByConferenceDayIdAndTeacherId(conferenceDayId, teacherId);
            else
                conferences =_ConferenceService.getConferencesByDay(conferenceDayId);

            conferences = templatize(conferences, conferenceDayId, teacherId);

            addLinks(conferences);
            response = new Resources<>(conferences, linkTo(methodOn(ConferenceController.class).getConferenceList(conferenceDayId, null, null, null)).withSelfRel());
        } else if (teacherId != null) {
            List<Conference> conferences =_ConferenceService.getConferencesByTeacher(teacherId);
            addLinks(conferences);
            response = new Resources<>(conferences, linkTo(methodOn(ConferenceController.class).getConferenceList(null, teacherId, null, null)).withSelfRel());
        } else if (studentId != null) {
            List<Conference> conferences =_ConferenceService.getConferencesByStudent(studentId);
            addLinks(conferences);
            response = new Resources<>(conferences, linkTo(methodOn(ConferenceController.class).getConferenceList(null, null, studentId, null)).withSelfRel());
        } else if (classroomId != null) {
            List<Conference> conferences =_ConferenceService.getConferencesByClassroom(classroomId);
            addLinks(conferences);
            response = new Resources<>(conferences, linkTo(methodOn(ConferenceController.class).getConferenceList(null, null, null, classroomId)).withSelfRel());
        } else {
            List<Conference> conferences =_ConferenceService.getConferenceList();
            addLinks(conferences);
            response = new Resources<>(conferences, linkTo(methodOn(ConferenceController.class).getConferenceList(null, null, null, null)).withSelfRel());
        }

        return response;
    }

    @RequestMapping(value = "/conferences", method = RequestMethod.POST)
    public ResponseEntity<?> createConference(@RequestBody @Valid Conference conference) {
        if (conference.getIdentity() == null ) {
            Conference savedConference = _ConferenceService.saveConference(conference);
            addLink(savedConference);
            return new ResponseEntity<>(savedConference, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/conferences/{conferenceId}", method = RequestMethod.POST)
    public ResponseEntity<?> saveConference(@RequestBody @Valid Conference conference, @PathVariable Integer conferenceId) {
        if (conference.getIdentity() != null && conferenceId != null && conference.getIdentity().equals(conferenceId)) {
            Conference savedConference = _ConferenceService.saveConference(conference);
            addLink(savedConference);
            return new ResponseEntity<>(savedConference, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/conferences/{conferenceId}", method = RequestMethod.DELETE)
    public Conference deleteConference(@PathVariable Integer conferenceId) {
        Conference conference = _ConferenceService.getConference(conferenceId);
        _ConferenceService.deleteConference(conferenceId);
        conference = templatize(conference.getSchoolId(), conference.getConferenceDayId(), conference.getTeacherId(), conference.getStartTime(), conference.getEndTime());
        addLink(conference);
        return conference;
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/conferences", method = RequestMethod.DELETE)
    public void deleteConferences(@RequestBody List<Conference> conferences) {
        _ConferenceService.deleteConference(conferences);
    }

    private List<Conference> templatize(List<Conference> scheduledConferences, Integer conferenceDayId, Integer teacherId) {
        ConferenceDay conferenceDay = _ConferenceDayService.getConferenceDay(conferenceDayId);
        ConferenceSession conferenceSession = _ConferenceSessionService.getConferenceSession(conferenceDay.getConferenceSessionId());

        Time workingTime = (Time) conferenceDay.getStartTime().clone();
        int duration = conferenceSession.getConferenceDurationMinutes();
        int interval = conferenceSession.getTimeBetweenConferencesMinutes();

        List<Conference> conferenceList = new ArrayList<Conference>();
        while (TimeTools.addMinutes(workingTime, duration + interval).before(conferenceDay.getEndTime())) {
            int i = 0;
            for (; i < scheduledConferences.size(); i++) {
                if (scheduledConferences.get(i).getStartTime().compareTo(workingTime) == 0) {
                    conferenceList.add(scheduledConferences.get(i));
                    break;
                }
            }

            if (i == scheduledConferences.size())
                conferenceList.add(templatize(conferenceDay.getSchoolId(), conferenceDayId, teacherId, workingTime, TimeTools.addMinutes(workingTime, duration)));

            workingTime = TimeTools.addMinutes(workingTime, duration + interval);
        }

        return conferenceList;
    }

    private Conference templatize(Integer schoolId, Integer conferenceDayId, Integer teacherId, Time startTime, Time endTime) {
        Conference conference = new Conference();
        conference.setSchoolId(schoolId);
        conference.setConferenceDayId(conferenceDayId);
        conference.setTeacherId(teacherId);
        conference.setStartTime(startTime);
        conference.setEndTime(endTime);
        return conference;
    }

    private void addLinks(List<Conference> conferences) {
        Iterator<Conference> iterator = conferences.iterator();
        Conference current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(Conference current) {
        current.add(linkTo(methodOn(ConferenceDayController.class).getConferenceDayList(current.getConferenceDayId())).withRel(new ConferenceDay().singularRelationshipName()));

        if (current.getIdentity() != null) {
            current.add(linkTo(methodOn(ConferenceController.class).getConference(current.getIdentity())).withSelfRel());
            current.add(linkTo(methodOn(StudentController.class).getStudent(current.getStudentId())).withRel(new Student().singularRelationshipName()));
            current.add(linkTo(methodOn(TeacherController.class).getTeacher(current.getTeacherId())).withRel(new Teacher().singularRelationshipName()));
            if (current.getClassroomId() != null)
                current.add(linkTo(methodOn(ClassroomController.class).getClassroom(current.getClassroomId())).withRel(new Classroom().singularRelationshipName()));
        } else
            current.add(linkTo(methodOn(ConferenceController.class).createConference(current)).withSelfRel());
    }
}
