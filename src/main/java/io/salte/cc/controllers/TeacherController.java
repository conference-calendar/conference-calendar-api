package io.salte.cc.controllers;

import io.salte.cc.models.Conference;
import io.salte.cc.models.School;
import io.salte.cc.models.Teacher;
import io.salte.cc.services.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Teacher.class)
public class TeacherController {
    @Autowired
    private ITeacherService _TeacherService;

    @RequestMapping(value = "/teachers/{personId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getTeacher(@PathVariable Integer personId) {
        Teacher teacher = _TeacherService.getTeacher(personId);
        if (teacher != null) {
            addLink(teacher);
            return new ResponseEntity<>(teacher, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/teachers", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Teacher> getTeacherList() {
        List<Teacher> teachers = _TeacherService.getTeacherList();
        addLinks(teachers);
        return new Resources<>(teachers, linkTo(methodOn(TeacherController.class).getTeacherList()).withSelfRel());
    }

    @RequestMapping(value = "/schools/{schoolId}/teachers", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Teacher> getTeacherList(@PathVariable Integer schoolId) {
        List<Teacher> teachers = _TeacherService.getTeacherList(schoolId);
        addLinks(teachers);
        return new Resources<>(teachers, linkTo(methodOn(TeacherController.class).getTeacherList(schoolId)).withSelfRel());
    }

    @RequestMapping(value = "/teachers", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createTeacher(@RequestBody @Valid Teacher Teacher) {
        if (Teacher.getIdentity() == null ) {
            Teacher teacher = _TeacherService.saveTeacher(Teacher);
            addLink(teacher);
            return new ResponseEntity<>(teacher, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/teachers/{personId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveTeacher(@RequestBody @Valid Teacher teacher, @PathVariable Integer personId) {
        if (teacher.getIdentity() != null && personId != null && teacher.getIdentity().equals(personId)) {
            Teacher savedTeacher = _TeacherService.saveTeacher(teacher);
            addLink(savedTeacher);
            return new ResponseEntity<>(savedTeacher, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/teachers/{personId}", method = RequestMethod.DELETE)
    public void deleteTeacher(@PathVariable Integer personId) {
        _TeacherService.deleteTeacher(personId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = { "/teachers", "/schools/{schoolId}/teachers" }, method = RequestMethod.DELETE)
    public void deleteTeachers(@RequestBody List<Teacher> teachers) {
        _TeacherService.deleteTeacher(teachers);
    }

    private void addLinks(List<Teacher> teachers) {
        Iterator<Teacher> iterator = teachers.iterator();
        Teacher current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(Teacher current) {
        current.add(linkTo(methodOn(TeacherController.class).getTeacher(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getSchoolId())).withRel(new School().singularRelationshipName()));
        current.add(linkTo(methodOn(ConferenceController.class).getConferenceList(null, current.getIdentity(), null, null)).withRel(new Conference().pluralRelationshipName()));
    }
}
