package io.salte.cc.controllers;

import io.salte.cc.models.Classroom;
import io.salte.cc.models.Conference;
import io.salte.cc.models.School;
import io.salte.cc.services.IClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Classroom.class)
public class ClassroomController {
    @Autowired
    private IClassroomService _ClassroomService;

    @RequestMapping(value = "/classrooms/{classroomId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public  ResponseEntity<?> getClassroom(@PathVariable  Integer classroomId) {
        Classroom room = _ClassroomService.getClassroom(classroomId);
        if (room != null) {
            addLink(room);
            return new ResponseEntity<>(room, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/classrooms", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Classroom> getClassroomList() {
        List<Classroom> rooms = _ClassroomService.getClassroomList();
        addLinks(rooms);
        return new Resources<>(rooms, linkTo(methodOn(ClassroomController.class).getClassroomList()).withSelfRel());
    }

    @RequestMapping(value = "/schools/{schoolId}/classrooms", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<Classroom> getClassroomList(@PathVariable Integer schoolId) {
        List<Classroom> rooms = _ClassroomService.getClassroomList(schoolId);
        addLinks(rooms);
        return new Resources<>(rooms, linkTo(methodOn(ClassroomController.class).getClassroomList(schoolId)).withSelfRel());
    }

    @RequestMapping(value = "/classrooms", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createClassroom(@RequestBody @Valid Classroom classroom) {
        if (classroom.getIdentity() == null ) {
            Classroom savedClassroom = _ClassroomService.saveClassroom(classroom);
            addLink(savedClassroom);
            return new ResponseEntity<>(savedClassroom, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/classrooms/{classroomId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveClassroom(@RequestBody @Valid Classroom classroom, @PathVariable Integer classroomId) {
        if (classroom.getIdentity() != null && classroomId != null && classroom.getIdentity().equals(classroomId)) {
            Classroom savedClassroom = _ClassroomService.saveClassroom(classroom);
            addLink(classroom);
            return new ResponseEntity<>(savedClassroom, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/classrooms/{classroomId}", method = RequestMethod.DELETE)
    public void deleteClassroom(@PathVariable Integer classroomId) {
        _ClassroomService.deleteClassroom(classroomId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = { "/classrooms", "/schools/{schoolId}/classrooms" }, method = RequestMethod.DELETE)
    public void deleteClassroom(@RequestBody List<Classroom> classrooms) {
        _ClassroomService.deleteClassrooms(classrooms);
    }

    private void addLinks(List<Classroom> rooms) {
        Iterator<Classroom> iterator = rooms.iterator();
        Classroom current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(Classroom current) {
        current.add(linkTo(methodOn(ClassroomController.class).getClassroom(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getSchoolId())).withRel(new School().singularRelationshipName()));
        current.add(linkTo(methodOn(ConferenceController.class).getConferenceList(null, null, null, current.getIdentity())).withRel(new Conference().pluralRelationshipName()));
    }
}
