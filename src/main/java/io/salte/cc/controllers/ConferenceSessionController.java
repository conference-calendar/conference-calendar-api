package io.salte.cc.controllers;

import io.salte.cc.models.ConferenceDay;
import io.salte.cc.models.ConferenceSession;
import io.salte.cc.models.School;
import io.salte.cc.services.IConferenceSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController("ConferenceSessionController")
@ExposesResourceFor(ConferenceSession.class)
public class ConferenceSessionController {
    @Autowired
    private IConferenceSessionService _ConferenceSessionService;

    @RequestMapping(value = "/conference-sessions/{conferenceSessionId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getConferenceSession(@PathVariable Integer conferenceSessionId) {
        ConferenceSession conferenceSession = _ConferenceSessionService.getConferenceSession(conferenceSessionId);
        if (conferenceSession != null) {
            addLink(conferenceSession);
            return new ResponseEntity<>(conferenceSession, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/conference-sessions", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<ConferenceSession> getConferenceSessionList() {
        List<ConferenceSession> conferenceSessions = _ConferenceSessionService.getConferenceSessionList();
        addLinks(conferenceSessions);
        return new Resources<>(conferenceSessions, linkTo(methodOn(ConferenceSessionController.class).getConferenceSessionList()).withSelfRel());
    }

    @RequestMapping(value = "/schools/{schoolId}/conference-sessions", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<ConferenceSession> getConferenceSessionList(@PathVariable int schoolId) {
        List<ConferenceSession> conferenceSessions = _ConferenceSessionService.getConferenceSessionList(schoolId);
        addLinks(conferenceSessions);
        return new Resources<>(conferenceSessions, linkTo(methodOn(ConferenceSessionController.class).getConferenceSessionList(schoolId)).withSelfRel());
    }

    @RequestMapping(value = "/conference-sessions", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> createConferenceSession(@RequestBody @Valid ConferenceSession ConferenceSession) {
        if (ConferenceSession.getIdentity() == null ) {
            ConferenceSession conferenceSession = _ConferenceSessionService.saveConferenceSession(ConferenceSession);
            addLink(conferenceSession);
            return new ResponseEntity<>(conferenceSession, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/conference-sessions/{conferenceSessionId}", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> saveConferenceSession(@RequestBody @Valid ConferenceSession ConferenceSession, @PathVariable Integer conferenceSessionId) {
        if (ConferenceSession.getIdentity() != null && conferenceSessionId != null && ConferenceSession.getIdentity().equals(conferenceSessionId)) {
            ConferenceSession conferenceSession = _ConferenceSessionService.saveConferenceSession(ConferenceSession);
            addLink(conferenceSession);
            return new ResponseEntity<>(conferenceSession, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/conference-sessions/{conferenceSessionId}", method = RequestMethod.DELETE)
    public void deleteConferenceSession(@PathVariable Integer conferenceSessionId) {

        _ConferenceSessionService.deleteConferenceSession(conferenceSessionId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = {"/conference-sessions", "/schools/{OrganizationId}/conference-sessions" }, method = RequestMethod.DELETE)
    public void deleteConferenceSessions(@RequestBody List<ConferenceSession> conferenceSessions) {
        _ConferenceSessionService.deleteConferenceSession(conferenceSessions);
    }

    private void addLinks(List<ConferenceSession> conferenceSessions) {
        Iterator<ConferenceSession> iterator = conferenceSessions.iterator();
        ConferenceSession current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(ConferenceSession current) {
        current.add(linkTo(methodOn(ConferenceSessionController.class).getConferenceSession(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(ConferenceDayController.class).getConferenceDayList(current.getIdentity())).withRel(new ConferenceDay().pluralRelationshipName()));
        current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getSchoolId())).withRel(new School().singularRelationshipName()));
    }
}
