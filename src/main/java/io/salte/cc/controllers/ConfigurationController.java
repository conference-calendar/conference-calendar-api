package io.salte.cc.controllers;

import io.salte.cc.configuration.ConfigurationSettings;
import io.salte.cc.exceptions.ConfigurationInitializationException;
import io.salte.cc.external.wso2.utilities.Constants;
import io.salte.cc.models.*;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.DatatypeConverter;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(Configuration.class)
public class ConfigurationController {
    @RequestMapping(value = "/configuration", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getConfiguration() {
        Configuration configurationInfo = new Configuration();

        try {
            String temp;
            String tempArray[] = ConfigurationSettings.getInstance().getSetting("project_version").split("-");
            configurationInfo.setBuildType(tempArray[1]);
            String tempArray2[] = tempArray[0].split("\\.");
            configurationInfo.setMajorVersion(Integer.parseInt(tempArray2[0]));
            configurationInfo.setMinorVersion(Integer.parseInt(tempArray2[1]));

            temp = ConfigurationSettings.getInstance().getSetting("build_number");
            if (temp != null && temp.length() > 0)
                configurationInfo.setBuildNumber(Integer.parseInt(temp));

            temp = ConfigurationSettings.getInstance().getSetting("build_timestamp");
            if (temp != null && temp.length() > 0)
                configurationInfo.setLastUpdated(DatatypeConverter.parseDate(temp).getTime());

            temp = ConfigurationSettings.getInstance().getSetting("app_gateway_enabled");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayEnabled(Boolean.parseBoolean(temp));

            temp = ConfigurationSettings.getInstance().getSetting("app_auth_enabled");
            if (temp != null && temp.length() > 0)
                configurationInfo.setAuthorizationEnabled(Boolean.parseBoolean(temp));

            temp = ConfigurationSettings.getInstance().getSetting("db_host");
            if (temp != null && temp.length() > 0)
                configurationInfo.setDatabaseHost(temp);

            temp = ConfigurationSettings.getInstance().getSetting("db_port");
            if (temp != null && temp.length() > 0)
                configurationInfo.setDatabasePort(temp);

            temp = ConfigurationSettings.getInstance().getSetting("db_schema");
            if (temp != null && temp.length() > 0)
                configurationInfo.setDatabaseSchema(temp);

            temp = ConfigurationSettings.getInstance().getSetting("db_username");
            if (temp != null && temp.length() > 0)
                configurationInfo.setDatabaseUsername(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_admin_service_url");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayAdminServiceUrl(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_username");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayUsername(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_db_host");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayDatabaseHost(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_db_port");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayDatabasePort(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_db_schema");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayDatabaseSchema(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_db_username");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayDatabaseUsername(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_jwt_assertion");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewayJwtAssertion(temp);

            temp = ConfigurationSettings.getInstance().getSetting("RS256");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewaySigningAlgorithm(temp);

            temp = ConfigurationSettings.getInstance().getSetting("gateway_signing_certificate");
            if (temp != null && temp.length() > 0)
                configurationInfo.setGatewaySigningCertificate(temp);

            configurationInfo.add(linkTo(methodOn(ConfigurationController.class).getConfiguration()).withSelfRel());
            configurationInfo.add(linkTo(methodOn(SchoolController.class).getSchoolList()).withRel(new School().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(ConferenceSessionController.class).getConferenceSessionList()).withRel(new ConferenceSession().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(ConferenceDayController.class).getConferenceDayList()).withRel(new ConferenceDay().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(ConferenceController.class).getConferenceList(null, null, null, null)).withRel(new Conference().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(ConferenceAttendeeController.class).getConferenceAttendees()).withRel(new ConferenceAttendee().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(PeopleController.class).getPersonList()).withRel(new Person().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(TeacherController.class).getTeacherList()).withRel(new Teacher().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(StudentController.class).getStudentList()).withRel(new Student().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(ClassroomController.class).getClassroomList()).withRel(new Classroom().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(PersonRoleController.class).getPersonRoleList()).withRel(new PersonRole().pluralRelationshipName()));
            configurationInfo.add(linkTo(methodOn(SearchController.class).getSearchResultList(null, null)).withRel(new SearchResult().pluralRelationshipName()));

            return new ResponseEntity<>(configurationInfo, HttpStatus.OK);
        } catch (ConfigurationInitializationException ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
