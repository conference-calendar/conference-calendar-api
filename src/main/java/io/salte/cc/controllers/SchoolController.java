package io.salte.cc.controllers;

import io.salte.cc.models.Classroom;
import io.salte.cc.models.School;
import io.salte.cc.models.Student;
import io.salte.cc.models.Teacher;
import io.salte.cc.services.ISchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(School.class)
public class SchoolController {
    @Autowired
    private ISchoolService _SchoolService;

    @RequestMapping(value = "/schools/{schoolId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getSchool(@PathVariable  Integer schoolId) {
        School current = _SchoolService.getSchool(schoolId);
        if (current != null) {
            addLink(current);
            return new ResponseEntity<>(current, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/schools", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<School> getSchoolList() {
        List<School> schools =_SchoolService.getSchoolList();
        addLinks(schools);
        return new Resources<>(schools, linkTo(methodOn(SchoolController.class).getSchoolList()).withSelfRel());
    }

    @RequestMapping(value = "/schools", method = RequestMethod.POST)
    public ResponseEntity<?> createSchool(@RequestBody @Valid School school) {
        if (school.getIdentity() == null ) {
            School savedSchool = _SchoolService.saveSchool(school);
            addLink(savedSchool);
            return new ResponseEntity<>(savedSchool, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/schools/{schoolId}", method = RequestMethod.POST)
    public ResponseEntity<?> saveSchool(@RequestBody @Valid School school, @PathVariable Integer schoolId) {
        if (school.getIdentity() != null && schoolId != null && school.getIdentity().equals(schoolId)) {
            School savedSchool = _SchoolService.saveSchool(school);
            addLink(savedSchool);
            return new ResponseEntity<>(savedSchool, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/schools/{schoolId}", method = RequestMethod.DELETE)
    public void deleteSchool(@PathVariable Integer schoolId) {
        _SchoolService.deleteSchool(schoolId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/schools", method = RequestMethod.DELETE)
    public void deleteSchool(@RequestBody List<School> schools) {
        _SchoolService.deleteSchool(schools);
    }

    private void addLinks(List<School> schools) {
        Iterator<School> iterator = schools.iterator();
        School current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(School current) {
        current.add(linkTo(methodOn(SchoolController.class).getSchool(current.getIdentity())).withSelfRel());
        current.add(linkTo(methodOn(ClassroomController.class).getClassroomList(current.getIdentity())).withRel(new Classroom().pluralRelationshipName()));
        current.add(linkTo(methodOn(StudentController.class).getStudentList(current.getIdentity())).withRel(new Student().pluralRelationshipName()));
        current.add(linkTo(methodOn(TeacherController.class).getTeacherList(current.getIdentity())).withRel(new Teacher().pluralRelationshipName()));
    }
}
