package io.salte.cc.controllers;

import io.salte.cc.exceptions.BadClientRequest;
import io.salte.cc.exceptions.UnauthorizedApiAccessException;
import io.salte.cc.external.wso2.exceptions.BadUserAdminServiceRequest;
import io.salte.cc.external.wso2.exceptions.ForbiddenUserAdminServiceRequest;
import io.salte.cc.external.wso2.exceptions.InvalidUserAdminServiceRequest;
import io.salte.cc.models.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
    @ExceptionHandler({UnauthorizedApiAccessException.class, AccessDeniedException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    ErrorResponse handleUnauthorizedException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex);
        return errorResponse;
    }

    @ExceptionHandler({BadUserAdminServiceRequest.class, BadClientRequest.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ErrorResponse handleBadRequestException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex);
        return errorResponse;
    }

    @ExceptionHandler(InvalidUserAdminServiceRequest.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ErrorResponse handleNotFoundException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex);
        return errorResponse;
    }

    @ExceptionHandler(ForbiddenUserAdminServiceRequest.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    ErrorResponse handleForbiddenException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex);
        return errorResponse;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    ErrorResponse handleInternalServerErrorException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex);
        return errorResponse;
    }
}
