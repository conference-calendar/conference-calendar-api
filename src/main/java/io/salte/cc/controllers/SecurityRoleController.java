package io.salte.cc.controllers;

import io.salte.cc.configuration.ConfigurationSettings;
import io.salte.cc.models.SecurityRole;
import io.salte.cc.services.ISecurityRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@ExposesResourceFor(SecurityRole.class)
public class SecurityRoleController {
    @Autowired
    private ISecurityRoleService _SecurityRoleService;

    @RequestMapping(value = "/security-roles/{securityRoleId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public ResponseEntity<?> getSecurityRole(@PathVariable  Integer securityRoleId) {
        if (securityRoleId == -1) {
            SecurityRole securityRole = new SecurityRole();
            securityRole.setIdentity(-1);
            securityRole.setName(ConfigurationSettings.getInstance().getSetting("gateway_jwt_role_admin"));
            addLink(securityRole);
            return new ResponseEntity<>(securityRole, HttpStatus.OK);
        } else {
            SecurityRole securityRole = _SecurityRoleService.getSecurityRole(securityRoleId);
            if (securityRole != null) {
                addLink(securityRole);
                return new ResponseEntity<>(securityRole, HttpStatus.OK);
            } else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/security-roles", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
    public Resources<SecurityRole> getSecurityRoleList() {
        List<SecurityRole> securityRoles = _SecurityRoleService.getSecurityRoleList();
        addLinks(securityRoles);
        return new Resources<>(securityRoles, linkTo(methodOn(SecurityRoleController.class).getSecurityRoleList()).withSelfRel());
    }

    @RequestMapping(value = "/security-roles", method = RequestMethod.POST)
    public ResponseEntity<?> createSecurityRole(@RequestBody @Valid SecurityRole securityRole) {
        if (securityRole.getIdentity() == null ) {
            SecurityRole savedSecurityRole = _SecurityRoleService.saveSecurityRole(securityRole);
            addLink(securityRole);
            return new ResponseEntity<>(savedSecurityRole  , HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/security-roles/{securityRoleId}", method = RequestMethod.POST)
    public ResponseEntity<?> saveSecurityRole(@RequestBody @Valid SecurityRole securityRole, @PathVariable Integer securityRoleId) {
        if (securityRole.getIdentity() != null && securityRoleId != null && securityRole.getIdentity().equals(securityRoleId)) {
            SecurityRole savedsecurityRole = _SecurityRoleService.saveSecurityRole(securityRole);
            addLink(securityRole);
            return new ResponseEntity<>(savedsecurityRole, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/security-roles/{securityRoleId}", method = RequestMethod.DELETE)
    public void deletesecurityRole(@PathVariable Integer securityRoleId) {
        _SecurityRoleService.deleteSecurityRole(securityRoleId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/security-roles", method = RequestMethod.DELETE)
    public void deleteSecurityRoles(@RequestBody List<SecurityRole> securityRoles) {
        _SecurityRoleService.deleteSecurityRole(securityRoles);
    }

    private void addLinks(List<SecurityRole> securityRoles) {
        Iterator<SecurityRole> iterator = securityRoles.iterator();
        SecurityRole current;
        while (iterator.hasNext()) {
            current = iterator.next();
            addLink(current);
        }
    }

    private void addLink(SecurityRole current) {
        current.add(linkTo(methodOn(SecurityRoleController.class).getSecurityRole(current.getIdentity())).withSelfRel());
    }
}
