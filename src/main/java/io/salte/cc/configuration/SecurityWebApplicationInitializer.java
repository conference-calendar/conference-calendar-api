package io.salte.cc.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@ComponentScan("io.salte.cc")
public class SecurityWebApplicationInitializer
   extends AbstractSecurityWebApplicationInitializer {
}
