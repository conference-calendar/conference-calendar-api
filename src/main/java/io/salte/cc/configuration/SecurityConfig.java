package io.salte.cc.configuration;

import io.salte.cc.exceptions.InternalServerError;
import io.salte.cc.security.StatelessAuthenticationFilter;
import io.salte.cc.security.TokenAuthenticationService;
import io.salte.cc.security.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

//import io.salte.cc.security.TokenAuthenticationProvider;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "io.salte.cc.security")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final Logger logger = LogManager.getLogger(SecurityConfig.class);
    private final UserService userService;
    private final TokenAuthenticationService authenticationService;

    public SecurityConfig() {
        super(true);

        try {
            this.userService = new UserService();
            authenticationService = new TokenAuthenticationService(ConfigurationSettings.getInstance().getSetting("gateway_signing_certificate"), userService);
        } catch (Exception ex) {
            InternalServerError internalServerError = new InternalServerError(String.format("Unable to initialize TokenAuthenticationService with the following certificate: %s.", ConfigurationSettings.getInstance().getSetting("gateway_signing_certificate")));
            internalServerError.initCause(ex);
            logger.error(internalServerError.toString());
            throw internalServerError;
        }
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.exceptionHandling().and()
                    .anonymous().and()
                    .servletApi().and()
                    .headers().cacheControl().disable().and()
                    .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .anyRequest().authenticated().and()
                    .addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService()), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    @Override
    public UserService userDetailsService() {
        return  userService;
    }

    @Bean
    public TokenAuthenticationService tokenAuthenticationService() {
        return authenticationService;
    }
}
