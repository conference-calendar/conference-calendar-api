package io.salte.cc.configuration;

import io.salte.cc.exceptions.ConfigurationInitializationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class ConfigurationSettings {
    private static final Logger logger = LogManager.getLogger(ConfigurationSettings.class);
    private static ConfigurationSettings instance = null;
    private Properties settings;

    public String getClaimsNamespace() {
        if (claimsNamespace == null)
            claimsNamespace = this.getSetting("gateway_jwt_namespace");

        return claimsNamespace;
    }

    private String claimsNamespace;

    private ConfigurationSettings() {}

    public static ConfigurationSettings getInstance() throws ConfigurationInitializationException {
        if (instance == null) {
            instance = new ConfigurationSettings();
            instance.Initialize();
        }
        return instance;
    }

    public String getSetting(String name) {
        String var = System.getenv(name);
        if (var != null && var.length() > 0)
            return var;
        else
            return settings.getProperty(name);
    }

    public String getFirstNameClaimIndex() {
        return this.getSetting("gateway_jwt_namespace").concat("/").concat(this.getSetting("gateway_jwt_first"));
    }

    public String getLastNameClaimIndex() {
        return this.getSetting("gateway_jwt_namespace").concat("/").concat(this.getSetting("gateway_jwt_last"));
    }

    public String getEmailClaimIndex() {
        return this.getSetting("gateway_jwt_namespace").concat("/").concat(this.getSetting("gateway_jwt_email"));
    }

    public String getRoleClaimIndex() {
        return this.getSetting("gateway_jwt_namespace").concat("/").concat(this.getSetting("gateway_jwt_role"));
    }

    private void Initialize() throws ConfigurationInitializationException {
        settings = new Properties();
        try {
            settings.load(getClass().getClassLoader().getResourceAsStream("conference-calendar-api.properties"));
        } catch (IOException ex) {
            logger.error(String.format("Unable to read configuration initialization settings: %s", ex.toString()));
            throw new ConfigurationInitializationException("Unable to read configuration initialization settings.");
        }
    }
}
