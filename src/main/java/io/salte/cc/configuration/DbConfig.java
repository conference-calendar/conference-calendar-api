package io.salte.cc.configuration;

import io.salte.cc.exceptions.ConfigurationInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"io.salte.cc.repositories", "io.salte.cc.models"})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories(basePackages = "io.salte.cc.repositories", entityManagerFactoryRef = "appDbEntityManagerFactory", transactionManagerRef = "appDbTransactionManager")
public class DbConfig {
    @Primary
    @Bean(name = "appDbDatasource")
    public DriverManagerDataSource appDbDatasource() throws ConfigurationInitializationException {
        String host = ConfigurationSettings.getInstance().getSetting("db_host");
        String port = ConfigurationSettings.getInstance().getSetting("db_port");
        String schema = ConfigurationSettings.getInstance().getSetting("db_schema");
        String username = ConfigurationSettings.getInstance().getSetting("db_username");
        String password = ConfigurationSettings.getInstance().getSetting("db_password");

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(String.format("jdbc:mysql://%s:%s/%s", host, port, schema));
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Primary
    @Bean(name = "appDbEntityManager")
    public EntityManager appDbEntityManager() {
        return appDbEntityManagerFactory().createEntityManager();
    }

    @Primary
    @Bean(name = "appDbEntityManagerFactory")
    public EntityManagerFactory appDbEntityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(Boolean.parseBoolean(ConfigurationSettings.getInstance().getSetting("app_show_sql")));
        vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5InnoDBDialect");
        vendorAdapter.setDatabase(Database.MYSQL);
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("io.salte.cc.models");
        factory.setDataSource(appDbDatasource());
        factory.setPersistenceUnitName("appDb");
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        properties.setProperty("hibernate.format_sql", "true");
        properties.setProperty("hibernate.ejb.entitymanager_factory_name", "appDbEntityManager");
        factory.setJpaProperties(properties);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Primary
    @Bean(name = "appDbTransactionManager")
    public JpaTransactionManager appDbTransactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        JpaDialect jpaDialect = new HibernateJpaDialect();
        txManager.setEntityManagerFactory(appDbEntityManagerFactory());
        txManager.setJpaDialect(jpaDialect);
        return txManager;
    }
}
