package io.salte.cc.configuration;

import io.salte.cc.exceptions.ConfigurationInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"io.salte.cc.external.wso2.repositories", "io.salte.cc.external.wso2.models"})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories(basePackages = "io.salte.cc.external.wso2.repositories", entityManagerFactoryRef = "wso2DbEntityManagerFactory", transactionManagerRef = "wso2DbTransactionManager")
public class WSO2DbConfig {
    @Bean(name = "wso2DbDatasource")
    public DriverManagerDataSource wso2DbDatasource() throws ConfigurationInitializationException {
        String host = ConfigurationSettings.getInstance().getSetting("gateway_db_host");
        String port = ConfigurationSettings.getInstance().getSetting("gateway_db_port");
        String schema = ConfigurationSettings.getInstance().getSetting("gateway_db_schema");
        String username = ConfigurationSettings.getInstance().getSetting("gateway_db_username");;
        String password = ConfigurationSettings.getInstance().getSetting("gateway_db_password");

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(String.format("jdbc:mysql://%s:%s/%s", host, port, schema));
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean(name = "wso2DbEntityManager")
    public EntityManager wso2DbEntityManager() {
        return wso2DbEntityManagerFactory().createEntityManager();
    }

    @Bean(name = "wso2DbEntityManagerFactory")
    public EntityManagerFactory wso2DbEntityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(Boolean.parseBoolean(ConfigurationSettings.getInstance().getSetting("app_show_sql")));
        vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5InnoDBDialect");
        vendorAdapter.setDatabase(Database.MYSQL);
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("io.salte.cc.external.wso2.models");
        factory.setDataSource(wso2DbDatasource());
        factory.setPersistenceUnitName("wso2");
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        properties.setProperty("hibernate.format_sql", "true");
        properties.setProperty("hibernate.ejb.entitymanager_factory_name", "wso2DbEntityManager");
        factory.setJpaProperties(properties);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean(name = "wso2DbTransactionManager")
    public JpaTransactionManager wso2DbTransactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        JpaDialect jpaDialect = new HibernateJpaDialect();
        txManager.setEntityManagerFactory(wso2DbEntityManagerFactory());
        txManager.setJpaDialect(jpaDialect);
        return txManager;
    }
}
