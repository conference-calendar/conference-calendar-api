package io.salte.cc.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "io.salte.cc.services")
public class ServiceConfig {
}
