package io.salte.cc.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "CA_CONFERENCE_ATTENDEE")
public class ConferenceAttendee extends BaseModel {
    private Integer conferenceAttendeeId;
    private Integer personId;
    private Integer conferenceId;
    private Integer personRoleId;
    private Integer schoolId;
    private Integer createUserId;
    private Date createDate;
    private Integer modifyUserId;
    private Date modifyDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CA_ID", nullable = false)
    public Integer getIdentity() {
        return conferenceAttendeeId;
    }

    public void setIdentity(Integer identity) {
        conferenceAttendeeId = identity;
    }

    @Basic
    @Column(name = "CA_PERSON_ID", nullable = false)
    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "CA_CONFERENCE_ID", nullable = false)
    public Integer getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(Integer conferenceId) {
        this.conferenceId = conferenceId;
    }

    @Basic
    @Column(name = "CA_PERSON_ROLE_ID", nullable = false)
    public Integer getPersonRoleId() {
        return personRoleId;
    }

    @Basic
    @Column(name = "CA_SCHOOL_ID", nullable = false)
    public void setPersonRoleId(Integer personRoleId) {
        this.personRoleId = personRoleId;
    }

    @Basic
    @Column(name = "CA_SCHOOL_ID", nullable = false)
    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Basic
    @Column(name = "CREATE_USER_ID", nullable = true)
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer CreateUserId) {
        this.createUserId = CreateUserId;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.createDate = CreateDate;
    }

    @Basic
    @Column(name = "MODIFY_USER_ID", nullable = true)
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(Integer ModifyUserId) {
        this.modifyUserId = ModifyUserId;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date ModifyDate) {
        this.modifyDate = ModifyDate;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConferenceAttendee that = (ConferenceAttendee) o;
        if (personId != null ? !personId.equals(that.personId) : that.personId != null)
            return false;
        if (conferenceId != null ? !conferenceId.equals(that.conferenceId) : that.conferenceId != null)
            return false;
        if (personRoleId != null ? !personRoleId.equals(that.personRoleId) : that.personRoleId != null)
            return false;
        if (schoolId != null ? !schoolId.equals(that.schoolId) : that.schoolId != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null)
            return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null)
            return false;

        return true;
    }
    public int hashCode() {
        int result = 0;
        result = 31 * result + (conferenceAttendeeId != null ? conferenceAttendeeId.hashCode() : 0);
        result = 31 * result + (personId != null ? personId.hashCode() : 0);
        result = 31 * result + (conferenceId != null ? conferenceId.hashCode() : 0);
        result = 31 * result + (personRoleId != null ? personRoleId.hashCode() : 0);
        result = 31 * result + (schoolId != null ? schoolId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);

        return result;
    }
}
