package io.salte.cc.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.ResourceSupport;

import java.util.Calendar;
import java.util.Date;

public class Configuration extends ResourceSupport {
    private Integer majorVersion;
    private Integer minorVersion;
    private Integer buildNumber;
    private Date lastUpdated;
    private String buildType;
    private Boolean authorizationEnabled;
    private Boolean gatewayEnabled;
    private String databaseHost;
    private String databasePort;
    private String databaseSchema;
    private String databaseUsername;
    private String gatewayUsername;
    private String gatewayDatabaseHost;
    private String gatewayDatabasePort;
    private String gatewayDatabaseSchema;
    private String gatewayDatabaseUsername;
    private String gatewayAdminServiceUrl;
    private String gatewayJwtAssertion;
    private String gatewaySigningAlgorithm;
    private String gatewaySigningCertificate;

    public Configuration() {
        majorVersion = 0;
        minorVersion = 0;
        buildNumber = -1;
        lastUpdated = Calendar.getInstance().getTime();
        buildType = "UNINITIALIZED";
        authorizationEnabled = false;
        gatewayEnabled = false;
        databaseHost = "UNINITIALIZED";;
        databasePort = "UNINITIALIZED";;
        databaseSchema = "UNINITIALIZED";;
        databaseUsername = "UNINITIALIZED";;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public Integer getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(Integer minorVersion) {
        this.minorVersion = minorVersion;
    }

    public Integer getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(Integer buildNumber) {
        this.buildNumber = buildNumber;
    }

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="MM-dd-yyyy hh:mm a z", timezone="US/Central")
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getBuildType() {
        return buildType;
    }

    public void setBuildType(String buildType) {
        this.buildType = buildType;
    }

    public Boolean getAuthorizationEnabled() {
        return authorizationEnabled;
    }

    public void setAuthorizationEnabled(Boolean authorizationEnabled) {
        this.authorizationEnabled = authorizationEnabled;
    }

    public Boolean getGatewayEnabled() {
        return gatewayEnabled;
    }

    public void setGatewayEnabled(Boolean gatewayEnabled) {
        this.gatewayEnabled = gatewayEnabled;
    }

    public String getDatabaseHost() {
        return databaseHost;
    }

    public void setDatabaseHost(String databaseHost) {
        this.databaseHost = databaseHost;
    }

    public String getDatabasePort() {
        return databasePort;
    }

    public void setDatabasePort(String databasePort) {
        this.databasePort = databasePort;
    }

    public String getDatabaseSchema() {
        return databaseSchema;
    }

    public void setDatabaseSchema(String databaseSchema) {
        this.databaseSchema = databaseSchema;
    }

    public String getDatabaseUsername() {
        return databaseUsername;
    }

    public void setDatabaseUsername(String databaseUsername) {
        this.databaseUsername = databaseUsername;
    }

    public String getGatewayUsername() {
        return gatewayUsername;
    }

    public void setGatewayUsername(String gatewayUsername) {
        this.gatewayUsername = gatewayUsername;
    }

    public String getGatewayDatabaseHost() {
        return gatewayDatabaseHost;
    }

    public void setGatewayDatabaseHost(String gatewayDatabaseHost) {
        this.gatewayDatabaseHost = gatewayDatabaseHost;
    }

    public String getGatewayDatabasePort() {
        return gatewayDatabasePort;
    }

    public void setGatewayDatabasePort(String gatewayDatabasePort) {
        this.gatewayDatabasePort = gatewayDatabasePort;
    }

    public String getGatewayDatabaseSchema() {
        return gatewayDatabaseSchema;
    }

    public void setGatewayDatabaseSchema(String gatewayDatabaseSchema) {
        this.gatewayDatabaseSchema = gatewayDatabaseSchema;
    }

    public String getGatewayDatabaseUsername() {
        return gatewayDatabaseUsername;
    }

    public void setGatewayDatabaseUsername(String gatewayDatabaseUsername) {
        this.gatewayDatabaseUsername = gatewayDatabaseUsername;
    }

    public String getGatewayAdminServiceUrl() {
        return gatewayAdminServiceUrl;
    }

    public void setGatewayAdminServiceUrl(String gatewayAdminServiceUrl) {
        this.gatewayAdminServiceUrl = gatewayAdminServiceUrl;
    }

    public String getGatewayJwtAssertion() {
        return gatewayJwtAssertion;
    }

    public void setGatewayJwtAssertion(String gatewayJwtAssertion) {
        this.gatewayJwtAssertion = gatewayJwtAssertion;
    }

    public String getGatewaySigningAlgorithm() {
        return gatewaySigningAlgorithm;
    }

    public void setGatewaySigningAlgorithm(String gatewaySigningAlgorithm) {
        this.gatewaySigningAlgorithm = gatewaySigningAlgorithm;
    }

    public String getGatewaySigningCertificate() {
        return gatewaySigningCertificate;
    }

    public void setGatewaySigningCertificate(String gatewaySigningCertificate) {
        this.gatewaySigningCertificate = gatewaySigningCertificate;
    }
}
