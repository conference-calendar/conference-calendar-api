package io.salte.cc.models;

public class ErrorResponse {
    String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public ErrorResponse(Exception exception) {
        message = (exception.getMessage() != null ? exception.getMessage() : "Unexpected server error occurred.");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
