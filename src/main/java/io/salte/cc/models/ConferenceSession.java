package io.salte.cc.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "CS_CONFERENCE_SESSION")
public class ConferenceSession extends BaseModel {
    private Integer conferenceSessionId;
    private String name;
    private Date startDate;
    private Date endDate;
    private Integer conferenceDurationMinutes;
    private Integer timeBetweenConferencesMinutes;
    private Integer schoolId;
    private Integer createUserId;
    private Date createDate;
    private Integer modifyUserId;
    private Date modifyDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CS_ID", nullable = false)
    public Integer getIdentity() {
        return conferenceSessionId;
    }

    public void setIdentity(Integer identity) {
        conferenceSessionId = identity;
    }

    @Basic
    @Column(name = "CS_NAME", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String title) {
        this.name = title;
    }

    @Basic
    @Column(name = "CS_START_DATE", nullable = false)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "CS_END_DATE", nullable = false)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "CS_CONFERENCE_DURATION_MIN", nullable = false)
    public Integer getConferenceDurationMinutes() {
        return conferenceDurationMinutes;
    }

    public void setConferenceDurationMinutes(Integer conferenceDurationMinutes) {
        this.conferenceDurationMinutes = conferenceDurationMinutes;
    }

    @Basic
    @Column(name = "CS_TIME_BETWEEN_CONFERENCES_MIN", nullable = false)
    public Integer getTimeBetweenConferencesMinutes() {
        return timeBetweenConferencesMinutes;
    }

    public void setTimeBetweenConferencesMinutes(Integer timeBetweenConferencesMinutes) {
        this.timeBetweenConferencesMinutes = timeBetweenConferencesMinutes;
    }

    @Column(name = "CS_SCHOOL_ID", nullable = false)
    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Basic
    @Column(name = "CREATE_USER_ID", nullable = true)
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer CreateUserId) {
        this.createUserId = CreateUserId;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.createDate = CreateDate;
    }

    @Basic
    @Column(name = "MODIFY_USER_ID", nullable = true)
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(Integer ModifyUserId) {
        this.modifyUserId = ModifyUserId;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date ModifyDate) {
        this.modifyDate = ModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConferenceSession that = (ConferenceSession) o;
        if (name != null ? !name.equals(that.name) : that.name != null)
            return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null)
            return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null)
            return false;
        if (conferenceDurationMinutes != null ? !conferenceDurationMinutes.equals(that.conferenceDurationMinutes) : that.conferenceDurationMinutes != null)
            return false;
        if (timeBetweenConferencesMinutes != null ? !timeBetweenConferencesMinutes.equals(that.timeBetweenConferencesMinutes) : that.timeBetweenConferencesMinutes != null)
            return false;
        if (schoolId != null ? !schoolId.equals(that.schoolId) : that.schoolId != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null)
            return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (conferenceSessionId != null ? conferenceSessionId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (conferenceDurationMinutes != null ? conferenceDurationMinutes.hashCode() : 0);
        result = 31 * result + (timeBetweenConferencesMinutes != null ? timeBetweenConferencesMinutes.hashCode() : 0);
        result = 31 * result + (schoolId != null ? schoolId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);

        return result;
    }
}
