package io.salte.cc.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "PE_PERSON")
@Inheritance(strategy = InheritanceType.JOINED)
public class Person extends BaseModel {
    private Integer personId;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean userFlag;
    private Integer schoolId;
    private Integer securityRoleId;
    private Integer createUserId;
    private Date createDate;
    private Integer modifyUserId;
    private Date modifyDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PE_ID", nullable = false)
    public Integer getIdentity() {
        return personId;
    }

    public void setIdentity(Integer identity) {
        personId = identity;
    }

    @Basic
    @Column(name = "PE_FNAME", nullable = false, length = 50)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "PE_LNAME", nullable = false, length = 50)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "PE_EMAIL", nullable = true, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "PE_USER_FLAG", nullable = false)
    public Boolean getUserFlag() {
        return userFlag;
    }

    public void setUserFlag(Boolean userFlag) {
        this.userFlag = userFlag;
    }

    @Basic
    @Column(name = "PE_SCHOOL_ID", nullable = false)
    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Basic
    @Column(name = "PE_SECURITY_ROLE_ID", nullable = true)
    public Integer getSecurityRoleId() {
        return securityRoleId;
    }

    public void setSecurityRoleId(Integer securityRoleId) {
        this.securityRoleId = securityRoleId;
    }

    @Basic
    @Column(name = "CREATE_USER_ID", nullable = true)
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer CreateUserId) {
        this.createUserId = CreateUserId;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.createDate = CreateDate;
    }

    @Basic
    @Column(name = "MODIFY_USER_ID", nullable = true)
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(Integer ModifyUserId) {
        this.modifyUserId = ModifyUserId;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date ModifyDate) {
        this.modifyDate = ModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person that = (Person) o;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null)
            return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null)
            return false;
        if (email != null ? !email.equals(that.email) : that.email != null)
            return false;
        if (userFlag != null ? !userFlag.equals(that.userFlag) : that.userFlag != null)
            return false;
        if (schoolId != null ? !schoolId.equals(that.schoolId) : that.schoolId != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null)
            return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (personId != null ? personId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (userFlag != null ? userFlag.hashCode() : 0);
        result = 31 * result + (schoolId != null ? schoolId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);

        return result;
    }

    @Override
    public String pluralRelationshipName() {
        if (this.getClass().getSimpleName().toLowerCase().equals("person"))
            return  "people";
        else
            return super.pluralRelationshipName();
    }
}
