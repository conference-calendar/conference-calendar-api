package io.salte.cc.models;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.standard.StandardFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import java.sql.Date;

@Indexed
@Entity
@AnalyzerDef(name = "standardanalyzer",
             tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
                         filters = { @TokenFilterDef(factory = StandardFilterFactory.class),
                                     @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                                     @TokenFilterDef(factory = StopFilterFactory.class,
                                                     params = { @Parameter(name = "words", value = "classpath:words.txt" ),
                                                                @Parameter(name = "ignoreCase", value = "true")} )
                                   })
@Analyzer(definition = "standardanalyzer")
@Table(name = "SE_SEARCH")
public class SearchResult extends BaseModel {
    private Integer searchResultId;
    private String resultSourceAlias;
    private String result;
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    private String keywordSourceAlias;
    private String keywordSourceTable;
    private Integer keywordSourceId;
    private String keywordSourceField;
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    private String keyword;
    private Integer schoolId;
    private Integer createUserId;
    private Date createDate;
    private Integer modifyUserId;
    private Date modifyDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SE_ID", nullable = false)
    public Integer getIdentity() {
        return searchResultId;
    }

    public void setIdentity(Integer identity) {
        searchResultId = identity;
    }

    @Basic
    @Column(name = "SE_RESULT_SOURCE_ALIAS", nullable = false, length = 50)
    public String getResultSourceAlias() {
        return resultSourceAlias;
    }

    public void setResultSourceAlias(String resultSourceAlias) {
        this.resultSourceAlias = resultSourceAlias;
    }

    @Basic
    @Column(name = "SE_RESULT", nullable = false, length = 50)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Basic
    @Column(name = "SE_KEYWORD_SOURCE_ALIAS", nullable = false, length = 50)
    public String getKeywordSourceAlias() {
        return keywordSourceAlias;
    }

    public void setKeywordSourceAlias(String keywordSourceAlias) {
        this.keywordSourceAlias = keywordSourceAlias;
    }

    @Basic
    @Column(name = "SE_KEYWORD_SOURCE_TABLE", nullable = false, length = 50)
    public String getKeywordSourceTable() {
        return keywordSourceTable;
    }

    public void setKeywordSourceTable(String keywordSourceTable) {
        this.keywordSourceTable = keywordSourceTable;
    }

    @Basic
    @Column(name = "SE_KEYWORD_SOURCE_ID", nullable = false)
    public Integer getKeywordSourceId() {
        return keywordSourceId;
    }

    public void setKeywordSourceId(Integer keywordSourceId) {
        this.keywordSourceId = keywordSourceId;
    }

    @Basic
    @Column(name = "SE_KEYWORD_SOURCE_FIELD", nullable = false, length = 50)
    public String getKeywordSourceField() {
        return keywordSourceField;
    }

    public void setKeywordSourceField(String keywordSourceField) {
        this.keywordSourceField = keywordSourceField;
    }

    @Basic
    @Column(name = "SE_KEYWORD", nullable = false, length = 50)
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Basic
    @Column(name = "SE_SCHOOL_ID", nullable = false)
    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Basic
    @Column(name = "CREATE_USER_ID", nullable = true)
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer CreateUserId) {
        this.createUserId = CreateUserId;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.createDate = CreateDate;
    }

    @Basic
    @Column(name = "MODIFY_USER_ID", nullable = true)
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(Integer ModifyUserId) {
        this.modifyUserId = ModifyUserId;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date ModifyDate) {
        this.modifyDate = ModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchResult that = (SearchResult) o;
        if (searchResultId != null ? !searchResultId.equals(that.searchResultId) : that.searchResultId != null)
            return false;
        if (resultSourceAlias != null ? !resultSourceAlias.equals(that.resultSourceAlias) : that.resultSourceAlias != null)
            return false;
        if (result != null ? !result.equals(that.result) : that.result != null)
            return false;
        if (keywordSourceAlias != null ? !keywordSourceAlias.equals(that.keywordSourceAlias) : that.keywordSourceAlias != null)
            return false;
        if (keywordSourceTable != null ? !keywordSourceTable.equals(that.keywordSourceTable) : that.keywordSourceTable != null)
            return false;
        if (keywordSourceId != null ? !keywordSourceId.equals(that.keywordSourceId) : that.keywordSourceId != null)
            return false;
        if (keywordSourceField != null ? !keywordSourceField.equals(that.keywordSourceField) : that.keywordSourceField != null)
            return false;
        if (keyword != null ? !keyword.equals(that.keyword) : that.keyword != null)
            return false;
        if (schoolId != null ? !schoolId.equals(that.schoolId) : that.schoolId != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null)
            return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (searchResultId != null ? searchResultId.hashCode() : 0);
        result = 31 * result + (resultSourceAlias != null ? resultSourceAlias.hashCode() : 0);
        result = 31 * result + (this.result != null ? this.result.hashCode() : 0);
        result = 31 * result + (keywordSourceAlias != null ? keywordSourceAlias.hashCode() : 0);
        result = 31 * result + (keywordSourceTable != null ? keywordSourceTable.hashCode() : 0);
        result = 31 * result + (keywordSourceId != null ? keywordSourceId.hashCode() : 0);
        result = 31 * result + (keywordSourceField != null ? keywordSourceField.hashCode() : 0);
        result = 31 * result + (keyword != null ? keyword.hashCode() : 0);
        result = 31 * result + (schoolId != null ? schoolId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);
        return result;
    }
}
