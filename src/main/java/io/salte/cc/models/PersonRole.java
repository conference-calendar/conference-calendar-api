package io.salte.cc.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "PR_PERSON_ROLE")
public class PersonRole extends BaseModel {
    private Integer personRoleId;
    private String name;
    private Integer createUserId;
    private Date createDate;
    private Integer modifyUserId;
    private Date modifyDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PR_ID", nullable = false)
    public Integer getIdentity() {
        return personRoleId;
    }

    public void setIdentity(Integer identity) {
        personRoleId = identity;
    }

    @Basic
    @Column(name = "PR_NAME", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "CREATE_USER_ID", nullable = true)
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer CreateUserId) {
        this.createUserId = CreateUserId;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.createDate = CreateDate;
    }

    @Basic
    @Column(name = "MODIFY_USER_ID", nullable = true)
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(Integer ModifyUserId) {
        this.modifyUserId = ModifyUserId;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date ModifyDate) {
        this.modifyDate = ModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonRole that = (PersonRole) o;
        if (personRoleId != null ? !personRoleId.equals(that.personRoleId) : that.personRoleId != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null)
            return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null)
            return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (personRoleId != null ? personRoleId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);

        return result;
    }
}
