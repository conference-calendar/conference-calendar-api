package io.salte.cc.models;

import org.springframework.hateoas.ResourceSupport;

public class BaseModel extends ResourceSupport {
    public String singularRelationshipName() {
        return  this.getClass().getSimpleName().replaceAll("(.)(\\p{Upper})", "$1-$2").toLowerCase();
    }

    public String pluralRelationshipName() {
        return  this.getClass().getSimpleName().replaceAll("(.)(\\p{Upper})", "$1-$2").toLowerCase().concat("s");
    }
}
