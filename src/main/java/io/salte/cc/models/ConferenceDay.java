package io.salte.cc.models;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name = "CD_CONFERENCE_DAY")
public class ConferenceDay extends BaseModel {
    private Integer conferenceDayId;
    private Date date;
    private Time startTime;
    private Time endTime;
    private Integer conferenceSessionId;
    private Integer schoolId;
    private Integer createUserId;
    private Date createDate;
    private Integer modifyUserId;
    private Date modifyDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CD_ID", nullable = false)
    public Integer getIdentity() {
        return conferenceDayId;
    }

    public void setIdentity(Integer identity) {
        conferenceDayId = identity;
    }

    @Basic
    @Column(name = "CD_DATE", nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "CD_START_TIME", nullable = false)
    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "CD_END_TIME", nullable = false)
    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "CD_CONFERENCE_SESSION_ID", nullable = false)
    public Integer getConferenceSessionId() {
        return conferenceSessionId;
    }

    public void setConferenceSessionId(Integer conferenceSessionId) {
        this.conferenceSessionId = conferenceSessionId;
    }

    @Basic
    @Column(name = "CD_SCHOOL_ID", nullable = false)
    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @Basic
    @Column(name = "CREATE_USER_ID", nullable = true)
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer CreateUserId) {
        this.createUserId = CreateUserId;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date CreateDate) {
        this.createDate = CreateDate;
    }

    @Basic
    @Column(name = "MODIFY_USER_ID", nullable = true)
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(Integer ModifyUserId) {
        this.modifyUserId = ModifyUserId;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date ModifyDate) {
        this.modifyDate = ModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConferenceDay that = (ConferenceDay) o;
        if (conferenceDayId != null ? !conferenceDayId.equals(that.conferenceDayId) : that.conferenceDayId != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (conferenceSessionId != null ? !conferenceSessionId.equals(that.conferenceSessionId) : that.conferenceSessionId != null) return false;
        if (schoolId != null ? !schoolId.equals(that.schoolId) : that.schoolId != null) return false;
        if (createUserId != null ? !createUserId.equals(that.createUserId) : that.createUserId != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null)
            return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (conferenceDayId != null ? conferenceDayId.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (conferenceSessionId != null ? conferenceSessionId.hashCode() : 0);
        result = 31 * result + (schoolId != null ? schoolId.hashCode() : 0);
        result = 31 * result + (createUserId != null ? createUserId.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);

        return result;
    }
}
