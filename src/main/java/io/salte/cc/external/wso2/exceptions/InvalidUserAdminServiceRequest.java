package io.salte.cc.external.wso2.exceptions;

public class InvalidUserAdminServiceRequest extends RuntimeException {
    public InvalidUserAdminServiceRequest() {
        super();
    }

    public InvalidUserAdminServiceRequest(String message) {
        super(message);
    }
}