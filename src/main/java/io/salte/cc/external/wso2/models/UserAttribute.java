package io.salte.cc.external.wso2.models;

import javax.persistence.*;

@Entity
@Table(name = "UM_USER_ATTRIBUTE")
public class UserAttribute {
    private Integer attributeId;
    private String attributeName;
    private String attributeValue;
    private Integer userId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UM_ID", nullable = false)
    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    @Basic
    @Column(name = "UM_ATTR_NAME", nullable = false, length = 255)
    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    @Basic
    @Column(name = "UM_ATTR_VALUE", nullable = true, length = 1024)
    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    @Basic
    @Column(name = "UM_USER_ID", nullable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAttribute that = (UserAttribute) o;
        if (attributeId!= that.attributeId) return false;
        if (attributeName != null ? !attributeName.equals(that.attributeName) : that.attributeName != null) return false;
        if (attributeValue != null ? !attributeValue.equals(that.attributeValue) : that.attributeValue != null) return false;
        if (userId!= that.userId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (attributeId != null ? attributeId.hashCode() : 0);
        result = 31 * result + (attributeName != null ? attributeName.hashCode() : 0);
        result = 31 * result + (attributeValue != null ? attributeValue.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);

        return result;
    }
}