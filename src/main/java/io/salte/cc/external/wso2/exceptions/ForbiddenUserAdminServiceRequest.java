package io.salte.cc.external.wso2.exceptions;

public class ForbiddenUserAdminServiceRequest extends RuntimeException {
    public ForbiddenUserAdminServiceRequest() {
        super();
    }

    public ForbiddenUserAdminServiceRequest(String message) {
        super(message);
    }
}