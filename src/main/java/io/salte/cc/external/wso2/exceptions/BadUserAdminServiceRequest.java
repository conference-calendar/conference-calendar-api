package io.salte.cc.external.wso2.exceptions;

public class BadUserAdminServiceRequest extends RuntimeException {
    public BadUserAdminServiceRequest() {
        super();
    }

    public BadUserAdminServiceRequest(String message) {
        super(message);
    }
}
