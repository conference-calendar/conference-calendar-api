/**
 * UserRealmInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public class UserRealmInfo  implements java.io.Serializable {
    private java.lang.String adminRole;

    private java.lang.String adminUser;

    private java.lang.Boolean bulkImportSupported;

    private java.lang.String[] domainNames;

    private java.lang.Boolean enableUIPageCache;

    private java.lang.String everyOneRole;

    private java.lang.Integer maxItemsPerUIPage;

    private java.lang.Integer maxUIPagesInCache;

    private java.lang.Boolean multipleUserStore;

    private io.salte.cc.external.wso2.services.UserStoreInfo primaryUserStoreInfo;

    private java.lang.String[] requiredUserClaims;

    private java.lang.String[] userClaims;

    private io.salte.cc.external.wso2.services.UserStoreInfo[] userStoresInfo;

    public UserRealmInfo() {
    }

    public UserRealmInfo(
            java.lang.String adminRole,
            java.lang.String adminUser,
            java.lang.Boolean bulkImportSupported,
            java.lang.String[] domainNames,
            java.lang.Boolean enableUIPageCache,
            java.lang.String everyOneRole,
            java.lang.Integer maxItemsPerUIPage,
            java.lang.Integer maxUIPagesInCache,
            java.lang.Boolean multipleUserStore,
            io.salte.cc.external.wso2.services.UserStoreInfo primaryUserStoreInfo,
            java.lang.String[] requiredUserClaims,
            java.lang.String[] userClaims,
            io.salte.cc.external.wso2.services.UserStoreInfo[] userStoresInfo) {
           this.adminRole = adminRole;
           this.adminUser = adminUser;
           this.bulkImportSupported = bulkImportSupported;
           this.domainNames = domainNames;
           this.enableUIPageCache = enableUIPageCache;
           this.everyOneRole = everyOneRole;
           this.maxItemsPerUIPage = maxItemsPerUIPage;
           this.maxUIPagesInCache = maxUIPagesInCache;
           this.multipleUserStore = multipleUserStore;
           this.primaryUserStoreInfo = primaryUserStoreInfo;
           this.requiredUserClaims = requiredUserClaims;
           this.userClaims = userClaims;
           this.userStoresInfo = userStoresInfo;
    }


    /**
     * Gets the adminRole value for this UserRealmInfo.
     * 
     * @return adminRole
     */
    public java.lang.String getAdminRole() {
        return adminRole;
    }


    /**
     * Sets the adminRole value for this UserRealmInfo.
     * 
     * @param adminRole
     */
    public void setAdminRole(java.lang.String adminRole) {
        this.adminRole = adminRole;
    }


    /**
     * Gets the adminUser value for this UserRealmInfo.
     * 
     * @return adminUser
     */
    public java.lang.String getAdminUser() {
        return adminUser;
    }


    /**
     * Sets the adminUser value for this UserRealmInfo.
     * 
     * @param adminUser
     */
    public void setAdminUser(java.lang.String adminUser) {
        this.adminUser = adminUser;
    }


    /**
     * Gets the bulkImportSupported value for this UserRealmInfo.
     * 
     * @return bulkImportSupported
     */
    public java.lang.Boolean getBulkImportSupported() {
        return bulkImportSupported;
    }


    /**
     * Sets the bulkImportSupported value for this UserRealmInfo.
     * 
     * @param bulkImportSupported
     */
    public void setBulkImportSupported(java.lang.Boolean bulkImportSupported) {
        this.bulkImportSupported = bulkImportSupported;
    }


    /**
     * Gets the domainNames value for this UserRealmInfo.
     * 
     * @return domainNames
     */
    public java.lang.String[] getDomainNames() {
        return domainNames;
    }


    /**
     * Sets the domainNames value for this UserRealmInfo.
     * 
     * @param domainNames
     */
    public void setDomainNames(java.lang.String[] domainNames) {
        this.domainNames = domainNames;
    }

    public java.lang.String getDomainNames(int i) {
        return this.domainNames[i];
    }

    public void setDomainNames(int i, java.lang.String _value) {
        this.domainNames[i] = _value;
    }


    /**
     * Gets the enableUIPageCache value for this UserRealmInfo.
     * 
     * @return enableUIPageCache
     */
    public java.lang.Boolean getEnableUIPageCache() {
        return enableUIPageCache;
    }


    /**
     * Sets the enableUIPageCache value for this UserRealmInfo.
     * 
     * @param enableUIPageCache
     */
    public void setEnableUIPageCache(java.lang.Boolean enableUIPageCache) {
        this.enableUIPageCache = enableUIPageCache;
    }


    /**
     * Gets the everyOneRole value for this UserRealmInfo.
     * 
     * @return everyOneRole
     */
    public java.lang.String getEveryOneRole() {
        return everyOneRole;
    }


    /**
     * Sets the everyOneRole value for this UserRealmInfo.
     * 
     * @param everyOneRole
     */
    public void setEveryOneRole(java.lang.String everyOneRole) {
        this.everyOneRole = everyOneRole;
    }


    /**
     * Gets the maxItemsPerUIPage value for this UserRealmInfo.
     * 
     * @return maxItemsPerUIPage
     */
    public java.lang.Integer getMaxItemsPerUIPage() {
        return maxItemsPerUIPage;
    }


    /**
     * Sets the maxItemsPerUIPage value for this UserRealmInfo.
     * 
     * @param maxItemsPerUIPage
     */
    public void setMaxItemsPerUIPage(java.lang.Integer maxItemsPerUIPage) {
        this.maxItemsPerUIPage = maxItemsPerUIPage;
    }


    /**
     * Gets the maxUIPagesInCache value for this UserRealmInfo.
     * 
     * @return maxUIPagesInCache
     */
    public java.lang.Integer getMaxUIPagesInCache() {
        return maxUIPagesInCache;
    }


    /**
     * Sets the maxUIPagesInCache value for this UserRealmInfo.
     * 
     * @param maxUIPagesInCache
     */
    public void setMaxUIPagesInCache(java.lang.Integer maxUIPagesInCache) {
        this.maxUIPagesInCache = maxUIPagesInCache;
    }


    /**
     * Gets the multipleUserStore value for this UserRealmInfo.
     * 
     * @return multipleUserStore
     */
    public java.lang.Boolean getMultipleUserStore() {
        return multipleUserStore;
    }


    /**
     * Sets the multipleUserStore value for this UserRealmInfo.
     * 
     * @param multipleUserStore
     */
    public void setMultipleUserStore(java.lang.Boolean multipleUserStore) {
        this.multipleUserStore = multipleUserStore;
    }


    /**
     * Gets the primaryUserStoreInfo value for this UserRealmInfo.
     * 
     * @return primaryUserStoreInfo
     */
    public io.salte.cc.external.wso2.services.UserStoreInfo getPrimaryUserStoreInfo() {
        return primaryUserStoreInfo;
    }


    /**
     * Sets the primaryUserStoreInfo value for this UserRealmInfo.
     * 
     * @param primaryUserStoreInfo
     */
    public void setPrimaryUserStoreInfo(io.salte.cc.external.wso2.services.UserStoreInfo primaryUserStoreInfo) {
        this.primaryUserStoreInfo = primaryUserStoreInfo;
    }


    /**
     * Gets the requiredUserClaims value for this UserRealmInfo.
     * 
     * @return requiredUserClaims
     */
    public java.lang.String[] getRequiredUserClaims() {
        return requiredUserClaims;
    }


    /**
     * Sets the requiredUserClaims value for this UserRealmInfo.
     * 
     * @param requiredUserClaims
     */
    public void setRequiredUserClaims(java.lang.String[] requiredUserClaims) {
        this.requiredUserClaims = requiredUserClaims;
    }

    public java.lang.String getRequiredUserClaims(int i) {
        return this.requiredUserClaims[i];
    }

    public void setRequiredUserClaims(int i, java.lang.String _value) {
        this.requiredUserClaims[i] = _value;
    }


    /**
     * Gets the userClaims value for this UserRealmInfo.
     * 
     * @return userClaims
     */
    public java.lang.String[] getUserClaims() {
        return userClaims;
    }


    /**
     * Sets the userClaims value for this UserRealmInfo.
     * 
     * @param userClaims
     */
    public void setUserClaims(java.lang.String[] userClaims) {
        this.userClaims = userClaims;
    }

    public java.lang.String getUserClaims(int i) {
        return this.userClaims[i];
    }

    public void setUserClaims(int i, java.lang.String _value) {
        this.userClaims[i] = _value;
    }


    /**
     * Gets the userStoresInfo value for this UserRealmInfo.
     * 
     * @return userStoresInfo
     */
    public io.salte.cc.external.wso2.services.UserStoreInfo[] getUserStoresInfo() {
        return userStoresInfo;
    }


    /**
     * Sets the userStoresInfo value for this UserRealmInfo.
     * 
     * @param userStoresInfo
     */
    public void setUserStoresInfo(io.salte.cc.external.wso2.services.UserStoreInfo[] userStoresInfo) {
        this.userStoresInfo = userStoresInfo;
    }

    public io.salte.cc.external.wso2.services.UserStoreInfo getUserStoresInfo(int i) {
        return this.userStoresInfo[i];
    }

    public void setUserStoresInfo(int i, io.salte.cc.external.wso2.services.UserStoreInfo _value) {
        this.userStoresInfo[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UserRealmInfo)) return false;
        UserRealmInfo other = (UserRealmInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.adminRole==null && other.getAdminRole()==null) || 
             (this.adminRole!=null &&
              this.adminRole.equals(other.getAdminRole()))) &&
            ((this.adminUser==null && other.getAdminUser()==null) || 
             (this.adminUser!=null &&
              this.adminUser.equals(other.getAdminUser()))) &&
            ((this.bulkImportSupported==null && other.getBulkImportSupported()==null) || 
             (this.bulkImportSupported!=null &&
              this.bulkImportSupported.equals(other.getBulkImportSupported()))) &&
            ((this.domainNames==null && other.getDomainNames()==null) || 
             (this.domainNames!=null &&
              java.util.Arrays.equals(this.domainNames, other.getDomainNames()))) &&
            ((this.enableUIPageCache==null && other.getEnableUIPageCache()==null) || 
             (this.enableUIPageCache!=null &&
              this.enableUIPageCache.equals(other.getEnableUIPageCache()))) &&
            ((this.everyOneRole==null && other.getEveryOneRole()==null) || 
             (this.everyOneRole!=null &&
              this.everyOneRole.equals(other.getEveryOneRole()))) &&
            ((this.maxItemsPerUIPage==null && other.getMaxItemsPerUIPage()==null) || 
             (this.maxItemsPerUIPage!=null &&
              this.maxItemsPerUIPage.equals(other.getMaxItemsPerUIPage()))) &&
            ((this.maxUIPagesInCache==null && other.getMaxUIPagesInCache()==null) || 
             (this.maxUIPagesInCache!=null &&
              this.maxUIPagesInCache.equals(other.getMaxUIPagesInCache()))) &&
            ((this.multipleUserStore==null && other.getMultipleUserStore()==null) || 
             (this.multipleUserStore!=null &&
              this.multipleUserStore.equals(other.getMultipleUserStore()))) &&
            ((this.primaryUserStoreInfo==null && other.getPrimaryUserStoreInfo()==null) || 
             (this.primaryUserStoreInfo!=null &&
              this.primaryUserStoreInfo.equals(other.getPrimaryUserStoreInfo()))) &&
            ((this.requiredUserClaims==null && other.getRequiredUserClaims()==null) || 
             (this.requiredUserClaims!=null &&
              java.util.Arrays.equals(this.requiredUserClaims, other.getRequiredUserClaims()))) &&
            ((this.userClaims==null && other.getUserClaims()==null) || 
             (this.userClaims!=null &&
              java.util.Arrays.equals(this.userClaims, other.getUserClaims()))) &&
            ((this.userStoresInfo==null && other.getUserStoresInfo()==null) || 
             (this.userStoresInfo!=null &&
              java.util.Arrays.equals(this.userStoresInfo, other.getUserStoresInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdminRole() != null) {
            _hashCode += getAdminRole().hashCode();
        }
        if (getAdminUser() != null) {
            _hashCode += getAdminUser().hashCode();
        }
        if (getBulkImportSupported() != null) {
            _hashCode += getBulkImportSupported().hashCode();
        }
        if (getDomainNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDomainNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDomainNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEnableUIPageCache() != null) {
            _hashCode += getEnableUIPageCache().hashCode();
        }
        if (getEveryOneRole() != null) {
            _hashCode += getEveryOneRole().hashCode();
        }
        if (getMaxItemsPerUIPage() != null) {
            _hashCode += getMaxItemsPerUIPage().hashCode();
        }
        if (getMaxUIPagesInCache() != null) {
            _hashCode += getMaxUIPagesInCache().hashCode();
        }
        if (getMultipleUserStore() != null) {
            _hashCode += getMultipleUserStore().hashCode();
        }
        if (getPrimaryUserStoreInfo() != null) {
            _hashCode += getPrimaryUserStoreInfo().hashCode();
        }
        if (getRequiredUserClaims() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRequiredUserClaims());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRequiredUserClaims(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUserClaims() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUserClaims());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUserClaims(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUserStoresInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUserStoresInfo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUserStoresInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UserRealmInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UserRealmInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adminRole");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "adminRole"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adminUser");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "adminUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bulkImportSupported");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "bulkImportSupported"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("domainNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "domainNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enableUIPageCache");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "enableUIPageCache"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("everyOneRole");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "everyOneRole"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxItemsPerUIPage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "maxItemsPerUIPage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxUIPagesInCache");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "maxUIPagesInCache"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multipleUserStore");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "multipleUserStore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryUserStoreInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "primaryUserStoreInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UserStoreInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requiredUserClaims");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "requiredUserClaims"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userClaims");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "userClaims"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userStoresInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "userStoresInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UserStoreInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
