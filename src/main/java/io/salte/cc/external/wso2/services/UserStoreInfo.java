/**
 * UserStoreInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public class UserStoreInfo  implements java.io.Serializable {
    private java.lang.Boolean bulkImportSupported;

    private java.lang.String domainName;

    private java.lang.String externalIdP;

    private java.lang.Integer maxRoleLimit;

    private java.lang.Integer maxUserLimit;

    private java.lang.String passwordRegEx;

    private java.lang.Boolean passwordsExternallyManaged;

    private java.lang.Boolean readOnly;

    private java.lang.String roleNameRegEx;

    private java.lang.String userNameRegEx;

    public UserStoreInfo() {
    }

    public UserStoreInfo(
           java.lang.Boolean bulkImportSupported,
           java.lang.String domainName,
           java.lang.String externalIdP,
           java.lang.Integer maxRoleLimit,
           java.lang.Integer maxUserLimit,
           java.lang.String passwordRegEx,
           java.lang.Boolean passwordsExternallyManaged,
           java.lang.Boolean readOnly,
           java.lang.String roleNameRegEx,
           java.lang.String userNameRegEx) {
           this.bulkImportSupported = bulkImportSupported;
           this.domainName = domainName;
           this.externalIdP = externalIdP;
           this.maxRoleLimit = maxRoleLimit;
           this.maxUserLimit = maxUserLimit;
           this.passwordRegEx = passwordRegEx;
           this.passwordsExternallyManaged = passwordsExternallyManaged;
           this.readOnly = readOnly;
           this.roleNameRegEx = roleNameRegEx;
           this.userNameRegEx = userNameRegEx;
    }


    /**
     * Gets the bulkImportSupported value for this UserStoreInfo.
     * 
     * @return bulkImportSupported
     */
    public java.lang.Boolean getBulkImportSupported() {
        return bulkImportSupported;
    }


    /**
     * Sets the bulkImportSupported value for this UserStoreInfo.
     * 
     * @param bulkImportSupported
     */
    public void setBulkImportSupported(java.lang.Boolean bulkImportSupported) {
        this.bulkImportSupported = bulkImportSupported;
    }


    /**
     * Gets the domainName value for this UserStoreInfo.
     * 
     * @return domainName
     */
    public java.lang.String getDomainName() {
        return domainName;
    }


    /**
     * Sets the domainName value for this UserStoreInfo.
     * 
     * @param domainName
     */
    public void setDomainName(java.lang.String domainName) {
        this.domainName = domainName;
    }


    /**
     * Gets the externalIdP value for this UserStoreInfo.
     * 
     * @return externalIdP
     */
    public java.lang.String getExternalIdP() {
        return externalIdP;
    }


    /**
     * Sets the externalIdP value for this UserStoreInfo.
     * 
     * @param externalIdP
     */
    public void setExternalIdP(java.lang.String externalIdP) {
        this.externalIdP = externalIdP;
    }


    /**
     * Gets the maxRoleLimit value for this UserStoreInfo.
     * 
     * @return maxRoleLimit
     */
    public java.lang.Integer getMaxRoleLimit() {
        return maxRoleLimit;
    }


    /**
     * Sets the maxRoleLimit value for this UserStoreInfo.
     * 
     * @param maxRoleLimit
     */
    public void setMaxRoleLimit(java.lang.Integer maxRoleLimit) {
        this.maxRoleLimit = maxRoleLimit;
    }


    /**
     * Gets the maxUserLimit value for this UserStoreInfo.
     * 
     * @return maxUserLimit
     */
    public java.lang.Integer getMaxUserLimit() {
        return maxUserLimit;
    }


    /**
     * Sets the maxUserLimit value for this UserStoreInfo.
     * 
     * @param maxUserLimit
     */
    public void setMaxUserLimit(java.lang.Integer maxUserLimit) {
        this.maxUserLimit = maxUserLimit;
    }


    /**
     * Gets the passwordRegEx value for this UserStoreInfo.
     * 
     * @return passwordRegEx
     */
    public java.lang.String getPasswordRegEx() {
        return passwordRegEx;
    }


    /**
     * Sets the passwordRegEx value for this UserStoreInfo.
     * 
     * @param passwordRegEx
     */
    public void setPasswordRegEx(java.lang.String passwordRegEx) {
        this.passwordRegEx = passwordRegEx;
    }


    /**
     * Gets the passwordsExternallyManaged value for this UserStoreInfo.
     * 
     * @return passwordsExternallyManaged
     */
    public java.lang.Boolean getPasswordsExternallyManaged() {
        return passwordsExternallyManaged;
    }


    /**
     * Sets the passwordsExternallyManaged value for this UserStoreInfo.
     * 
     * @param passwordsExternallyManaged
     */
    public void setPasswordsExternallyManaged(java.lang.Boolean passwordsExternallyManaged) {
        this.passwordsExternallyManaged = passwordsExternallyManaged;
    }


    /**
     * Gets the readOnly value for this UserStoreInfo.
     * 
     * @return readOnly
     */
    public java.lang.Boolean getReadOnly() {
        return readOnly;
    }


    /**
     * Sets the readOnly value for this UserStoreInfo.
     * 
     * @param readOnly
     */
    public void setReadOnly(java.lang.Boolean readOnly) {
        this.readOnly = readOnly;
    }


    /**
     * Gets the roleNameRegEx value for this UserStoreInfo.
     * 
     * @return roleNameRegEx
     */
    public java.lang.String getRoleNameRegEx() {
        return roleNameRegEx;
    }


    /**
     * Sets the roleNameRegEx value for this UserStoreInfo.
     * 
     * @param roleNameRegEx
     */
    public void setRoleNameRegEx(java.lang.String roleNameRegEx) {
        this.roleNameRegEx = roleNameRegEx;
    }


    /**
     * Gets the userNameRegEx value for this UserStoreInfo.
     * 
     * @return userNameRegEx
     */
    public java.lang.String getUserNameRegEx() {
        return userNameRegEx;
    }


    /**
     * Sets the userNameRegEx value for this UserStoreInfo.
     * 
     * @param userNameRegEx
     */
    public void setUserNameRegEx(java.lang.String userNameRegEx) {
        this.userNameRegEx = userNameRegEx;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UserStoreInfo)) return false;
        UserStoreInfo other = (UserStoreInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bulkImportSupported==null && other.getBulkImportSupported()==null) || 
             (this.bulkImportSupported!=null &&
              this.bulkImportSupported.equals(other.getBulkImportSupported()))) &&
            ((this.domainName==null && other.getDomainName()==null) || 
             (this.domainName!=null &&
              this.domainName.equals(other.getDomainName()))) &&
            ((this.externalIdP==null && other.getExternalIdP()==null) || 
             (this.externalIdP!=null &&
              this.externalIdP.equals(other.getExternalIdP()))) &&
            ((this.maxRoleLimit==null && other.getMaxRoleLimit()==null) || 
             (this.maxRoleLimit!=null &&
              this.maxRoleLimit.equals(other.getMaxRoleLimit()))) &&
            ((this.maxUserLimit==null && other.getMaxUserLimit()==null) || 
             (this.maxUserLimit!=null &&
              this.maxUserLimit.equals(other.getMaxUserLimit()))) &&
            ((this.passwordRegEx==null && other.getPasswordRegEx()==null) || 
             (this.passwordRegEx!=null &&
              this.passwordRegEx.equals(other.getPasswordRegEx()))) &&
            ((this.passwordsExternallyManaged==null && other.getPasswordsExternallyManaged()==null) || 
             (this.passwordsExternallyManaged!=null &&
              this.passwordsExternallyManaged.equals(other.getPasswordsExternallyManaged()))) &&
            ((this.readOnly==null && other.getReadOnly()==null) || 
             (this.readOnly!=null &&
              this.readOnly.equals(other.getReadOnly()))) &&
            ((this.roleNameRegEx==null && other.getRoleNameRegEx()==null) || 
             (this.roleNameRegEx!=null &&
              this.roleNameRegEx.equals(other.getRoleNameRegEx()))) &&
            ((this.userNameRegEx==null && other.getUserNameRegEx()==null) || 
             (this.userNameRegEx!=null &&
              this.userNameRegEx.equals(other.getUserNameRegEx())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBulkImportSupported() != null) {
            _hashCode += getBulkImportSupported().hashCode();
        }
        if (getDomainName() != null) {
            _hashCode += getDomainName().hashCode();
        }
        if (getExternalIdP() != null) {
            _hashCode += getExternalIdP().hashCode();
        }
        if (getMaxRoleLimit() != null) {
            _hashCode += getMaxRoleLimit().hashCode();
        }
        if (getMaxUserLimit() != null) {
            _hashCode += getMaxUserLimit().hashCode();
        }
        if (getPasswordRegEx() != null) {
            _hashCode += getPasswordRegEx().hashCode();
        }
        if (getPasswordsExternallyManaged() != null) {
            _hashCode += getPasswordsExternallyManaged().hashCode();
        }
        if (getReadOnly() != null) {
            _hashCode += getReadOnly().hashCode();
        }
        if (getRoleNameRegEx() != null) {
            _hashCode += getRoleNameRegEx().hashCode();
        }
        if (getUserNameRegEx() != null) {
            _hashCode += getUserNameRegEx().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UserStoreInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UserStoreInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bulkImportSupported");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "bulkImportSupported"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("domainName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "domainName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalIdP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "externalIdP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxRoleLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "maxRoleLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxUserLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "maxUserLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordRegEx");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "passwordRegEx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordsExternallyManaged");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "passwordsExternallyManaged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readOnly");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "readOnly"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roleNameRegEx");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "roleNameRegEx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userNameRegEx");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "userNameRegEx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
