/**
 * UserAdmin.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public interface UserAdmin extends javax.xml.rpc.Service {
    public java.lang.String getUserAdminHttpsSoap11EndpointAddress();

    public io.salte.cc.external.wso2.services.UserAdminPortType getUserAdminHttpsSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public io.salte.cc.external.wso2.services.UserAdminPortType getUserAdminHttpsSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
