/**
 * UserAdminUserAdminException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public class UserAdminUserAdminException  extends org.apache.axis.AxisFault  implements java.io.Serializable {
    private io.salte.cc.external.wso2.services.UserAdminException userAdminException;

    public UserAdminUserAdminException() {
    }

    public UserAdminUserAdminException(
            io.salte.cc.external.wso2.services.UserAdminException userAdminException) {
        this.userAdminException = userAdminException;
    }


    /**
     * Gets the userAdminException value for this UserAdminUserAdminException.
     * 
     * @return userAdminException
     */
    public io.salte.cc.external.wso2.services.UserAdminException getUserAdminException() {
        return userAdminException;
    }


    /**
     * Sets the userAdminException value for this UserAdminUserAdminException.
     * 
     * @param userAdminException
     */
    public void setUserAdminException(io.salte.cc.external.wso2.services.UserAdminException userAdminException) {
        this.userAdminException = userAdminException;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UserAdminUserAdminException)) return false;
        UserAdminUserAdminException other = (UserAdminUserAdminException) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userAdminException==null && other.getUserAdminException()==null) || 
             (this.userAdminException!=null &&
              this.userAdminException.equals(other.getUserAdminException())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserAdminException() != null) {
            _hashCode += getUserAdminException().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UserAdminUserAdminException.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://org.apache.axis2/xsd", ">UserAdminUserAdminException"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userAdminException");
        elemField.setXmlName(new javax.xml.namespace.QName("http://org.apache.axis2/xsd", "UserAdminException"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UserAdminException"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }


    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, this);
    }
}
