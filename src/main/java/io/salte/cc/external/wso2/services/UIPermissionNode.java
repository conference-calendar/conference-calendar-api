/**
 * UIPermissionNode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public class UIPermissionNode  implements java.io.Serializable {
    private java.lang.String displayName;

    private io.salte.cc.external.wso2.services.UIPermissionNode[] nodeList;

    private java.lang.String resourcePath;

    private java.lang.Boolean selected;

    public UIPermissionNode() {
    }

    public UIPermissionNode(
            java.lang.String displayName,
            io.salte.cc.external.wso2.services.UIPermissionNode[] nodeList,
            java.lang.String resourcePath,
            java.lang.Boolean selected) {
           this.displayName = displayName;
           this.nodeList = nodeList;
           this.resourcePath = resourcePath;
           this.selected = selected;
    }


    /**
     * Gets the displayName value for this UIPermissionNode.
     * 
     * @return displayName
     */
    public java.lang.String getDisplayName() {
        return displayName;
    }


    /**
     * Sets the displayName value for this UIPermissionNode.
     * 
     * @param displayName
     */
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }


    /**
     * Gets the nodeList value for this UIPermissionNode.
     * 
     * @return nodeList
     */
    public io.salte.cc.external.wso2.services.UIPermissionNode[] getNodeList() {
        return nodeList;
    }


    /**
     * Sets the nodeList value for this UIPermissionNode.
     * 
     * @param nodeList
     */
    public void setNodeList(io.salte.cc.external.wso2.services.UIPermissionNode[] nodeList) {
        this.nodeList = nodeList;
    }

    public io.salte.cc.external.wso2.services.UIPermissionNode getNodeList(int i) {
        return this.nodeList[i];
    }

    public void setNodeList(int i, io.salte.cc.external.wso2.services.UIPermissionNode _value) {
        this.nodeList[i] = _value;
    }


    /**
     * Gets the resourcePath value for this UIPermissionNode.
     * 
     * @return resourcePath
     */
    public java.lang.String getResourcePath() {
        return resourcePath;
    }


    /**
     * Sets the resourcePath value for this UIPermissionNode.
     * 
     * @param resourcePath
     */
    public void setResourcePath(java.lang.String resourcePath) {
        this.resourcePath = resourcePath;
    }


    /**
     * Gets the selected value for this UIPermissionNode.
     * 
     * @return selected
     */
    public java.lang.Boolean getSelected() {
        return selected;
    }


    /**
     * Sets the selected value for this UIPermissionNode.
     * 
     * @param selected
     */
    public void setSelected(java.lang.Boolean selected) {
        this.selected = selected;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UIPermissionNode)) return false;
        UIPermissionNode other = (UIPermissionNode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.displayName==null && other.getDisplayName()==null) || 
             (this.displayName!=null &&
              this.displayName.equals(other.getDisplayName()))) &&
            ((this.nodeList==null && other.getNodeList()==null) || 
             (this.nodeList!=null &&
              java.util.Arrays.equals(this.nodeList, other.getNodeList()))) &&
            ((this.resourcePath==null && other.getResourcePath()==null) || 
             (this.resourcePath!=null &&
              this.resourcePath.equals(other.getResourcePath()))) &&
            ((this.selected==null && other.getSelected()==null) || 
             (this.selected!=null &&
              this.selected.equals(other.getSelected())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDisplayName() != null) {
            _hashCode += getDisplayName().hashCode();
        }
        if (getNodeList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNodeList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNodeList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResourcePath() != null) {
            _hashCode += getResourcePath().hashCode();
        }
        if (getSelected() != null) {
            _hashCode += getSelected().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UIPermissionNode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UIPermissionNode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "displayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "nodeList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "UIPermissionNode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resourcePath");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "resourcePath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("selected");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "selected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
