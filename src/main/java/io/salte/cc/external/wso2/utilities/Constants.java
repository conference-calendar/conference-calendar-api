package io.salte.cc.external.wso2.utilities;

public class Constants {
    private final static String pluralUserRelationshipName = "users";
    private final static String singularUserRelationshipName = "user";
    private final static int DEFAULT_MAX_USERS = 100;

    public static String getPluralUserRelationshipName() {
        return pluralUserRelationshipName;
    }

    public static String getSingularUserRelationshipName() {
        return singularUserRelationshipName;
    }

    public static int getDefaultMaxUsers() {
        return DEFAULT_MAX_USERS;
    }
}
