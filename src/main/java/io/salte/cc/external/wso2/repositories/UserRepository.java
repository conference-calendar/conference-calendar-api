package io.salte.cc.external.wso2.repositories;

import io.salte.cc.external.wso2.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("UserRepository")
public interface UserRepository extends JpaRepository<User, Integer> {
    User findOneByUsername(String username);
}