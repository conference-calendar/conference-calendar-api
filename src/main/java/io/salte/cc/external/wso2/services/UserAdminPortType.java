/**
 * UserAdminPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public interface UserAdminPortType extends java.rmi.Remote {
    public void addInternalRole(java.lang.String roleName, java.lang.String[] userList, java.lang.String[] permissions) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void changePassword(java.lang.String userName, java.lang.String newPassword) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void addRemoveUsersOfRole(java.lang.String roleName, java.lang.String[] newUsers, java.lang.String[] deletedUsers) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] getAllRolesNames(java.lang.String filter, java.lang.Integer limit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] getAllSharedRoleNames(java.lang.String filter, java.lang.Integer limit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void updateUsersOfRole(java.lang.String roleName, io.salte.cc.external.wso2.services.FlaggedName[] userList) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] getRolesOfUser(java.lang.String userName, java.lang.String filter, java.lang.Integer limit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public java.lang.String[] listUsers(java.lang.String filter, java.lang.Integer limit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void updateRolesOfUser(java.lang.String userName, java.lang.String[] newUserList) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void deleteUser(java.lang.String userName) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void deleteRole(java.lang.String roleName) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void bulkImportUsers(java.lang.String fileName, byte[] handler, java.lang.String defaultPassword) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.UIPermissionNode getRolePermissions(java.lang.String roleName) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public java.lang.Boolean isSharedRolesEnabled() throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void updateRoleName(java.lang.String roleName, java.lang.String newRoleName) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] getUsersOfRole(java.lang.String roleName, java.lang.String filter, java.lang.Integer limit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.UserRealmInfo getUserRealmInfo() throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void setRoleUIPermission(java.lang.String roleName, java.lang.String[] rawResources) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void changePasswordByUser(java.lang.String oldPassword, java.lang.String newPassword) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] listAllUsers(java.lang.String filter, java.lang.Integer limit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void addRemoveRolesOfUser(java.lang.String userName, java.lang.String[] newRoles, java.lang.String[] deletedRoles) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void addUser(java.lang.String userName, java.lang.String password, java.lang.String[] roles, io.salte.cc.external.wso2.services.ClaimValue[] claims, java.lang.String profileName) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public void addRole(java.lang.String roleName, java.lang.String[] userList, java.lang.String[] permissions, java.lang.Boolean isSharedRole) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.UIPermissionNode getAllUIPermissions() throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] getRolesOfCurrentUser() throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public io.salte.cc.external.wso2.services.FlaggedName[] listUserByClaim(io.salte.cc.external.wso2.services.ClaimValue claimValue, java.lang.String filter, java.lang.Integer maxLimit) throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
    public java.lang.Boolean hasMultipleUserStores() throws java.rmi.RemoteException, io.salte.cc.external.wso2.services.UserAdminUserAdminException;
}
