package io.salte.cc.external.wso2.repositories;

import io.salte.cc.external.wso2.models.UserAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("UserAttributeRepository")
public interface UserAttributeRepository extends JpaRepository<UserAttribute, Integer> {
    UserAttribute findOneByUserIdAndAttributeName(Integer userId, String attributeValue);
}
