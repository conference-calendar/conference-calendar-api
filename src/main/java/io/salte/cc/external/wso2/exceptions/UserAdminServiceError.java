package io.salte.cc.external.wso2.exceptions;

public class UserAdminServiceError extends RuntimeException {
    public UserAdminServiceError() {
        super();
    }

    public UserAdminServiceError(String message) {
        super(message);
    }
}
