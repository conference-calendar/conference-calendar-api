/**
 * FlaggedName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package io.salte.cc.external.wso2.services;

public class FlaggedName  implements java.io.Serializable {
    private java.lang.String dn;

    private java.lang.String domainName;

    private java.lang.Boolean editable;

    private java.lang.String itemDisplayName;

    private java.lang.String itemName;

    private java.lang.Boolean readOnly;

    private java.lang.String roleType;

    private java.lang.Boolean selected;

    private java.lang.Boolean shared;

    public FlaggedName() {
    }

    public FlaggedName(
           java.lang.String dn,
           java.lang.String domainName,
           java.lang.Boolean editable,
           java.lang.String itemDisplayName,
           java.lang.String itemName,
           java.lang.Boolean readOnly,
           java.lang.String roleType,
           java.lang.Boolean selected,
           java.lang.Boolean shared) {
           this.dn = dn;
           this.domainName = domainName;
           this.editable = editable;
           this.itemDisplayName = itemDisplayName;
           this.itemName = itemName;
           this.readOnly = readOnly;
           this.roleType = roleType;
           this.selected = selected;
           this.shared = shared;
    }


    /**
     * Gets the dn value for this FlaggedName.
     * 
     * @return dn
     */
    public java.lang.String getDn() {
        return dn;
    }


    /**
     * Sets the dn value for this FlaggedName.
     * 
     * @param dn
     */
    public void setDn(java.lang.String dn) {
        this.dn = dn;
    }


    /**
     * Gets the domainName value for this FlaggedName.
     * 
     * @return domainName
     */
    public java.lang.String getDomainName() {
        return domainName;
    }


    /**
     * Sets the domainName value for this FlaggedName.
     * 
     * @param domainName
     */
    public void setDomainName(java.lang.String domainName) {
        this.domainName = domainName;
    }


    /**
     * Gets the editable value for this FlaggedName.
     * 
     * @return editable
     */
    public java.lang.Boolean getEditable() {
        return editable;
    }


    /**
     * Sets the editable value for this FlaggedName.
     * 
     * @param editable
     */
    public void setEditable(java.lang.Boolean editable) {
        this.editable = editable;
    }


    /**
     * Gets the itemDisplayName value for this FlaggedName.
     * 
     * @return itemDisplayName
     */
    public java.lang.String getItemDisplayName() {
        return itemDisplayName;
    }


    /**
     * Sets the itemDisplayName value for this FlaggedName.
     * 
     * @param itemDisplayName
     */
    public void setItemDisplayName(java.lang.String itemDisplayName) {
        this.itemDisplayName = itemDisplayName;
    }


    /**
     * Gets the itemName value for this FlaggedName.
     * 
     * @return itemName
     */
    public java.lang.String getItemName() {
        return itemName;
    }


    /**
     * Sets the itemName value for this FlaggedName.
     * 
     * @param itemName
     */
    public void setItemName(java.lang.String itemName) {
        this.itemName = itemName;
    }


    /**
     * Gets the readOnly value for this FlaggedName.
     * 
     * @return readOnly
     */
    public java.lang.Boolean getReadOnly() {
        return readOnly;
    }


    /**
     * Sets the readOnly value for this FlaggedName.
     * 
     * @param readOnly
     */
    public void setReadOnly(java.lang.Boolean readOnly) {
        this.readOnly = readOnly;
    }


    /**
     * Gets the roleType value for this FlaggedName.
     * 
     * @return roleType
     */
    public java.lang.String getRoleType() {
        return roleType;
    }


    /**
     * Sets the roleType value for this FlaggedName.
     * 
     * @param roleType
     */
    public void setRoleType(java.lang.String roleType) {
        this.roleType = roleType;
    }


    /**
     * Gets the selected value for this FlaggedName.
     * 
     * @return selected
     */
    public java.lang.Boolean getSelected() {
        return selected;
    }


    /**
     * Sets the selected value for this FlaggedName.
     * 
     * @param selected
     */
    public void setSelected(java.lang.Boolean selected) {
        this.selected = selected;
    }


    /**
     * Gets the shared value for this FlaggedName.
     * 
     * @return shared
     */
    public java.lang.Boolean getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this FlaggedName.
     * 
     * @param shared
     */
    public void setShared(java.lang.Boolean shared) {
        this.shared = shared;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FlaggedName)) return false;
        FlaggedName other = (FlaggedName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dn==null && other.getDn()==null) || 
             (this.dn!=null &&
              this.dn.equals(other.getDn()))) &&
            ((this.domainName==null && other.getDomainName()==null) || 
             (this.domainName!=null &&
              this.domainName.equals(other.getDomainName()))) &&
            ((this.editable==null && other.getEditable()==null) || 
             (this.editable!=null &&
              this.editable.equals(other.getEditable()))) &&
            ((this.itemDisplayName==null && other.getItemDisplayName()==null) || 
             (this.itemDisplayName!=null &&
              this.itemDisplayName.equals(other.getItemDisplayName()))) &&
            ((this.itemName==null && other.getItemName()==null) || 
             (this.itemName!=null &&
              this.itemName.equals(other.getItemName()))) &&
            ((this.readOnly==null && other.getReadOnly()==null) || 
             (this.readOnly!=null &&
              this.readOnly.equals(other.getReadOnly()))) &&
            ((this.roleType==null && other.getRoleType()==null) || 
             (this.roleType!=null &&
              this.roleType.equals(other.getRoleType()))) &&
            ((this.selected==null && other.getSelected()==null) || 
             (this.selected!=null &&
              this.selected.equals(other.getSelected()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDn() != null) {
            _hashCode += getDn().hashCode();
        }
        if (getDomainName() != null) {
            _hashCode += getDomainName().hashCode();
        }
        if (getEditable() != null) {
            _hashCode += getEditable().hashCode();
        }
        if (getItemDisplayName() != null) {
            _hashCode += getItemDisplayName().hashCode();
        }
        if (getItemName() != null) {
            _hashCode += getItemName().hashCode();
        }
        if (getReadOnly() != null) {
            _hashCode += getReadOnly().hashCode();
        }
        if (getRoleType() != null) {
            _hashCode += getRoleType().hashCode();
        }
        if (getSelected() != null) {
            _hashCode += getSelected().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FlaggedName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "FlaggedName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "dn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("domainName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "domainName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("editable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "editable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemDisplayName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "itemDisplayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "itemName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readOnly");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "readOnly"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roleType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "roleType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("selected");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "selected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("http://common.mgt.user.carbon.services.org/xsd", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
