image: salte/maven-flyway

variables:
  VERSION: "1.0-RELEASE"
  CERT_FILENAME: "api_salte_io.crt"
  CERT_SOURCE: "src/main/resources"
  CERT_DEST: "/opt/wso2am/repository/resources/security"

stages:
 - build
 - deploy_db
 - deploy
 - test

Build and Unit Test Other Branches:
  stage: build
  except:
   - live@salte-io/conference-calendar-api
   - alpha@salte-io/conference-calendar-api
  script:
   - mvn --quiet clean compile test
  tags:
   - maven

Build and Unit Test for Alpha:
  stage: build
  only:
   - alpha@salte-io/conference-calendar-api
  script:
   - export buildTimeStamp=`date +"%Y-%m-%dT%H:%M:%S%:z"`
   - sonar-runner -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_USER -Dsonar.password=$SONAR_PWD -Dsonar.projectKey=$SONAR_PROJECT_KEY -Dsonar.sources=/builds/salte-io/conference-calendar-api/src/main/java -Dsonar.projectVersion=$VERSION -Dsonar.projectName="$SONAR_PROJECT_NAME"
   - mvn --quiet versions:set -DnewVersion=$VERSION
   - mvn --quiet clean compile test package -Dbuild.number=$CI_BUILD_ID -Dbuild.timestamp=$buildTimeStamp -Dgateway.signing.certificate=$CERT_DEST/$CERT_FILENAME
   - mvn --quiet --settings /builds/salte-io/conference-calendar-api/target/classes/settings.xml deploy:deploy-file -Dfile=/builds/salte-io/conference-calendar-api/target/ConferenceCalendarAPI.war -DrepositoryId=releases -Durl=http://nexus.salte.io:8091/repository/maven-releases -DpomFile=/builds/salte-io/conference-calendar-api/pom.xml
  tags:
   - maven

Migrate Alpha Database:
  stage: deploy_db
  only:
   - alpha@salte-io/conference-calendar-api
  script:
   - flyway -url=jdbc:mysql://$DB_HOST:$DB_PORT_ALPHA/$DB_SCHEMA -user=$DB_USER -password=$DB_PWD -locations=filesystem:. migrate
  tags:
   - maven

Deploy to Alpha:
  stage: deploy
  only:
   - alpha@salte-io/conference-calendar-api
  script:
   - docker stop $DOCKER_ALPHA 2> /dev/null || echo "Docker container $DOCKER_ALPHA not running on Docker host."
   - docker rm $DOCKER_ALPHA 2> /dev/null || echo "Docker container $DOCKER_ALPHA doesn't exist on Docker host."
   - docker rmi $DOCKER_ALPHA 2> /dev/null || echo "Docker image $DOCKER_ALPHA doesn't exist on Docker host."
   - docker build --no-cache --build-arg CERT_SOURCE=$CERT_SOURCE --build-arg CERT_DEST=$CERT_DEST --build-arg CERT_FILENAME=$CERT_FILENAME --build-arg NEXUS_USER=$NEXUS_USER --build-arg NEXUS_PWD=$NEXUS_PWD --build-arg REMOTE_FILE=http://nexus.salte.io:8091/repository/maven-releases/io/salte/conference-calendar-api/$VERSION/conference-calendar-api-$VERSION.war -t $DOCKER_ALPHA .
   - docker run --restart=always --name $DOCKER_ALPHA -p $EXPOSED_PORT_ALPHA:8080 -e app_gateway_enabled=$GATEWAY_ENABLED_ALPHA -e app_auth_enabled=$AUTH_ENABLED_ALPHA -e db_host=$DB_HOST -e db_port=$DB_PORT_ALPHA -e db_schema=$DB_SCHEMA -e db_username=$DB_USER -e db_password=$DB_PWD -d $DOCKER_ALPHA
  tags:
   - shell

Migrate Live Database:
  stage: deploy_db
  only:
   - live@salte-io/conference-calendar-api
  script:
   - flyway -url=jdbc:mysql://$DB_HOST:$DB_PORT_LIVE/$DB_SCHEMA -user=$DB_USER -password=$DB_PWD -locations=filesystem:. migrate
  tags:
   - maven

Deploy to Live:
  stage: deploy
  only:
   - live@salte-io/conference-calendar-api
  script:
   - docker stop $DOCKER_LIVE 2> /dev/null || echo "Docker container $DOCKER_LIVE not running on Docker host."
   - docker rm $DOCKER_LIVE 2> /dev/null || echo "Docker container $DOCKER_LIVE doesn't exist on Docker host."
   - docker rmi $DOCKER_LIVE 2> /dev/null || echo "Docker image $DOCKER_LIVE doesn't exist on Docker host."
   - docker build --no-cache --build-arg CERT_SOURCE=$CERT_SOURCE --build-arg CERT_DEST=$CERT_DEST --build-arg CERT_FILENAME=$CERT_FILENAME --build-arg NEXUS_USER=$NEXUS_USER --build-arg NEXUS_PWD=$NEXUS_PWD --build-arg REMOTE_FILE=http://nexus.salte.io:8091/repository/maven-releases/io/salte/conference-calendar-api/$VERSION/conference-calendar-api-$VERSION.war -t $DOCKER_LIVE .
   - docker run --restart=always --name $DOCKER_LIVE -p $EXPOSED_PORT_LIVE:8080 -e app_gateway_enabled=$GATEWAY_ENABLED_LIVE -e app_auth_enabled=$AUTH_ENABLED_LIVE -e db_host=$DB_HOST -e db_port=$DB_PORT_LIVE -e db_schema=$DB_SCHEMA -e db_username=$DB_USER -e db_password=$DB_PWD -d $DOCKER_LIVE
  tags:
   - shell

Trigger API and UI Tests:
  stage: test
  only:
    - alpha@salte-io/conference-calendar-api
  script:
    - "curl -X POST -F token=1e9bfccd3c0f62667cf2426d7b6743 -F ref=master -F variables[GATEWAY]=$GATEWAY_TEST_ENABLED_ALPHA -F variables[BASE_URL]=$GATEWAY_URL -F variables[TOKEN_URL]=$TOKEN_URL -F variables[BASIC]=$BASIC_AUTH_ALPHA -F variables[USERNAME]=$TEST_USERNAME -F variables[PASSWORD]=$TEST_PASSWORD https://gitlab.com/api/v3/projects/759962/trigger/builds"
    - "curl -X POST -F token=2a1c1b45211ece84014edc32af8f9f -F ref=alpha https://gitlab.com/api/v3/projects/758127/trigger/builds"
  tags:
   - shell
