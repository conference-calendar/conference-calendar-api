FROM tomcat:8-jre8
MAINTAINER Dave Woodward <davidwoodwrd@hotmail.com>

ARG CERT_FILENAME
ARG CERT_SOURCE
ARG CERT_DEST
ARG REMOTE_FILE
ARG NEXUS_USER
ARG NEXUS_PWD

RUN rm -r /usr/local/tomcat/webapps/* && \
    wget --user $NEXUS_USER --password $NEXUS_PWD -O /usr/local/tomcat/webapps/api.war $REMOTE_FILE
COPY $CERT_SOURCE/$CERT_FILENAME $CERT_DEST/$CERT_FILENAME
RUN keytool -import -trustcacerts -noprompt -alias salte -keystore /etc/ssl/certs/java/cacerts -storepass changeit -file $CERT_DEST/$CERT_FILENAME